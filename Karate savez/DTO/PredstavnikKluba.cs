﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DTO
{
    public class PredstavnikKluba : Osoba
    {
        public string EmailAdresa { get; set; }
        public string BrojTelefona { get; set; }

        public PredstavnikKluba(int IdOsobe, string Ime, string Prezime, DateTime DatumRodjenja, string Pol, string EmailAdresa, string BrojTelefona) : base(IdOsobe, Ime, Prezime, DatumRodjenja, Pol)
        {
            this.EmailAdresa = EmailAdresa;
            this.BrojTelefona = BrojTelefona;
        }

        public override string ToString()
        {
            return Ime + " " + Prezime + ";" + BrojTelefona + ";" + EmailAdresa;
        }
    }
}