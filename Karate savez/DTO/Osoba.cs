﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DTO
{
    public class Osoba
    {
        public int IdOsobe { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Pol { get; set; }
        public DateTime DatumRodjenja { get; set; }

        public Osoba(int IdOsobe, string Ime, string Prezime, DateTime DatumRodjenja, string Pol)
        {
            this.IdOsobe = IdOsobe;
            this.Ime = Ime;
            this.Prezime = Prezime;
            this.DatumRodjenja = DatumRodjenja;
            this.Pol = Pol;
        }

        public override string ToString()
        {
            return IdOsobe + ";" + Ime + " " + Prezime + ";" + DatumRodjenja.ToLongDateString();
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Osoba o))
                return false;
            return IdOsobe == o.IdOsobe;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}