﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DTO
{
    public class Takmicar : Osoba
    {
        public string BrojRegistracijeUSavezu { get; set; }
        public bool JeAktivan { get; set; }
        public Klub Klub { get; set; }
        public Pojas Pojas { get; set; }

        public Takmicar(int IdOsobe, string Ime, string Prezime, DateTime DatumRodjenja, string Pol, string BrojRegistracijeUSavezu, Klub Klub, Pojas Pojas = Pojas.Bijeli, bool JeAktivan = true) : base(IdOsobe, Ime, Prezime, DatumRodjenja, Pol)
        {
            this.BrojRegistracijeUSavezu = BrojRegistracijeUSavezu;
            this.Klub = Klub;
            this.Pojas = Pojas;
            this.JeAktivan = JeAktivan;
        }

        public override string ToString()
        {
            return Ime + " " + Prezime + " - " + Klub;
        }
    }
}