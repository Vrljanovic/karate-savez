﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DTO
{
    public class Sudija : Osoba
    {
        public string BrojSudijskeLicence { get; set; }
        public bool JeAktivan { get; set; }

        public Sudija(int IdOsobe, string Ime, string Prezime, DateTime DatumRodjenja, string Pol, string BrojSudijskeLicence, bool JeAktivan = true):base(IdOsobe, Ime, Prezime, DatumRodjenja, Pol)
        {
            this.BrojSudijskeLicence = BrojSudijskeLicence;
            this.JeAktivan = JeAktivan;
        }

        public override string ToString()
        {
            return Ime + " " + Prezime + ";" + BrojSudijskeLicence;
        }
    }
}