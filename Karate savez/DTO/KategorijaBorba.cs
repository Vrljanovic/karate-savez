﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DTO
{
    public class KategorijaBorba : Kategorija
    {
        public int MasaOd { get; set; }
        public int MasaDo { get; set; }

        public KategorijaBorba(int IdKategorije, int GodineOd, int GodineDo, string Pol, int MasaOd, int MasaDo):base(IdKategorije, GodineOd, GodineDo, Pol)
        {
            if (MasaOd >= MasaDo && MasaDo != -1)
                throw new Exception("Greška prilikom kreiranja kategorije!");
            this.MasaOd = MasaOd;
            this.MasaDo = MasaDo;
        }

        public override string ToString()
        {
            string masa = "od " + MasaOd + "kg do " + MasaDo + "kg";
            if (MasaDo == -1 && MasaOd > 0)
                masa = MasaOd + "+kg";
            else if (MasaOd == 0 && MasaDo != -1)
                masa = "-" + MasaDo + "kg";
            else if (MasaOd == 0 && MasaDo == -1)
                masa = "svi";
            if (GodineDo == -1)
                return "Borbe " + Pol + ": " + GodineOd + " godina i stariji - " + masa;
            else if (GodineOd == GodineDo)
                return "Borbe " + Pol + ": " + GodineOd + " godina - " + masa;
            else if (GodineOd == -1)
                return "Borbe " + Pol + ": " + GodineDo + " godina i mladji - " + masa;
            return "Borbe " + Pol + ": " + GodineOd + " - " + GodineDo + " godina - " + masa;
        }
    }
}
