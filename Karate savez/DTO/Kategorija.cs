﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DTO
{
    public class Kategorija
    {
        public int IdKategorije { get; set; }
        public int GodineOd { get; set; }
        public int GodineDo { get; set; }
        public string Pol { get; set; }

        public Kategorija(int IdKategorije, int GodineOd, int GodineDo, string Pol)
        {
            if (GodineDo < GodineOd && GodineDo != -1)
                throw new Exception("Greška prilikom kreiranja kategorije");
            this.IdKategorije = IdKategorije;
            this.GodineOd = GodineOd;
            this.GodineDo = GodineDo;
            this.Pol = Pol;
        }
    }
}
