﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DTO
{
    class RezultatTakmicara
    {
        public Takmicar Takmicar { get; set; }
        public Takmicenje Takmicenje { get; set; }
        public Kategorija Kategorija { get; set; }
        public int OsvojenoMjesto { get; set; }

        public RezultatTakmicara(Takmicar Takmicar, Takmicenje Takmicenje, Kategorija Kategorija, int OsvojenoMjesto)
        {
            this.Takmicar = Takmicar;
            this.Takmicenje = Takmicenje;
            this.Kategorija = Kategorija;
            this.OsvojenoMjesto = OsvojenoMjesto;
        }
    }
}
