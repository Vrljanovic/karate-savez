﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DTO
{
    public class Rezultat
    {
        public Takmicenje Takmicenje { get; set; }
        public Kategorija Kategorija { get; set; }
        public Takmicar PrvoMjesto { get; set; }
        public Takmicar DrugoMjesto { get; set; }
        public Takmicar TreceMjesto { get; set; }
        public Takmicar TreceMjesto2 { get; set; }

        public Rezultat(Takmicenje Takmicenje, Kategorija Kategorija, Takmicar PrvoMjesto, Takmicar DrugoMjesto, Takmicar TreceMjesto, Takmicar TreceMjesto2)
        {
            this.Takmicenje = Takmicenje;
            this.Kategorija = Kategorija;
            this.PrvoMjesto = PrvoMjesto;
            this.DrugoMjesto = DrugoMjesto;
            this.TreceMjesto = TreceMjesto;
            this.TreceMjesto2 = TreceMjesto2;
        }
    }
}
