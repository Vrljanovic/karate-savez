﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DTO
{
    public class Takmicenje
    {
        public int IdTakmicenja { get; set; }
        public DateTime PocetakTakmicenja { get; set; }
        public int BrojBorilista { get; set; }
        public Mjesto MjestoOdrzavanja { get; set; }

        public Takmicenje(int Id, DateTime PocetakOdrzavanja, int BrojBorilista, Mjesto MjestoOdrzavanja)
        {
            if (BrojBorilista <= 0)
                throw new Exception("Nemoguće kreireati Takmicenje");
            this.IdTakmicenja = Id;
            this.PocetakTakmicenja = PocetakOdrzavanja;
            this.BrojBorilista = BrojBorilista;
            this.MjestoOdrzavanja = MjestoOdrzavanja;
        }

        public override string ToString()
        {
            return IdTakmicenja + ": " + MjestoOdrzavanja + "; " + PocetakTakmicenja.ToString() + "; " + BrojBorilista;
        }
    }
}
