﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DTO
{
    public class KategorijaKata : Kategorija
    {
        public int Nivo { get; set; }

        public KategorijaKata(int IdKategorije, int GodineOd, int GodineDo, string Pol, int Nivo) :base(IdKategorije, GodineOd, GodineDo, Pol)
        {
            this.Nivo = Nivo;
        }
        public override string ToString()
        {
            if (GodineDo == -1)
                return "Kate " + Pol + ": " + GodineOd + "godina i stariji - " + Nivo + ". nivo";
            else if (GodineOd == GodineDo)
                return "Kate " + Pol + ": " + GodineOd + "godina - " + Nivo + ". nivo";
            else if (GodineOd == 0)
                return "Kate " + Pol + ": " + GodineDo + "godina i mladji - " + Nivo + ". nivo";
            return "Kate " + Pol + ": od " + GodineOd + " - " + GodineDo + "godina - " + Nivo + ". nivo";
        }
    }
}
