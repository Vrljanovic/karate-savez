﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DTO
{
    public class Mjesto
    {
        public int IdMjesta { get; set; }
        public string Naziv { get; set; }
        public string Adresa { get; set; }

        public Mjesto(int IdMjesta, string Naziv, string Adresa)
        {
            this.IdMjesta = IdMjesta;
            this.Naziv = Naziv;
            this.Adresa = Adresa;
        }

        public override string ToString()
        {
            return Adresa + " " + Naziv;
        }

        public override bool Equals(object obj)
        {
            if(!(obj is Mjesto mjesto))
                return false;
            return IdMjesta == mjesto.IdMjesta;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}