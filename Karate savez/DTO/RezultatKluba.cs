﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DTO
{
    class RezultatKluba
    {
        public Klub Klub { get; set; }
        public Takmicenje Takmicenje { get; set; }
        public int OsvojenoMjesto { get; set; }

        public RezultatKluba(Klub Klub, Takmicenje Takmicenje, int OsvojenoMjesto)
        {
            this.Klub = Klub;
            this.Takmicenje = Takmicenje;
            this.OsvojenoMjesto = OsvojenoMjesto;
        }
    }
}
