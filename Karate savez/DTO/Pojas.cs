﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DTO
{
    public enum Pojas
    {
        Bijeli,
        Zuti,
        Narandzasti,
        Zeleni,
        Plavi,
        Smedji,
        Crni
    }
    static class PronadjiPojas
    {
        public static Pojas Pojas(string pojas)
        {
            switch (pojas)
            {
                case "Zuti":
                    return DTO.Pojas.Zuti;
                case "Narandzasti":
                    return DTO.Pojas.Narandzasti;
                case "Zeleni":
                    return DTO.Pojas.Zeleni;
                case "Plavi":
                    return DTO.Pojas.Plavi;
                case "Smedji":
                    return DTO.Pojas.Smedji;
                case "Crni":
                    return DTO.Pojas.Crni;
                default:
                    return DTO.Pojas.Bijeli;
            }
        }
    }
}