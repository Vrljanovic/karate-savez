﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DTO
{
    class ClanUpravnogOdbora : Osoba
    {
        public DateTime ClanOd { get; set; }
        public DateTime ClanDo { get; set; }

        public ClanUpravnogOdbora(int IdOsobe, string Ime, string Prezime, string Pol, DateTime DatumRodjenja, DateTime ClanOd, DateTime ClanDo) : base(IdOsobe, Ime, Prezime, DatumRodjenja, Pol)
        {
            this.ClanOd = ClanOd;
            this.ClanDo = ClanDo;
        }
    }
}
