﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DTO
{
    public class Klub
    {
        public int IdKluba{ get; set; }
        public string Naziv { get; set; }
        public string JIB { get; set; }
        public bool JeAktivan { get; set; }
        public Mjesto Sjediste { get; set; }
        public PredstavnikKluba PredstavnikKluba { get; set; }

        public Klub(int IdKluba, string Naziv, string JIB,  Mjesto Sjediste, PredstavnikKluba PredstavnikKluba, bool JeAktivan = true)
        {
            this.JeAktivan = JeAktivan;
            this.IdKluba = IdKluba;
            this.Naziv = Naziv;
            this.JIB = JIB;
            this.Sjediste = Sjediste;
            this.PredstavnikKluba = PredstavnikKluba;
        }

        public override string ToString()
        {
            return "Karate Klub \"" + Naziv + "\" " + Sjediste.Naziv;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Klub drugiKlub))
                return false;
            return drugiKlub.IdKluba == IdKluba;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}