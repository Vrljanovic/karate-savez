﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DTO
{
    public class Sastanak
    {
        public int IdSastanka { get; set; }
        public DateTime DatumOdrzavanja { get; set; }
        public string Tema { get; set; }

        public Sastanak(int IdSastanka, DateTime DatumOdrzavanja, string Tema)
        {
            this.IdSastanka = IdSastanka;
            this.DatumOdrzavanja = DatumOdrzavanja;
            this.Tema = Tema;
        }
    }
}
