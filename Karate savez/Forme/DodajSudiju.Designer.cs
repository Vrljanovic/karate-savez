﻿namespace Karate_savez.Forme
{
    partial class DodajSudiju
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ImeTextBox = new System.Windows.Forms.TextBox();
            this.PrezimeTextBox = new System.Windows.Forms.TextBox();
            this.BrojSudijskeLicenceTextBox = new System.Windows.Forms.TextBox();
            this.DatumRodjenjaPicker = new System.Windows.Forms.DateTimePicker();
            this.Dugme_DodajSudiju = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.PolComboBox = new System.Windows.Forms.ComboBox();
            this.Dugme_Odustani = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(72, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ime";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Prezime";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(72, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Broj sudijske licence";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(72, 174);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Datum rodjenja";
            // 
            // ImeTextBox
            // 
            this.ImeTextBox.Location = new System.Drawing.Point(75, 59);
            this.ImeTextBox.Name = "ImeTextBox";
            this.ImeTextBox.Size = new System.Drawing.Size(230, 22);
            this.ImeTextBox.TabIndex = 1;
            // 
            // PrezimeTextBox
            // 
            this.PrezimeTextBox.Location = new System.Drawing.Point(75, 104);
            this.PrezimeTextBox.Name = "PrezimeTextBox";
            this.PrezimeTextBox.Size = new System.Drawing.Size(230, 22);
            this.PrezimeTextBox.TabIndex = 3;
            // 
            // BrojSudijskeLicenceTextBox
            // 
            this.BrojSudijskeLicenceTextBox.Location = new System.Drawing.Point(77, 149);
            this.BrojSudijskeLicenceTextBox.Name = "BrojSudijskeLicenceTextBox";
            this.BrojSudijskeLicenceTextBox.Size = new System.Drawing.Size(228, 22);
            this.BrojSudijskeLicenceTextBox.TabIndex = 5;
            // 
            // DatumRodjenjaPicker
            // 
            this.DatumRodjenjaPicker.Location = new System.Drawing.Point(77, 194);
            this.DatumRodjenjaPicker.Name = "DatumRodjenjaPicker";
            this.DatumRodjenjaPicker.Size = new System.Drawing.Size(228, 22);
            this.DatumRodjenjaPicker.TabIndex = 7;
            // 
            // Dugme_DodajSudiju
            // 
            this.Dugme_DodajSudiju.BackColor = System.Drawing.Color.Transparent;
            this.Dugme_DodajSudiju.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_DodajSudiju.Location = new System.Drawing.Point(14, 309);
            this.Dugme_DodajSudiju.Name = "Dugme_DodajSudiju";
            this.Dugme_DodajSudiju.Size = new System.Drawing.Size(162, 41);
            this.Dugme_DodajSudiju.TabIndex = 10;
            this.Dugme_DodajSudiju.Text = "Potvrdi";
            this.Dugme_DodajSudiju.UseVisualStyleBackColor = false;
            this.Dugme_DodajSudiju.Click += new System.EventHandler(this.Dugme_DodajSudiju_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(75, 223);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Pol";
            // 
            // PolComboBox
            // 
            this.PolComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PolComboBox.FormattingEnabled = true;
            this.PolComboBox.Items.AddRange(new object[] {
            "Muški",
            "Ženski"});
            this.PolComboBox.Location = new System.Drawing.Point(75, 244);
            this.PolComboBox.Name = "PolComboBox";
            this.PolComboBox.Size = new System.Drawing.Size(230, 24);
            this.PolComboBox.TabIndex = 9;
            // 
            // Dugme_Odustani
            // 
            this.Dugme_Odustani.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Odustani.Location = new System.Drawing.Point(208, 310);
            this.Dugme_Odustani.Name = "Dugme_Odustani";
            this.Dugme_Odustani.Size = new System.Drawing.Size(162, 41);
            this.Dugme_Odustani.TabIndex = 11;
            this.Dugme_Odustani.Text = "Odustani";
            this.Dugme_Odustani.UseVisualStyleBackColor = true;
            this.Dugme_Odustani.Click += new System.EventHandler(this.Dugme_Odustani_Click);
            // 
            // DodajSudiju
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 403);
            this.Controls.Add(this.Dugme_Odustani);
            this.Controls.Add(this.PolComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Dugme_DodajSudiju);
            this.Controls.Add(this.DatumRodjenjaPicker);
            this.Controls.Add(this.BrojSudijskeLicenceTextBox);
            this.Controls.Add(this.PrezimeTextBox);
            this.Controls.Add(this.ImeTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DodajSudiju";
            this.Text = "Dodaj sudiju";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ImeTextBox;
        private System.Windows.Forms.TextBox PrezimeTextBox;
        private System.Windows.Forms.TextBox BrojSudijskeLicenceTextBox;
        private System.Windows.Forms.DateTimePicker DatumRodjenjaPicker;
        private System.Windows.Forms.Button Dugme_DodajSudiju;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox PolComboBox;
        private System.Windows.Forms.Button Dugme_Odustani;
    }
}