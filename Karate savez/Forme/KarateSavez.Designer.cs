﻿namespace Karate_savez
{
    partial class KarateSavez
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KarateSavez));
            this.ksLabela = new System.Windows.Forms.Label();
            this.grupa = new System.Windows.Forms.GroupBox();
            this.Dugme_Klubovi = new System.Windows.Forms.Button();
            this.Dugme_Takmicenja = new System.Windows.Forms.Button();
            this.Dugme_Sudije = new System.Windows.Forms.Button();
            this.Dugme_Takmicari = new System.Windows.Forms.Button();
            this.Dugme_Sastanci = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Dugme_Odbori = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Dugme_Zatvori = new System.Windows.Forms.Button();
            this.grupa.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // ksLabela
            // 
            this.ksLabela.AutoSize = true;
            this.ksLabela.Location = new System.Drawing.Point(12, 9);
            this.ksLabela.Name = "ksLabela";
            this.ksLabela.Size = new System.Drawing.Size(208, 17);
            this.ksLabela.TabIndex = 0;
            this.ksLabela.Text = "Karate Savez Republike Srpske";
            // 
            // grupa
            // 
            this.grupa.Controls.Add(this.Dugme_Klubovi);
            this.grupa.Controls.Add(this.Dugme_Takmicenja);
            this.grupa.Controls.Add(this.Dugme_Sudije);
            this.grupa.Controls.Add(this.Dugme_Takmicari);
            this.grupa.Location = new System.Drawing.Point(15, 46);
            this.grupa.Name = "grupa";
            this.grupa.Size = new System.Drawing.Size(356, 151);
            this.grupa.TabIndex = 1;
            this.grupa.TabStop = false;
            this.grupa.Text = "Meni";
            // 
            // Dugme_Klubovi
            // 
            this.Dugme_Klubovi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Klubovi.Location = new System.Drawing.Point(19, 21);
            this.Dugme_Klubovi.Name = "Dugme_Klubovi";
            this.Dugme_Klubovi.Size = new System.Drawing.Size(136, 44);
            this.Dugme_Klubovi.TabIndex = 0;
            this.Dugme_Klubovi.Text = "Klubovi";
            this.Dugme_Klubovi.UseVisualStyleBackColor = true;
            this.Dugme_Klubovi.Click += new System.EventHandler(this.Dugme_Klubovi_Click);
            // 
            // Dugme_Takmicenja
            // 
            this.Dugme_Takmicenja.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Takmicenja.Location = new System.Drawing.Point(203, 21);
            this.Dugme_Takmicenja.Name = "Dugme_Takmicenja";
            this.Dugme_Takmicenja.Size = new System.Drawing.Size(136, 44);
            this.Dugme_Takmicenja.TabIndex = 1;
            this.Dugme_Takmicenja.Text = "Takmičenja";
            this.Dugme_Takmicenja.UseVisualStyleBackColor = true;
            this.Dugme_Takmicenja.Click += new System.EventHandler(this.Dugme_Takmicenja_Click);
            // 
            // Dugme_Sudije
            // 
            this.Dugme_Sudije.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Sudije.Location = new System.Drawing.Point(19, 84);
            this.Dugme_Sudije.Name = "Dugme_Sudije";
            this.Dugme_Sudije.Size = new System.Drawing.Size(136, 45);
            this.Dugme_Sudije.TabIndex = 2;
            this.Dugme_Sudije.Text = "Sudije";
            this.Dugme_Sudije.UseVisualStyleBackColor = true;
            this.Dugme_Sudije.Click += new System.EventHandler(this.Dugme_Sudije_Click);
            // 
            // Dugme_Takmicari
            // 
            this.Dugme_Takmicari.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Takmicari.Location = new System.Drawing.Point(203, 84);
            this.Dugme_Takmicari.Name = "Dugme_Takmicari";
            this.Dugme_Takmicari.Size = new System.Drawing.Size(136, 45);
            this.Dugme_Takmicari.TabIndex = 3;
            this.Dugme_Takmicari.Text = "Takmičari";
            this.Dugme_Takmicari.UseVisualStyleBackColor = true;
            this.Dugme_Takmicari.Click += new System.EventHandler(this.Dugme_Takmicari_Click);
            // 
            // Dugme_Sastanci
            // 
            this.Dugme_Sastanci.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Sastanci.Location = new System.Drawing.Point(203, 32);
            this.Dugme_Sastanci.Name = "Dugme_Sastanci";
            this.Dugme_Sastanci.Size = new System.Drawing.Size(136, 45);
            this.Dugme_Sastanci.TabIndex = 1;
            this.Dugme_Sastanci.Text = "Sastanci";
            this.Dugme_Sastanci.UseVisualStyleBackColor = true;
            this.Dugme_Sastanci.Click += new System.EventHandler(this.Dugme_Sastanci_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Dugme_Sastanci);
            this.groupBox1.Controls.Add(this.Dugme_Odbori);
            this.groupBox1.Location = new System.Drawing.Point(15, 289);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(356, 98);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Unutrašnja kontrola";
            // 
            // Dugme_Odbori
            // 
            this.Dugme_Odbori.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Odbori.Location = new System.Drawing.Point(19, 32);
            this.Dugme_Odbori.Name = "Dugme_Odbori";
            this.Dugme_Odbori.Size = new System.Drawing.Size(136, 45);
            this.Dugme_Odbori.TabIndex = 0;
            this.Dugme_Odbori.Text = "Odbori";
            this.Dugme_Odbori.UseVisualStyleBackColor = true;
            this.Dugme_Odbori.Click += new System.EventHandler(this.Dugme_Odbori_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(387, 29);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(380, 358);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // Dugme_Zatvori
            // 
            this.Dugme_Zatvori.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Zatvori.Location = new System.Drawing.Point(631, 393);
            this.Dugme_Zatvori.Name = "Dugme_Zatvori";
            this.Dugme_Zatvori.Size = new System.Drawing.Size(136, 45);
            this.Dugme_Zatvori.TabIndex = 3;
            this.Dugme_Zatvori.Text = "Zatvori";
            this.Dugme_Zatvori.UseVisualStyleBackColor = true;
            this.Dugme_Zatvori.Click += new System.EventHandler(this.Dugme_Zatvori_Click);
            // 
            // KarateSavez
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Dugme_Zatvori);
            this.Controls.Add(this.grupa);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.ksLabela);
            this.MaximizeBox = false;
            this.Name = "KarateSavez";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Karate savez";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.KarateSavez_FormClosed);
            this.grupa.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ksLabela;
        private System.Windows.Forms.GroupBox grupa;
        private System.Windows.Forms.Button Dugme_Takmicari;
        private System.Windows.Forms.Button Dugme_Klubovi;
        private System.Windows.Forms.Button Dugme_Takmicenja;
        private System.Windows.Forms.Button Dugme_Sudije;
        private System.Windows.Forms.Button Dugme_Sastanci;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button Dugme_Odbori;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button Dugme_Zatvori;
    }
}

