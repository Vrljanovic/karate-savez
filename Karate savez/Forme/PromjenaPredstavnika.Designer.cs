﻿namespace Karate_savez.Forme
{
    partial class PromjenaPredstavnika
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.ImeTextBox = new System.Windows.Forms.TextBox();
            this.PrezimeTextBox = new System.Windows.Forms.TextBox();
            this.EmailAdresaTextBox = new System.Windows.Forms.TextBox();
            this.BrojTelefonaTextBox = new System.Windows.Forms.TextBox();
            this.Label_Info = new System.Windows.Forms.Label();
            this.Dugme_Potvrdi = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.PolComboBox = new System.Windows.Forms.ComboBox();
            this.Dugme_Odustani = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 84);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Ime";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(45, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Prezime";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(45, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Email adresa";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(45, 219);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Broj telefona";
            // 
            // ImeTextBox
            // 
            this.ImeTextBox.Location = new System.Drawing.Point(47, 104);
            this.ImeTextBox.Name = "ImeTextBox";
            this.ImeTextBox.Size = new System.Drawing.Size(282, 22);
            this.ImeTextBox.TabIndex = 2;
            // 
            // PrezimeTextBox
            // 
            this.PrezimeTextBox.Location = new System.Drawing.Point(48, 149);
            this.PrezimeTextBox.Name = "PrezimeTextBox";
            this.PrezimeTextBox.Size = new System.Drawing.Size(282, 22);
            this.PrezimeTextBox.TabIndex = 4;
            // 
            // EmailAdresaTextBox
            // 
            this.EmailAdresaTextBox.Location = new System.Drawing.Point(48, 194);
            this.EmailAdresaTextBox.Name = "EmailAdresaTextBox";
            this.EmailAdresaTextBox.Size = new System.Drawing.Size(282, 22);
            this.EmailAdresaTextBox.TabIndex = 6;
            // 
            // BrojTelefonaTextBox
            // 
            this.BrojTelefonaTextBox.Location = new System.Drawing.Point(48, 239);
            this.BrojTelefonaTextBox.Name = "BrojTelefonaTextBox";
            this.BrojTelefonaTextBox.Size = new System.Drawing.Size(282, 22);
            this.BrojTelefonaTextBox.TabIndex = 8;
            // 
            // Label_Info
            // 
            this.Label_Info.AutoSize = true;
            this.Label_Info.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_Info.Location = new System.Drawing.Point(43, 41);
            this.Label_Info.Name = "Label_Info";
            this.Label_Info.Size = new System.Drawing.Size(0, 25);
            this.Label_Info.TabIndex = 0;
            // 
            // Dugme_Potvrdi
            // 
            this.Dugme_Potvrdi.BackColor = System.Drawing.Color.Transparent;
            this.Dugme_Potvrdi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_Potvrdi.Location = new System.Drawing.Point(35, 354);
            this.Dugme_Potvrdi.Name = "Dugme_Potvrdi";
            this.Dugme_Potvrdi.Size = new System.Drawing.Size(143, 48);
            this.Dugme_Potvrdi.TabIndex = 11;
            this.Dugme_Potvrdi.Text = "Potvrdi";
            this.Dugme_Potvrdi.UseVisualStyleBackColor = false;
            this.Dugme_Potvrdi.Click += new System.EventHandler(this.Dugme_Potvrdi_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(45, 264);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Pol";
            // 
            // PolComboBox
            // 
            this.PolComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PolComboBox.FormattingEnabled = true;
            this.PolComboBox.Items.AddRange(new object[] {
            "Muški",
            "Ženski"});
            this.PolComboBox.Location = new System.Drawing.Point(47, 284);
            this.PolComboBox.Name = "PolComboBox";
            this.PolComboBox.Size = new System.Drawing.Size(283, 24);
            this.PolComboBox.TabIndex = 10;
            // 
            // Dugme_Odustani
            // 
            this.Dugme_Odustani.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Odustani.Location = new System.Drawing.Point(201, 355);
            this.Dugme_Odustani.Name = "Dugme_Odustani";
            this.Dugme_Odustani.Size = new System.Drawing.Size(143, 48);
            this.Dugme_Odustani.TabIndex = 12;
            this.Dugme_Odustani.Text = "Odustani";
            this.Dugme_Odustani.UseVisualStyleBackColor = true;
            this.Dugme_Odustani.Click += new System.EventHandler(this.Dugme_Odustani_Click);
            // 
            // PromjenaPredstavnika
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 453);
            this.Controls.Add(this.Dugme_Odustani);
            this.Controls.Add(this.PolComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Dugme_Potvrdi);
            this.Controls.Add(this.Label_Info);
            this.Controls.Add(this.BrojTelefonaTextBox);
            this.Controls.Add(this.EmailAdresaTextBox);
            this.Controls.Add(this.PrezimeTextBox);
            this.Controls.Add(this.ImeTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PromjenaPredstavnika";
            this.Text = "Promjena predstavnika";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox ImeTextBox;
        private System.Windows.Forms.TextBox PrezimeTextBox;
        private System.Windows.Forms.TextBox EmailAdresaTextBox;
        private System.Windows.Forms.TextBox BrojTelefonaTextBox;
        private System.Windows.Forms.Label Label_Info;
        private System.Windows.Forms.Button Dugme_Potvrdi;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox PolComboBox;
        private System.Windows.Forms.Button Dugme_Odustani;
    }
}