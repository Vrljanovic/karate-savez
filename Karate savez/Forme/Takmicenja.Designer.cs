﻿namespace Karate_savez.Forme
{
    partial class Takmicenja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TabelaTakmicenja = new System.Windows.Forms.DataGridView();
            this.IdTakmicenja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DatumOdrzavanja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PocetakTakmicenja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MjestoOdrzavanja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BrojBorilista = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dugme_DodajTakmicenje = new System.Windows.Forms.Button();
            this.TakmicenjaCheckBox = new System.Windows.Forms.CheckBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Dugme_RezultatiTakmicenja = new System.Windows.Forms.Button();
            this.Dugme_DelegirajSudije = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Pretraga_TextBox = new System.Windows.Forms.TextBox();
            this.Dugme_Nazad = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TabelaTakmicenja)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabelaTakmicenja
            // 
            this.TabelaTakmicenja.AllowUserToAddRows = false;
            this.TabelaTakmicenja.AllowUserToDeleteRows = false;
            this.TabelaTakmicenja.AllowUserToResizeColumns = false;
            this.TabelaTakmicenja.AllowUserToResizeRows = false;
            this.TabelaTakmicenja.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabelaTakmicenja.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.TabelaTakmicenja.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdTakmicenja,
            this.DatumOdrzavanja,
            this.PocetakTakmicenja,
            this.MjestoOdrzavanja,
            this.BrojBorilista});
            this.TabelaTakmicenja.Location = new System.Drawing.Point(0, 83);
            this.TabelaTakmicenja.MultiSelect = false;
            this.TabelaTakmicenja.Name = "TabelaTakmicenja";
            this.TabelaTakmicenja.RowHeadersVisible = false;
            this.TabelaTakmicenja.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.TabelaTakmicenja.RowTemplate.Height = 24;
            this.TabelaTakmicenja.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TabelaTakmicenja.Size = new System.Drawing.Size(800, 455);
            this.TabelaTakmicenja.TabIndex = 2;
            this.TabelaTakmicenja.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.TabelaTakmicenja_RowEnter);
            // 
            // IdTakmicenja
            // 
            this.IdTakmicenja.HeaderText = "ID takmičenja";
            this.IdTakmicenja.Name = "IdTakmicenja";
            this.IdTakmicenja.ReadOnly = true;
            this.IdTakmicenja.Visible = false;
            // 
            // DatumOdrzavanja
            // 
            this.DatumOdrzavanja.HeaderText = "Datum održavanja";
            this.DatumOdrzavanja.Name = "DatumOdrzavanja";
            this.DatumOdrzavanja.ReadOnly = true;
            this.DatumOdrzavanja.Width = 120;
            // 
            // PocetakTakmicenja
            // 
            this.PocetakTakmicenja.HeaderText = "Početak takmičenja";
            this.PocetakTakmicenja.Name = "PocetakTakmicenja";
            this.PocetakTakmicenja.ReadOnly = true;
            // 
            // MjestoOdrzavanja
            // 
            this.MjestoOdrzavanja.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MjestoOdrzavanja.HeaderText = "Mjesto održavanja";
            this.MjestoOdrzavanja.Name = "MjestoOdrzavanja";
            this.MjestoOdrzavanja.ReadOnly = true;
            // 
            // BrojBorilista
            // 
            this.BrojBorilista.HeaderText = "Broj borilišta";
            this.BrojBorilista.Name = "BrojBorilista";
            this.BrojBorilista.ReadOnly = true;
            // 
            // Dugme_DodajTakmicenje
            // 
            this.Dugme_DodajTakmicenje.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Dugme_DodajTakmicenje.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_DodajTakmicenje.Location = new System.Drawing.Point(567, 602);
            this.Dugme_DodajTakmicenje.Name = "Dugme_DodajTakmicenje";
            this.Dugme_DodajTakmicenje.Size = new System.Drawing.Size(203, 44);
            this.Dugme_DodajTakmicenje.TabIndex = 5;
            this.Dugme_DodajTakmicenje.Text = "Zakaži takmičenje";
            this.Dugme_DodajTakmicenje.UseVisualStyleBackColor = true;
            this.Dugme_DodajTakmicenje.Click += new System.EventHandler(this.Dugme_DodajTakmicenje_Click);
            // 
            // TakmicenjaCheckBox
            // 
            this.TakmicenjaCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TakmicenjaCheckBox.AutoSize = true;
            this.TakmicenjaCheckBox.Location = new System.Drawing.Point(12, 544);
            this.TakmicenjaCheckBox.Name = "TakmicenjaCheckBox";
            this.TakmicenjaCheckBox.Size = new System.Drawing.Size(237, 21);
            this.TakmicenjaCheckBox.TabIndex = 3;
            this.TakmicenjaCheckBox.Text = "Prikaži samo održana takmičenja";
            this.TakmicenjaCheckBox.UseVisualStyleBackColor = true;
            this.TakmicenjaCheckBox.CheckedChanged += new System.EventHandler(this.TakmicenjaCheckBox_CheckedChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 681);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(782, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox2.Controls.Add(this.Dugme_RezultatiTakmicenja);
            this.groupBox2.Controls.Add(this.Dugme_DelegirajSudije);
            this.groupBox2.Location = new System.Drawing.Point(12, 581);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(431, 86);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Opcije za zakazana takmičenja";
            // 
            // Dugme_RezultatiTakmicenja
            // 
            this.Dugme_RezultatiTakmicenja.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_RezultatiTakmicenja.Location = new System.Drawing.Point(222, 21);
            this.Dugme_RezultatiTakmicenja.Name = "Dugme_RezultatiTakmicenja";
            this.Dugme_RezultatiTakmicenja.Size = new System.Drawing.Size(203, 44);
            this.Dugme_RezultatiTakmicenja.TabIndex = 1;
            this.Dugme_RezultatiTakmicenja.Text = "Rezultati takmičenja";
            this.Dugme_RezultatiTakmicenja.UseVisualStyleBackColor = true;
            this.Dugme_RezultatiTakmicenja.Click += new System.EventHandler(this.Dugme_RezultatiTakmicenja_Click);
            // 
            // Dugme_DelegirajSudije
            // 
            this.Dugme_DelegirajSudije.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_DelegirajSudije.Location = new System.Drawing.Point(13, 21);
            this.Dugme_DelegirajSudije.Name = "Dugme_DelegirajSudije";
            this.Dugme_DelegirajSudije.Size = new System.Drawing.Size(203, 44);
            this.Dugme_DelegirajSudije.TabIndex = 0;
            this.Dugme_DelegirajSudije.Text = "Delegiraj sudije";
            this.Dugme_DelegirajSudije.UseVisualStyleBackColor = true;
            this.Dugme_DelegirajSudije.Click += new System.EventHandler(this.Dugme_DelegirajSudije_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(7, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pretraži po mjestu:";
            // 
            // Pretraga_TextBox
            // 
            this.Pretraga_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F);
            this.Pretraga_TextBox.Location = new System.Drawing.Point(203, 26);
            this.Pretraga_TextBox.Name = "Pretraga_TextBox";
            this.Pretraga_TextBox.Size = new System.Drawing.Size(400, 38);
            this.Pretraga_TextBox.TabIndex = 1;
            this.Pretraga_TextBox.TextChanged += new System.EventHandler(this.Pretraga_TextBox_TextChanged);
            // 
            // Dugme_Nazad
            // 
            this.Dugme_Nazad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Dugme_Nazad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Nazad.Location = new System.Drawing.Point(630, 26);
            this.Dugme_Nazad.Name = "Dugme_Nazad";
            this.Dugme_Nazad.Size = new System.Drawing.Size(140, 38);
            this.Dugme_Nazad.TabIndex = 7;
            this.Dugme_Nazad.Text = "Nazad";
            this.Dugme_Nazad.UseVisualStyleBackColor = true;
            this.Dugme_Nazad.Click += new System.EventHandler(this.Dugme_Nazad_Click);
            // 
            // Takmicenja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 703);
            this.Controls.Add(this.Dugme_Nazad);
            this.Controls.Add(this.Pretraga_TextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.Dugme_DodajTakmicenje);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.TakmicenjaCheckBox);
            this.Controls.Add(this.TabelaTakmicenja);
            this.MinimumSize = new System.Drawing.Size(800, 750);
            this.Name = "Takmicenja";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Takmičenja";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Takmicenja_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.TabelaTakmicenja)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView TabelaTakmicenja;
        private System.Windows.Forms.Button Dugme_DodajTakmicenje;
        private System.Windows.Forms.CheckBox TakmicenjaCheckBox;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button Dugme_DelegirajSudije;
        private System.Windows.Forms.Button Dugme_RezultatiTakmicenja;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Pretraga_TextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdTakmicenja;
        private System.Windows.Forms.DataGridViewTextBoxColumn DatumOdrzavanja;
        private System.Windows.Forms.DataGridViewTextBoxColumn PocetakTakmicenja;
        private System.Windows.Forms.DataGridViewTextBoxColumn MjestoOdrzavanja;
        private System.Windows.Forms.DataGridViewTextBoxColumn BrojBorilista;
        private System.Windows.Forms.Button Dugme_Nazad;
    }
}