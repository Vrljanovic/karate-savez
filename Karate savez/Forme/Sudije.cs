﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class Sudije : Form
    {
        private  Form pocetnaForm;
        public Sudije(Form pocetnaForm)
        {
            this.pocetnaForm = pocetnaForm;
            InitializeComponent();
            PopuniTabelu(null);
        }

        private void Sudije_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.pocetnaForm.Show();
        }

        private void PopuniTabelu(List<Sudija> sudije)
        {
            if(sudije == null)
                sudije = new SudijaDAO().DohvatiSveSudije();
            foreach (Sudija s in sudije)
                TabelaSudija.Rows.Add(s.BrojSudijskeLicence, s.Pol, s.JeAktivan ? "Da" : "Ne", s.Ime, s.Prezime, s.DatumRodjenja.ToLongDateString());
        }

        private void Dugme_DodajSudiju_Click(object sender, EventArgs e)
        {
            new DodajSudiju(this.StatusLabel).ShowDialog();
            TabelaSudija.Rows.Clear();
            PopuniTabelu(null);
        }

        private void Dugme_PromijeniAktivnost_Click(object sender, EventArgs e)
        {
            if (TabelaSudija.SelectedRows.Count > 0)
            {
                SudijaDAO sudijaDAO = new SudijaDAO();
                Sudija sudija = sudijaDAO.DohvatiSudijuPoBrojuLicence((string)TabelaSudija.SelectedRows[0].Cells[0].Value);
                if (sudijaDAO.PromjeniAktivnost(sudija))
                {
                    StatusLabel.Text = "Aktivnost sudije uspješno promjenjena.";
                    StatusLabel.ForeColor = Color.Green;
                }
                else
                {
                    StatusLabel.Text = "Neuspješno mijenjanje aktivnosti sudije";
                    StatusLabel.ForeColor = Color.Red;
                }
                TabelaSudija.Rows.Clear();
                PopuniTabelu(null);
            }
        }

        private void Pretraga_TextBox_TextChanged(object sender, EventArgs e)
        {
            string str = Pretraga_TextBox.Text;
            TabelaSudija.Rows.Clear();
            if (string.Empty.Equals(str))
                PopuniTabelu(null);
            else
                PopuniTabelu(new SudijaDAO().DohvatiPoImenu(str));
        }

        private void Dugme_Nazad_Click(object sender, EventArgs e)
        {
            this.pocetnaForm.Show();
            this.Close();
        }
    }
}