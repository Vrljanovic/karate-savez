﻿namespace Karate_savez
{
    partial class KlubInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MjestoTextBox = new System.Windows.Forms.TextBox();
            this.AdresaTextBox = new System.Windows.Forms.TextBox();
            this.JIBTextBox = new System.Windows.Forms.TextBox();
            this.NazivKlubaTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PolComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.EmailAdresaTextBox = new System.Windows.Forms.TextBox();
            this.BrojTelefonaTextBox = new System.Windows.Forms.TextBox();
            this.PrezimeTextBox = new System.Windows.Forms.TextBox();
            this.ImeTextBox = new System.Windows.Forms.TextBox();
            this.Dugme_DodajKlub = new System.Windows.Forms.Button();
            this.Dugme_Odustani = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // MjestoTextBox
            // 
            this.MjestoTextBox.Location = new System.Drawing.Point(32, 144);
            this.MjestoTextBox.Name = "MjestoTextBox";
            this.MjestoTextBox.Size = new System.Drawing.Size(200, 22);
            this.MjestoTextBox.TabIndex = 5;
            // 
            // AdresaTextBox
            // 
            this.AdresaTextBox.Location = new System.Drawing.Point(32, 189);
            this.AdresaTextBox.Name = "AdresaTextBox";
            this.AdresaTextBox.Size = new System.Drawing.Size(200, 22);
            this.AdresaTextBox.TabIndex = 7;
            // 
            // JIBTextBox
            // 
            this.JIBTextBox.Location = new System.Drawing.Point(32, 50);
            this.JIBTextBox.Name = "JIBTextBox";
            this.JIBTextBox.Size = new System.Drawing.Size(200, 22);
            this.JIBTextBox.TabIndex = 1;
            // 
            // NazivKlubaTextBox
            // 
            this.NazivKlubaTextBox.Location = new System.Drawing.Point(32, 99);
            this.NazivKlubaTextBox.Name = "NazivKlubaTextBox";
            this.NazivKlubaTextBox.Size = new System.Drawing.Size(200, 22);
            this.NazivKlubaTextBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(200, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Jedinstveni identifikacioni broj:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Naziv Kluba:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Mjesto:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Adresa:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.NazivKlubaTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.AdresaTextBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.JIBTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.MjestoTextBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(270, 274);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Podaci o klubu";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.PolComboBox);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.EmailAdresaTextBox);
            this.groupBox2.Controls.Add(this.BrojTelefonaTextBox);
            this.groupBox2.Controls.Add(this.PrezimeTextBox);
            this.groupBox2.Controls.Add(this.ImeTextBox);
            this.groupBox2.Location = new System.Drawing.Point(300, 33);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(270, 274);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Podaci o predstavniku kluba";
            // 
            // PolComboBox
            // 
            this.PolComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PolComboBox.FormattingEnabled = true;
            this.PolComboBox.Items.AddRange(new object[] {
            "Muški",
            "Ženski"});
            this.PolComboBox.Location = new System.Drawing.Point(40, 234);
            this.PolComboBox.Name = "PolComboBox";
            this.PolComboBox.Size = new System.Drawing.Size(197, 24);
            this.PolComboBox.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(37, 214);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(28, 17);
            this.label9.TabIndex = 8;
            this.label9.Text = "Pol";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(34, 169);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(95, 17);
            this.label8.TabIndex = 6;
            this.label8.Text = "Email Adresa:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(34, 124);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 17);
            this.label7.TabIndex = 4;
            this.label7.Text = "Broj Telefona:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(34, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 17);
            this.label6.TabIndex = 2;
            this.label6.Text = "Prezime:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(34, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Ime:";
            // 
            // EmailAdresaTextBox
            // 
            this.EmailAdresaTextBox.Location = new System.Drawing.Point(37, 189);
            this.EmailAdresaTextBox.Name = "EmailAdresaTextBox";
            this.EmailAdresaTextBox.Size = new System.Drawing.Size(200, 22);
            this.EmailAdresaTextBox.TabIndex = 7;
            // 
            // BrojTelefonaTextBox
            // 
            this.BrojTelefonaTextBox.Location = new System.Drawing.Point(37, 144);
            this.BrojTelefonaTextBox.Name = "BrojTelefonaTextBox";
            this.BrojTelefonaTextBox.Size = new System.Drawing.Size(200, 22);
            this.BrojTelefonaTextBox.TabIndex = 5;
            // 
            // PrezimeTextBox
            // 
            this.PrezimeTextBox.Location = new System.Drawing.Point(37, 99);
            this.PrezimeTextBox.Name = "PrezimeTextBox";
            this.PrezimeTextBox.Size = new System.Drawing.Size(200, 22);
            this.PrezimeTextBox.TabIndex = 3;
            // 
            // ImeTextBox
            // 
            this.ImeTextBox.Location = new System.Drawing.Point(37, 50);
            this.ImeTextBox.Name = "ImeTextBox";
            this.ImeTextBox.Size = new System.Drawing.Size(200, 22);
            this.ImeTextBox.TabIndex = 1;
            // 
            // Dugme_DodajKlub
            // 
            this.Dugme_DodajKlub.BackColor = System.Drawing.Color.Transparent;
            this.Dugme_DodajKlub.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_DodajKlub.Location = new System.Drawing.Point(44, 332);
            this.Dugme_DodajKlub.Name = "Dugme_DodajKlub";
            this.Dugme_DodajKlub.Size = new System.Drawing.Size(200, 43);
            this.Dugme_DodajKlub.TabIndex = 2;
            this.Dugme_DodajKlub.Text = "Dodaj klub";
            this.Dugme_DodajKlub.UseVisualStyleBackColor = false;
            this.Dugme_DodajKlub.Click += new System.EventHandler(this.Dugme_DodajKlub_Click);
            // 
            // Dugme_Odustani
            // 
            this.Dugme_Odustani.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Odustani.Location = new System.Drawing.Point(337, 332);
            this.Dugme_Odustani.Name = "Dugme_Odustani";
            this.Dugme_Odustani.Size = new System.Drawing.Size(200, 43);
            this.Dugme_Odustani.TabIndex = 3;
            this.Dugme_Odustani.Text = "Odustani";
            this.Dugme_Odustani.UseVisualStyleBackColor = true;
            this.Dugme_Odustani.Click += new System.EventHandler(this.Dugme_Odustani_Click);
            // 
            // KlubInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(582, 403);
            this.Controls.Add(this.Dugme_Odustani);
            this.Controls.Add(this.Dugme_DodajKlub);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "KlubInfo";
            this.Text = "Dodaj klub";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox MjestoTextBox;
        private System.Windows.Forms.TextBox AdresaTextBox;
        private System.Windows.Forms.TextBox JIBTextBox;
        private System.Windows.Forms.TextBox NazivKlubaTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox EmailAdresaTextBox;
        private System.Windows.Forms.TextBox BrojTelefonaTextBox;
        private System.Windows.Forms.TextBox PrezimeTextBox;
        private System.Windows.Forms.TextBox ImeTextBox;
        public System.Windows.Forms.Button Dugme_DodajKlub;
        private System.Windows.Forms.ComboBox PolComboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button Dugme_Odustani;
    }
}