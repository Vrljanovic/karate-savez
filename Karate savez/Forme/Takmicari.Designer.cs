﻿namespace Karate_savez.Forme
{
    partial class Takmicari
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TabelaTakmicara = new System.Windows.Forms.DataGridView();
            this.BrojRegistracijeUSavezu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Aktivan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Prezime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DatumRodjenja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pojas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Klub = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Dugme_PromijeniKlub = new System.Windows.Forms.Button();
            this.Dugme_PromijeniPojas = new System.Windows.Forms.Button();
            this.Dugme_PromijeniAktivnost = new System.Windows.Forms.Button();
            this.Dugme_DodajTakmicara = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.Dugme_Rezultati = new System.Windows.Forms.Button();
            this.Pretraga_ComboBox = new System.Windows.Forms.ComboBox();
            this.Pretraga_TextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Dugme_Nazad = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TabelaTakmicara)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabelaTakmicara
            // 
            this.TabelaTakmicara.AllowUserToAddRows = false;
            this.TabelaTakmicara.AllowUserToDeleteRows = false;
            this.TabelaTakmicara.AllowUserToResizeColumns = false;
            this.TabelaTakmicara.AllowUserToResizeRows = false;
            this.TabelaTakmicara.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabelaTakmicara.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TabelaTakmicara.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BrojRegistracijeUSavezu,
            this.Pol,
            this.Aktivan,
            this.Ime,
            this.Prezime,
            this.DatumRodjenja,
            this.Pojas,
            this.Klub});
            this.TabelaTakmicara.Location = new System.Drawing.Point(0, 83);
            this.TabelaTakmicara.MultiSelect = false;
            this.TabelaTakmicara.Name = "TabelaTakmicara";
            this.TabelaTakmicara.ReadOnly = true;
            this.TabelaTakmicara.RowHeadersVisible = false;
            this.TabelaTakmicara.RowTemplate.Height = 24;
            this.TabelaTakmicara.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TabelaTakmicara.Size = new System.Drawing.Size(1550, 350);
            this.TabelaTakmicara.TabIndex = 3;
            this.TabelaTakmicara.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.TabelaTakmicara_RowEnter);
            // 
            // BrojRegistracijeUSavezu
            // 
            this.BrojRegistracijeUSavezu.HeaderText = "Broj registracije u savezu";
            this.BrojRegistracijeUSavezu.Name = "BrojRegistracijeUSavezu";
            this.BrojRegistracijeUSavezu.ReadOnly = true;
            this.BrojRegistracijeUSavezu.Width = 200;
            // 
            // Pol
            // 
            this.Pol.HeaderText = "Pol";
            this.Pol.Name = "Pol";
            this.Pol.ReadOnly = true;
            this.Pol.Width = 50;
            // 
            // Aktivan
            // 
            this.Aktivan.HeaderText = "Aktivan";
            this.Aktivan.Name = "Aktivan";
            this.Aktivan.ReadOnly = true;
            this.Aktivan.Width = 50;
            // 
            // Ime
            // 
            this.Ime.HeaderText = "Ime";
            this.Ime.Name = "Ime";
            this.Ime.ReadOnly = true;
            this.Ime.Width = 200;
            // 
            // Prezime
            // 
            this.Prezime.HeaderText = "Prezime";
            this.Prezime.Name = "Prezime";
            this.Prezime.ReadOnly = true;
            this.Prezime.Width = 200;
            // 
            // DatumRodjenja
            // 
            this.DatumRodjenja.HeaderText = "Datum rodjenja";
            this.DatumRodjenja.Name = "DatumRodjenja";
            this.DatumRodjenja.ReadOnly = true;
            this.DatumRodjenja.Width = 150;
            // 
            // Pojas
            // 
            this.Pojas.HeaderText = "Pojas";
            this.Pojas.Name = "Pojas";
            this.Pojas.ReadOnly = true;
            // 
            // Klub
            // 
            this.Klub.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Klub.HeaderText = "Klub";
            this.Klub.Name = "Klub";
            this.Klub.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.Dugme_PromijeniKlub);
            this.groupBox1.Controls.Add(this.Dugme_PromijeniPojas);
            this.groupBox1.Controls.Add(this.Dugme_PromijeniAktivnost);
            this.groupBox1.Controls.Add(this.Dugme_DodajTakmicara);
            this.groupBox1.Location = new System.Drawing.Point(12, 439);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(969, 89);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Opcije";
            // 
            // Dugme_PromijeniKlub
            // 
            this.Dugme_PromijeniKlub.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Dugme_PromijeniKlub.BackColor = System.Drawing.Color.Transparent;
            this.Dugme_PromijeniKlub.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_PromijeniKlub.Location = new System.Drawing.Point(495, 19);
            this.Dugme_PromijeniKlub.Name = "Dugme_PromijeniKlub";
            this.Dugme_PromijeniKlub.Size = new System.Drawing.Size(215, 44);
            this.Dugme_PromijeniKlub.TabIndex = 2;
            this.Dugme_PromijeniKlub.Text = "Promijeni klub";
            this.Dugme_PromijeniKlub.UseVisualStyleBackColor = false;
            this.Dugme_PromijeniKlub.Click += new System.EventHandler(this.Dugme_PromijeniKlub_Click);
            // 
            // Dugme_PromijeniPojas
            // 
            this.Dugme_PromijeniPojas.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Dugme_PromijeniPojas.BackColor = System.Drawing.Color.Transparent;
            this.Dugme_PromijeniPojas.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_PromijeniPojas.Location = new System.Drawing.Point(258, 20);
            this.Dugme_PromijeniPojas.Name = "Dugme_PromijeniPojas";
            this.Dugme_PromijeniPojas.Size = new System.Drawing.Size(215, 44);
            this.Dugme_PromijeniPojas.TabIndex = 1;
            this.Dugme_PromijeniPojas.Text = "Promijeni pojas";
            this.Dugme_PromijeniPojas.UseVisualStyleBackColor = false;
            this.Dugme_PromijeniPojas.Click += new System.EventHandler(this.Dugme_PromijeniPojas_Click);
            // 
            // Dugme_PromijeniAktivnost
            // 
            this.Dugme_PromijeniAktivnost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Dugme_PromijeniAktivnost.BackColor = System.Drawing.Color.Transparent;
            this.Dugme_PromijeniAktivnost.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_PromijeniAktivnost.Location = new System.Drawing.Point(732, 18);
            this.Dugme_PromijeniAktivnost.Name = "Dugme_PromijeniAktivnost";
            this.Dugme_PromijeniAktivnost.Size = new System.Drawing.Size(215, 45);
            this.Dugme_PromijeniAktivnost.TabIndex = 3;
            this.Dugme_PromijeniAktivnost.Text = "Promijeni aktivnost";
            this.Dugme_PromijeniAktivnost.UseVisualStyleBackColor = false;
            this.Dugme_PromijeniAktivnost.Click += new System.EventHandler(this.Dugme_PromijeniAktivnost_Click);
            // 
            // Dugme_DodajTakmicara
            // 
            this.Dugme_DodajTakmicara.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Dugme_DodajTakmicara.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_DodajTakmicara.Location = new System.Drawing.Point(22, 20);
            this.Dugme_DodajTakmicara.Name = "Dugme_DodajTakmicara";
            this.Dugme_DodajTakmicara.Size = new System.Drawing.Size(215, 45);
            this.Dugme_DodajTakmicara.TabIndex = 0;
            this.Dugme_DodajTakmicara.Text = "Dodaj takmičara";
            this.Dugme_DodajTakmicara.UseVisualStyleBackColor = true;
            this.Dugme_DodajTakmicara.Click += new System.EventHandler(this.Dugme_DodajTakmicara_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 531);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1532, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // Dugme_Rezultati
            // 
            this.Dugme_Rezultati.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Dugme_Rezultati.BackColor = System.Drawing.Color.Transparent;
            this.Dugme_Rezultati.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_Rezultati.Location = new System.Drawing.Point(1340, 459);
            this.Dugme_Rezultati.Name = "Dugme_Rezultati";
            this.Dugme_Rezultati.Size = new System.Drawing.Size(180, 44);
            this.Dugme_Rezultati.TabIndex = 5;
            this.Dugme_Rezultati.Text = "Rezultati";
            this.Dugme_Rezultati.UseVisualStyleBackColor = false;
            this.Dugme_Rezultati.Click += new System.EventHandler(this.Dugme_Rezultati_Click);
            // 
            // Pretraga_ComboBox
            // 
            this.Pretraga_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Pretraga_ComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Pretraga_ComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F);
            this.Pretraga_ComboBox.FormattingEnabled = true;
            this.Pretraga_ComboBox.Items.AddRange(new object[] {
            "Po imenu",
            "Po klubu",
            "Po pojasu"});
            this.Pretraga_ComboBox.Location = new System.Drawing.Point(8, 29);
            this.Pretraga_ComboBox.Name = "Pretraga_ComboBox";
            this.Pretraga_ComboBox.Size = new System.Drawing.Size(227, 37);
            this.Pretraga_ComboBox.TabIndex = 1;
            // 
            // Pretraga_TextBox
            // 
            this.Pretraga_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F);
            this.Pretraga_TextBox.Location = new System.Drawing.Point(261, 29);
            this.Pretraga_TextBox.Name = "Pretraga_TextBox";
            this.Pretraga_TextBox.Size = new System.Drawing.Size(400, 38);
            this.Pretraga_TextBox.TabIndex = 2;
            this.Pretraga_TextBox.TextChanged += new System.EventHandler(this.Pretraga_TextBox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pretraži takmičare:";
            // 
            // Dugme_Nazad
            // 
            this.Dugme_Nazad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Dugme_Nazad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Nazad.Location = new System.Drawing.Point(1380, 28);
            this.Dugme_Nazad.Name = "Dugme_Nazad";
            this.Dugme_Nazad.Size = new System.Drawing.Size(140, 38);
            this.Dugme_Nazad.TabIndex = 7;
            this.Dugme_Nazad.Text = "Nazad";
            this.Dugme_Nazad.UseVisualStyleBackColor = true;
            this.Dugme_Nazad.Click += new System.EventHandler(this.Dugme_Nazad_Click);
            // 
            // Takmicari
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1532, 553);
            this.Controls.Add(this.Dugme_Nazad);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Pretraga_TextBox);
            this.Controls.Add(this.Pretraga_ComboBox);
            this.Controls.Add(this.Dugme_Rezultati);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TabelaTakmicara);
            this.MinimumSize = new System.Drawing.Size(1550, 600);
            this.Name = "Takmicari";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Takmičari";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Takmicari_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.TabelaTakmicara)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView TabelaTakmicara;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button Dugme_PromijeniKlub;
        private System.Windows.Forms.Button Dugme_PromijeniPojas;
        private System.Windows.Forms.Button Dugme_PromijeniAktivnost;
        private System.Windows.Forms.Button Dugme_DodajTakmicara;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.Button Dugme_Rezultati;
        private System.Windows.Forms.ComboBox Pretraga_ComboBox;
        private System.Windows.Forms.TextBox Pretraga_TextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn BrojRegistracijeUSavezu;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Aktivan;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Prezime;
        private System.Windows.Forms.DataGridViewTextBoxColumn DatumRodjenja;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pojas;
        private System.Windows.Forms.DataGridViewTextBoxColumn Klub;
        private System.Windows.Forms.Button Dugme_Nazad;
    }
}