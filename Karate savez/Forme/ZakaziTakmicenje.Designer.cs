﻿namespace Karate_savez.Forme
{
    partial class ZakaziTakmicenje
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PocetakTakmicenjaPicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.MjestoOdrzavanjaTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.AdresaTextBox = new System.Windows.Forms.TextBox();
            this.Dugme_Potvrdi = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.BrojBorilistaTextBox = new System.Windows.Forms.TextBox();
            this.Dugme_Odustani = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PocetakTakmicenjaPicker
            // 
            this.PocetakTakmicenjaPicker.CustomFormat = "dd.MM.yyyy u hh:mm";
            this.PocetakTakmicenjaPicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.PocetakTakmicenjaPicker.Location = new System.Drawing.Point(57, 61);
            this.PocetakTakmicenjaPicker.Name = "PocetakTakmicenjaPicker";
            this.PocetakTakmicenjaPicker.Size = new System.Drawing.Size(241, 22);
            this.PocetakTakmicenjaPicker.TabIndex = 1;
            this.PocetakTakmicenjaPicker.Value = new System.DateTime(2019, 6, 11, 0, 0, 0, 0);
            this.PocetakTakmicenjaPicker.ValueChanged += new System.EventHandler(this.PocetakTakmicenjaPicker_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Početak takmičenja";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(54, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Mjesto održavanja";
            // 
            // MjestoOdrzavanjaTextBox
            // 
            this.MjestoOdrzavanjaTextBox.Location = new System.Drawing.Point(57, 106);
            this.MjestoOdrzavanjaTextBox.Name = "MjestoOdrzavanjaTextBox";
            this.MjestoOdrzavanjaTextBox.Size = new System.Drawing.Size(241, 22);
            this.MjestoOdrzavanjaTextBox.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(54, 131);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Adresa";
            // 
            // AdresaTextBox
            // 
            this.AdresaTextBox.Location = new System.Drawing.Point(57, 151);
            this.AdresaTextBox.Name = "AdresaTextBox";
            this.AdresaTextBox.Size = new System.Drawing.Size(241, 22);
            this.AdresaTextBox.TabIndex = 5;
            // 
            // Dugme_Potvrdi
            // 
            this.Dugme_Potvrdi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_Potvrdi.Location = new System.Drawing.Point(23, 248);
            this.Dugme_Potvrdi.Name = "Dugme_Potvrdi";
            this.Dugme_Potvrdi.Size = new System.Drawing.Size(145, 47);
            this.Dugme_Potvrdi.TabIndex = 8;
            this.Dugme_Potvrdi.Text = "Potvrdi";
            this.Dugme_Potvrdi.UseVisualStyleBackColor = true;
            this.Dugme_Potvrdi.Click += new System.EventHandler(this.Dugme_Potvrdi_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(54, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Broj borilišta";
            // 
            // BrojBorilistaTextBox
            // 
            this.BrojBorilistaTextBox.Location = new System.Drawing.Point(57, 196);
            this.BrojBorilistaTextBox.Name = "BrojBorilistaTextBox";
            this.BrojBorilistaTextBox.Size = new System.Drawing.Size(241, 22);
            this.BrojBorilistaTextBox.TabIndex = 7;
            // 
            // Dugme_Odustani
            // 
            this.Dugme_Odustani.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Odustani.Location = new System.Drawing.Point(190, 249);
            this.Dugme_Odustani.Name = "Dugme_Odustani";
            this.Dugme_Odustani.Size = new System.Drawing.Size(145, 47);
            this.Dugme_Odustani.TabIndex = 9;
            this.Dugme_Odustani.Text = "Odustani";
            this.Dugme_Odustani.UseVisualStyleBackColor = true;
            this.Dugme_Odustani.Click += new System.EventHandler(this.Dugme_Odustani_Click);
            // 
            // ZakaziTakmicenje
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 321);
            this.Controls.Add(this.Dugme_Odustani);
            this.Controls.Add(this.BrojBorilistaTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.Dugme_Potvrdi);
            this.Controls.Add(this.AdresaTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.MjestoOdrzavanjaTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PocetakTakmicenjaPicker);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ZakaziTakmicenje";
            this.Text = "Zakaži takmičenje";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox MjestoOdrzavanjaTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox AdresaTextBox;
        private System.Windows.Forms.Button Dugme_Potvrdi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox BrojBorilistaTextBox;
        private System.Windows.Forms.DateTimePicker PocetakTakmicenjaPicker;
        private System.Windows.Forms.Button Dugme_Odustani;
    }
}