﻿namespace Karate_savez.Forme
{
    partial class Sastanci
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TabelaSastanaka = new System.Windows.Forms.DataGridView();
            this.IdSastanka = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DatumOdrzavanja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Učesnici = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Dugme_DodajSastanak = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Pretrazi_Sastanak = new System.Windows.Forms.TextBox();
            this.Dugme_Nazad = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TabelaSastanaka)).BeginInit();
            this.SuspendLayout();
            // 
            // TabelaSastanaka
            // 
            this.TabelaSastanaka.AllowUserToAddRows = false;
            this.TabelaSastanaka.AllowUserToDeleteRows = false;
            this.TabelaSastanaka.AllowUserToOrderColumns = true;
            this.TabelaSastanaka.AllowUserToResizeColumns = false;
            this.TabelaSastanaka.AllowUserToResizeRows = false;
            this.TabelaSastanaka.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabelaSastanaka.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TabelaSastanaka.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdSastanka,
            this.DatumOdrzavanja,
            this.Time,
            this.Učesnici});
            this.TabelaSastanaka.Location = new System.Drawing.Point(1, 81);
            this.TabelaSastanaka.Name = "TabelaSastanaka";
            this.TabelaSastanaka.ReadOnly = true;
            this.TabelaSastanaka.RowHeadersVisible = false;
            this.TabelaSastanaka.RowTemplate.Height = 24;
            this.TabelaSastanaka.Size = new System.Drawing.Size(880, 345);
            this.TabelaSastanaka.TabIndex = 2;
            this.TabelaSastanaka.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TabelaSastanaka_CellContentClick);
            // 
            // IdSastanka
            // 
            this.IdSastanka.HeaderText = "IdSastanka";
            this.IdSastanka.Name = "IdSastanka";
            this.IdSastanka.ReadOnly = true;
            this.IdSastanka.Visible = false;
            // 
            // DatumOdrzavanja
            // 
            this.DatumOdrzavanja.HeaderText = "Datum održavanja";
            this.DatumOdrzavanja.Name = "DatumOdrzavanja";
            this.DatumOdrzavanja.ReadOnly = true;
            // 
            // Time
            // 
            this.Time.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Time.HeaderText = "Tema";
            this.Time.Name = "Time";
            this.Time.ReadOnly = true;
            // 
            // Učesnici
            // 
            this.Učesnici.HeaderText = "Učesnici";
            this.Učesnici.Name = "Učesnici";
            this.Učesnici.ReadOnly = true;
            this.Učesnici.Width = 150;
            // 
            // Dugme_DodajSastanak
            // 
            this.Dugme_DodajSastanak.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Dugme_DodajSastanak.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_DodajSastanak.Location = new System.Drawing.Point(620, 449);
            this.Dugme_DodajSastanak.Name = "Dugme_DodajSastanak";
            this.Dugme_DodajSastanak.Size = new System.Drawing.Size(235, 42);
            this.Dugme_DodajSastanak.TabIndex = 3;
            this.Dugme_DodajSastanak.Text = "Dodaj sastanak";
            this.Dugme_DodajSastanak.UseVisualStyleBackColor = true;
            this.Dugme_DodajSastanak.Click += new System.EventHandler(this.Dugme_DodajSastanak_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label1.Location = new System.Drawing.Point(12, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pretraži po temi: ";
            // 
            // Pretrazi_Sastanak
            // 
            this.Pretrazi_Sastanak.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F);
            this.Pretrazi_Sastanak.Location = new System.Drawing.Point(187, 20);
            this.Pretrazi_Sastanak.Name = "Pretrazi_Sastanak";
            this.Pretrazi_Sastanak.Size = new System.Drawing.Size(400, 38);
            this.Pretrazi_Sastanak.TabIndex = 1;
            this.Pretrazi_Sastanak.TextChanged += new System.EventHandler(this.Pretrazi_Sastanak_TextChanged);
            // 
            // Dugme_Nazad
            // 
            this.Dugme_Nazad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Dugme_Nazad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Nazad.Location = new System.Drawing.Point(715, 20);
            this.Dugme_Nazad.Name = "Dugme_Nazad";
            this.Dugme_Nazad.Size = new System.Drawing.Size(140, 38);
            this.Dugme_Nazad.TabIndex = 4;
            this.Dugme_Nazad.Text = "Nazad";
            this.Dugme_Nazad.UseVisualStyleBackColor = true;
            this.Dugme_Nazad.Click += new System.EventHandler(this.Dugme_Nazad_Click);
            // 
            // Sastanci
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(867, 503);
            this.Controls.Add(this.Dugme_Nazad);
            this.Controls.Add(this.Pretrazi_Sastanak);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Dugme_DodajSastanak);
            this.Controls.Add(this.TabelaSastanaka);
            this.MinimumSize = new System.Drawing.Size(885, 500);
            this.Name = "Sastanci";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sastanci";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Sastanci_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.TabelaSastanaka)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView TabelaSastanaka;
        private System.Windows.Forms.Button Dugme_DodajSastanak;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Pretrazi_Sastanak;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdSastanka;
        private System.Windows.Forms.DataGridViewTextBoxColumn DatumOdrzavanja;
        private System.Windows.Forms.DataGridViewTextBoxColumn Time;
        private System.Windows.Forms.DataGridViewButtonColumn Učesnici;
        private System.Windows.Forms.Button Dugme_Nazad;
    }
}