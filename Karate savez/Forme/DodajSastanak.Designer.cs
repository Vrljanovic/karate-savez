﻿namespace Karate_savez.Forme
{
    partial class DodajSastanak
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.DatumOdrzavanjPicker = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.TemaTextBox = new System.Windows.Forms.TextBox();
            this.TabelaPrisutnih = new System.Windows.Forms.DataGridView();
            this.IdOsobe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Odbor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Prisustvovao = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ImeIPrezime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dugme_Potvrdi = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.Dugme_Odustani = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TabelaPrisutnih)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(49, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(188, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Datum održavanja sastanka:";
            // 
            // DatumOdrzavanjPicker
            // 
            this.DatumOdrzavanjPicker.Location = new System.Drawing.Point(52, 46);
            this.DatumOdrzavanjPicker.Name = "DatumOdrzavanjPicker";
            this.DatumOdrzavanjPicker.Size = new System.Drawing.Size(300, 22);
            this.DatumOdrzavanjPicker.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(49, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tema:";
            // 
            // TemaTextBox
            // 
            this.TemaTextBox.Location = new System.Drawing.Point(52, 91);
            this.TemaTextBox.Name = "TemaTextBox";
            this.TemaTextBox.Size = new System.Drawing.Size(300, 22);
            this.TemaTextBox.TabIndex = 3;
            // 
            // TabelaPrisutnih
            // 
            this.TabelaPrisutnih.AllowUserToAddRows = false;
            this.TabelaPrisutnih.AllowUserToDeleteRows = false;
            this.TabelaPrisutnih.AllowUserToOrderColumns = true;
            this.TabelaPrisutnih.AllowUserToResizeColumns = false;
            this.TabelaPrisutnih.AllowUserToResizeRows = false;
            this.TabelaPrisutnih.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TabelaPrisutnih.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdOsobe,
            this.Odbor,
            this.Prisustvovao,
            this.ImeIPrezime});
            this.TabelaPrisutnih.Location = new System.Drawing.Point(52, 135);
            this.TabelaPrisutnih.Name = "TabelaPrisutnih";
            this.TabelaPrisutnih.RowHeadersVisible = false;
            this.TabelaPrisutnih.RowTemplate.Height = 24;
            this.TabelaPrisutnih.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TabelaPrisutnih.Size = new System.Drawing.Size(300, 250);
            this.TabelaPrisutnih.TabIndex = 5;
            // 
            // IdOsobe
            // 
            this.IdOsobe.HeaderText = "IdOsobe";
            this.IdOsobe.Name = "IdOsobe";
            this.IdOsobe.ReadOnly = true;
            this.IdOsobe.Visible = false;
            // 
            // Odbor
            // 
            this.Odbor.HeaderText = "Odbor";
            this.Odbor.Name = "Odbor";
            this.Odbor.ReadOnly = true;
            this.Odbor.Visible = false;
            // 
            // Prisustvovao
            // 
            this.Prisustvovao.HeaderText = "Prisustvovao";
            this.Prisustvovao.Name = "Prisustvovao";
            this.Prisustvovao.Width = 80;
            // 
            // ImeIPrezime
            // 
            this.ImeIPrezime.HeaderText = "Ime i prezime";
            this.ImeIPrezime.Name = "ImeIPrezime";
            this.ImeIPrezime.ReadOnly = true;
            this.ImeIPrezime.Width = 140;
            // 
            // Dugme_Potvrdi
            // 
            this.Dugme_Potvrdi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_Potvrdi.Location = new System.Drawing.Point(22, 400);
            this.Dugme_Potvrdi.Name = "Dugme_Potvrdi";
            this.Dugme_Potvrdi.Size = new System.Drawing.Size(168, 38);
            this.Dugme_Potvrdi.TabIndex = 6;
            this.Dugme_Potvrdi.Text = "Potvrdi";
            this.Dugme_Potvrdi.UseVisualStyleBackColor = true;
            this.Dugme_Potvrdi.Click += new System.EventHandler(this.Dugme_Potvrdi_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Prisustvovali sastanku:";
            // 
            // Dugme_Odustani
            // 
            this.Dugme_Odustani.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Odustani.Location = new System.Drawing.Point(221, 400);
            this.Dugme_Odustani.Name = "Dugme_Odustani";
            this.Dugme_Odustani.Size = new System.Drawing.Size(168, 38);
            this.Dugme_Odustani.TabIndex = 7;
            this.Dugme_Odustani.Text = "Odustani";
            this.Dugme_Odustani.UseVisualStyleBackColor = true;
            this.Dugme_Odustani.Click += new System.EventHandler(this.Dugme_Odustani_Click);
            // 
            // DodajSastanak
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 450);
            this.Controls.Add(this.Dugme_Odustani);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Dugme_Potvrdi);
            this.Controls.Add(this.TabelaPrisutnih);
            this.Controls.Add(this.TemaTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DatumOdrzavanjPicker);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DodajSastanak";
            this.Text = "Dodaj sastanak";
            ((System.ComponentModel.ISupportInitialize)(this.TabelaPrisutnih)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker DatumOdrzavanjPicker;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TemaTextBox;
        private System.Windows.Forms.DataGridView TabelaPrisutnih;
        private System.Windows.Forms.Button Dugme_Potvrdi;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdOsobe;
        private System.Windows.Forms.DataGridViewTextBoxColumn Odbor;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Prisustvovao;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImeIPrezime;
        private System.Windows.Forms.Button Dugme_Odustani;
    }
}