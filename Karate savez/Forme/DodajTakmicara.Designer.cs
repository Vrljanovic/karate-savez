﻿namespace Karate_savez.Forme
{
    partial class DodajTakmicara
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.ImeTextBox = new System.Windows.Forms.TextBox();
            this.PrezimeTextBox = new System.Windows.Forms.TextBox();
            this.DatumRodjenjaPicker = new System.Windows.Forms.DateTimePicker();
            this.PojasComboBox = new System.Windows.Forms.ComboBox();
            this.BrojRegistracijeTextBox = new System.Windows.Forms.TextBox();
            this.KlubComboBox = new System.Windows.Forms.ComboBox();
            this.Dugme_DodajTakmicara = new System.Windows.Forms.Button();
            this.PolComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.Dugme_Odustani = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(74, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ime";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(74, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Prezime";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(74, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Datum rođenja";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(74, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(167, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Broj registracije u savezu";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(74, 199);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "Pojas";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(74, 246);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(36, 17);
            this.label6.TabIndex = 10;
            this.label6.Text = "Klub";
            // 
            // ImeTextBox
            // 
            this.ImeTextBox.Location = new System.Drawing.Point(77, 39);
            this.ImeTextBox.Name = "ImeTextBox";
            this.ImeTextBox.Size = new System.Drawing.Size(266, 22);
            this.ImeTextBox.TabIndex = 1;
            // 
            // PrezimeTextBox
            // 
            this.PrezimeTextBox.Location = new System.Drawing.Point(77, 84);
            this.PrezimeTextBox.Name = "PrezimeTextBox";
            this.PrezimeTextBox.Size = new System.Drawing.Size(266, 22);
            this.PrezimeTextBox.TabIndex = 3;
            // 
            // DatumRodjenjaPicker
            // 
            this.DatumRodjenjaPicker.Location = new System.Drawing.Point(77, 129);
            this.DatumRodjenjaPicker.Name = "DatumRodjenjaPicker";
            this.DatumRodjenjaPicker.Size = new System.Drawing.Size(266, 22);
            this.DatumRodjenjaPicker.TabIndex = 5;
            // 
            // PojasComboBox
            // 
            this.PojasComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.PojasComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.PojasComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PojasComboBox.FormattingEnabled = true;
            this.PojasComboBox.Location = new System.Drawing.Point(77, 219);
            this.PojasComboBox.Name = "PojasComboBox";
            this.PojasComboBox.Size = new System.Drawing.Size(266, 24);
            this.PojasComboBox.TabIndex = 9;
            // 
            // BrojRegistracijeTextBox
            // 
            this.BrojRegistracijeTextBox.Location = new System.Drawing.Point(77, 174);
            this.BrojRegistracijeTextBox.Name = "BrojRegistracijeTextBox";
            this.BrojRegistracijeTextBox.Size = new System.Drawing.Size(266, 22);
            this.BrojRegistracijeTextBox.TabIndex = 7;
            // 
            // KlubComboBox
            // 
            this.KlubComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.KlubComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.KlubComboBox.FormattingEnabled = true;
            this.KlubComboBox.Location = new System.Drawing.Point(77, 266);
            this.KlubComboBox.Name = "KlubComboBox";
            this.KlubComboBox.Size = new System.Drawing.Size(266, 24);
            this.KlubComboBox.TabIndex = 11;
            // 
            // Dugme_DodajTakmicara
            // 
            this.Dugme_DodajTakmicara.BackColor = System.Drawing.Color.Transparent;
            this.Dugme_DodajTakmicara.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_DodajTakmicara.Location = new System.Drawing.Point(38, 387);
            this.Dugme_DodajTakmicara.Name = "Dugme_DodajTakmicara";
            this.Dugme_DodajTakmicara.Size = new System.Drawing.Size(157, 42);
            this.Dugme_DodajTakmicara.TabIndex = 14;
            this.Dugme_DodajTakmicara.Text = "Potvrdi";
            this.Dugme_DodajTakmicara.UseVisualStyleBackColor = false;
            this.Dugme_DodajTakmicara.Click += new System.EventHandler(this.Dugme_DodajTakmicara_Click);
            // 
            // PolComboBox
            // 
            this.PolComboBox.AutoCompleteCustomSource.AddRange(new string[] {
            "Muški",
            "Ženski"});
            this.PolComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PolComboBox.FormattingEnabled = true;
            this.PolComboBox.Items.AddRange(new object[] {
            "Muški",
            "Ženski"});
            this.PolComboBox.Location = new System.Drawing.Point(77, 313);
            this.PolComboBox.Name = "PolComboBox";
            this.PolComboBox.Size = new System.Drawing.Size(266, 24);
            this.PolComboBox.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(74, 293);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 17);
            this.label7.TabIndex = 12;
            this.label7.Text = "Pol";
            // 
            // Dugme_Odustani
            // 
            this.Dugme_Odustani.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Odustani.Location = new System.Drawing.Point(230, 387);
            this.Dugme_Odustani.Name = "Dugme_Odustani";
            this.Dugme_Odustani.Size = new System.Drawing.Size(157, 42);
            this.Dugme_Odustani.TabIndex = 15;
            this.Dugme_Odustani.Text = "Odustani";
            this.Dugme_Odustani.UseVisualStyleBackColor = true;
            this.Dugme_Odustani.Click += new System.EventHandler(this.Dugme_Odustani_Click);
            // 
            // DodajTakmicara
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 478);
            this.Controls.Add(this.Dugme_Odustani);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.PolComboBox);
            this.Controls.Add(this.Dugme_DodajTakmicara);
            this.Controls.Add(this.KlubComboBox);
            this.Controls.Add(this.BrojRegistracijeTextBox);
            this.Controls.Add(this.PojasComboBox);
            this.Controls.Add(this.DatumRodjenjaPicker);
            this.Controls.Add(this.PrezimeTextBox);
            this.Controls.Add(this.ImeTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DodajTakmicara";
            this.Text = "Dodaj takmičara";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox ImeTextBox;
        private System.Windows.Forms.TextBox PrezimeTextBox;
        private System.Windows.Forms.DateTimePicker DatumRodjenjaPicker;
        private System.Windows.Forms.ComboBox PojasComboBox;
        private System.Windows.Forms.TextBox BrojRegistracijeTextBox;
        private System.Windows.Forms.ComboBox KlubComboBox;
        private System.Windows.Forms.Button Dugme_DodajTakmicara;
        private System.Windows.Forms.ComboBox PolComboBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button Dugme_Odustani;
    }
}