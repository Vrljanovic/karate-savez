﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class IzmjenaPodataka : Form
    {
        private static Klub klub = null;
        private static ToolStripStatusLabel statusLabel = null;
        public IzmjenaPodataka(Klub k, ToolStripStatusLabel l)
        {
            klub = k;
            statusLabel = l;
            InitializeComponent();
            Label_ImeKluba.Text = "Karate klub \"" + k.Naziv + "\"";
            NazivKlubaTextBox.Text = k.Naziv;
            JIBTextBox.Text = k.JIB;
            MjestoTextBox.Text = k.Sjediste.Naziv;
            AdresaTextBox.Text = k.Sjediste.Adresa;
        }

        private void Dugme_Potvrdi_Click(object sender, EventArgs e)
        {
            if (NazivKlubaTextBox.Text.Equals(string.Empty) || JIBTextBox.Text.Equals(string.Empty) || MjestoTextBox.Equals(string.Empty) || AdresaTextBox.Text.Equals(string.Empty))
            {
                MessageBox.Show("Niste popunili sva polja");
                return;
            }
            if (DialogResult.OK == MessageBox.Show("Da li su unijeti podaci tačni ? Ako ste sigurni stisnite dugme \"Ok\"", "Upozorenje", MessageBoxButtons.OKCancel))
            {
                klub.Naziv = NazivKlubaTextBox.Text;
                klub.JIB = JIBTextBox.Text;
                klub.Sjediste.Naziv = MjestoTextBox.Text;
                klub.Sjediste.Adresa = AdresaTextBox.Text;
                KlubDAO klubDAO = new KlubDAO();
                if (klubDAO.IzmjeniPodatke(klub))
                {
                    statusLabel.Text = "Podaci o klubu su uspješno izmjenjeni.";
                    statusLabel.ForeColor = Color.Green;
                }
                else
                {
                    statusLabel.Text = "Neuspješna izmjena podataka o klubu.";
                    statusLabel.ForeColor = Color.Red;
                }
                this.Close();
            }
        }

        private void Dugme_Odustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}