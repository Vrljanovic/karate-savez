﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class DodajClanaOdbora : Form
    {
        private ToolStripStatusLabel label;
        private string odbor;

        public DodajClanaOdbora(ToolStripStatusLabel label, string odbor)
        {
            InitializeComponent();
            this.label = label;
            this.odbor = odbor;
        }

        public object ClanUp { get; private set; }

        private void Dugme_Odustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Dugme_Potvrdi_Click(object sender, EventArgs e)
        {
            if(string.Empty.Equals(ImeTextBox.Text) || string.Empty.Equals(PrezimeTextBox.Text) || string.Empty.Equals(PolComboBox.SelectedItem) || 
                DateTime.Today > OdDatePicker.Value || DateTime.Today > DoDatePicker.Value || DoDatePicker.Value < OdDatePicker.Value || 
                DoDatePicker.Value.ToLongDateString().Equals(OdDatePicker.Value.ToLongDateString()))
            {
                MessageBox.Show("Podaci nisu validni. Pokušajte ponovo.");
                ImeTextBox.Clear();
                PrezimeTextBox.Clear();
                OdDatePicker.Value = DateTime.Today;
                DoDatePicker.Value = DateTime.Today;
                return;
            }
            if (DialogResult.OK == MessageBox.Show("Da li su unijeti podaci tačni ? Ako ste sigurni stisnite dugme \"OK\"", "Upozorenje", MessageBoxButtons.OKCancel))
            {
                string ime = ImeTextBox.Text;
                string prezime = PrezimeTextBox.Text;
                string pol = (string)PolComboBox.SelectedItem;
                DateTime od = OdDatePicker.Value;
                DateTime _do = DoDatePicker.Value;

                bool uspjesno = true;
                if ("upravni".Equals(odbor))
                {
                    ClanUpravnogOdbora c = new ClanUpravnogOdbora(0, ime, prezime, pol, new DateTime(), od, _do);
                    if (new ClanOdboraDAO().DodajClanaUpravnogOdbora(c))
                        uspjesno = true;

                }
                else
                {
                    ClanNadzornogOdbora c = new ClanNadzornogOdbora(0, ime, prezime, pol, new DateTime(), od, _do);
                    if (new ClanOdboraDAO().DodajClanaNadzornogOdbora(c))
                        uspjesno = true;
                }

                if (uspjesno)
                {
                    label.Text = "Uspješno dodavanje novog člana odbora.";
                    label.ForeColor = Color.Green;
                    this.Close();
                    return;
                }
                label.Text = "Neuspješno dodavanje novog člana odbora.";
                label.ForeColor = Color.Red;
                this.Close();
            }
        }
    }
}
