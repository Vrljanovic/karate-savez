﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Karate_savez.DAO;
using Karate_savez.DTO;
using MySql.Data.MySqlClient;

namespace Karate_savez
{
    public partial class KlubInfo : Form
    {
        private static ToolStripStatusLabel statusStrip;
        private static Klub klub;

        public KlubInfo(ToolStripStatusLabel strip, Klub k = null)
        {
            klub = k;
            statusStrip = strip;
            InitializeComponent();
        }

        private void Dugme_Odustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Dugme_DodajKlub_Click(object sender, EventArgs e)
        {
            if (JIBTextBox.Text.Equals(string.Empty) || NazivKlubaTextBox.Text.Equals(string.Empty) || MjestoTextBox.Text.Equals(string.Empty) ||
                AdresaTextBox.Text.Equals(string.Empty) || ImeTextBox.Text.Equals(string.Empty) || PrezimeTextBox.Text.Equals(string.Empty) ||
                BrojTelefonaTextBox.Text.Equals(string.Empty) || EmailAdresaTextBox.Text.Equals(string.Empty))
            {
                MessageBox.Show("Niste popunili sva polja");
                return;
            }
            if (DialogResult.OK == MessageBox.Show("Da li su unijeti podaci tačni? Ako ste sigurni stisnite dugme \"OK\"", "Upozorenje", MessageBoxButtons.OKCancel))
            {
                PredstavnikKluba predstavnik = new PredstavnikKluba(0, ImeTextBox.Text, PrezimeTextBox.Text, new DateTime(1900, 1, 1), (string)PolComboBox.SelectedItem, EmailAdresaTextBox.Text, BrojTelefonaTextBox.Text);
                Mjesto mjesto = new Mjesto(0, MjestoTextBox.Text, AdresaTextBox.Text);
                klub = new Klub(klub == null ? 0 : klub.IdKluba, NazivKlubaTextBox.Text, JIBTextBox.Text, mjesto, predstavnik);
                KlubDAO klubDAO = new KlubDAO();
                if (klubDAO.DodajKlub(klub))
                {
                    statusStrip.Text = "Klub je uspješno dodan u bazu";
                    statusStrip.ForeColor = Color.Green;
                }
                else
                {
                    statusStrip.Text = "Neuspješno dodavanje kluba. Provjerite unijete podatke.";
                    statusStrip.ForeColor = Color.Red;
                }
                this.Close();
            }
        }
    }
}