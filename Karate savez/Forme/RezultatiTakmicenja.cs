﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class RezultatiTakmicenja : Form
    {
        private Takmicenje takmicenje;
        private static Dictionary<Kategorija, Rezultat> rezultati;
        ToolStripStatusLabel label;

        public RezultatiTakmicenja(Takmicenje takmicenje, ToolStripStatusLabel label)
        {
            this.label = label;
            this.takmicenje = takmicenje;
            InitializeComponent();
            drugoMjestoComboBox.Enabled = false;
            treceMjestoComboBox.Enabled = false;
            treceMjesto2ComboBox.Enabled = false;
            drugoMjestoKlubBox.Enabled = false;
            treceMjestoKlubBox.Enabled = false;
            cetvrtoMjestoKlubBox.Enabled = false;
            rezultati = new Dictionary<Kategorija, Rezultat>();
        }

        private void RezultatiTakmicenja_Load(object sender, EventArgs e)
        {
            Takmicar[] takmicari = new TakmicarDAO().DohvatiSveTakmicare().ToArray();
            List<Kategorija> kategorije = new KategorijaDAO().DohvatiSveKategorije();
            kategorijaComboBox.Items.AddRange(kategorije.ToArray());
            prvoMjestoComboBox.Items.AddRange(takmicari);
            drugoMjestoComboBox.Items.AddRange(takmicari);
            treceMjestoComboBox.Items.AddRange(takmicari);
            treceMjesto2ComboBox.Items.AddRange(takmicari);

            Klub[] klubovi = new KlubDAO().DohvatiSveAktivneKlubove().ToArray();
            prvoMjestoKlubBox.Items.AddRange(klubovi);
            drugoMjestoKlubBox.Items.AddRange(klubovi);
            treceMjestoKlubBox.Items.AddRange(klubovi);
            cetvrtoMjestoKlubBox.Items.AddRange(klubovi);

            RezultatDAO rezultatDAO = new RezultatDAO();
            kategorije.ForEach(k =>
            {
                Rezultat rez = rezultatDAO.RezultatTakmicenjaZaKategoriju(takmicenje, k);
                if (rez.PrvoMjesto != null)
                    rezultati.Add(k, rez);
            });
            UcitajRezultateKlubova();
        }

        private void UcitajRezultateKlubova()
        {
            LinkedList<Klub> rezultatiKlubova = new RezultatDAO().RezultatTakmicenjaZaKlubove(takmicenje);
            if (rezultatiKlubova.Count > 0)
            {
                prvoMjestoKlubBox.SelectedItem = rezultatiKlubova.First.Value;
                rezultatiKlubova.RemoveFirst();
                if (rezultatiKlubova.Count > 0)
                {
                    drugoMjestoKlubBox.SelectedItem = rezultatiKlubova.First.Value;
                    rezultatiKlubova.RemoveFirst();
                    if (rezultatiKlubova.Count > 0)
                    {
                        treceMjestoKlubBox.SelectedItem = rezultatiKlubova.First.Value;
                        rezultatiKlubova.RemoveFirst();
                        if (rezultatiKlubova.Count > 0)
                        {
                            cetvrtoMjestoKlubBox.SelectedItem = rezultatiKlubova.First.Value;
                        }
                    }
                }
            }
        }

        private void kategorijaComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            prvoMjestoComboBox.SelectedItem = null;
            drugoMjestoComboBox.SelectedItem = null;
            treceMjesto2ComboBox.SelectedItem = null;
            treceMjestoComboBox.SelectedItem = null;

            Kategorija k = (Kategorija)kategorijaComboBox.SelectedItem;
            rezultati.TryGetValue(k, out Rezultat r);
            if (r != null)
            {
                prvoMjestoComboBox.SelectedItem = r.PrvoMjesto;
                drugoMjestoComboBox.SelectedItem = r.DrugoMjesto;
                treceMjestoComboBox.SelectedItem = r.TreceMjesto;
                treceMjesto2ComboBox.SelectedItem = r.TreceMjesto2;
            }
        }

        private void Dugme_Odustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void kategorijaComboBox_Enter(object sender, EventArgs e)
        {
            Kategorija k = (Kategorija)kategorijaComboBox.SelectedItem;
            if (k != null)
            {
                Rezultat r = new Rezultat(takmicenje, k, (Takmicar)prvoMjestoComboBox.SelectedItem, (Takmicar)drugoMjestoComboBox.SelectedItem, (Takmicar)treceMjestoComboBox.SelectedItem, (Takmicar)treceMjesto2ComboBox.SelectedItem);
                rezultati.Add(k, r);
            }
        }

        private void Dugme_Potvrdi_Click(object sender, EventArgs e)
        {
            Kategorija k = (Kategorija)kategorijaComboBox.SelectedItem;
            Takmicar prvi = (Takmicar)prvoMjestoComboBox.SelectedItem;
            Takmicar drugi = (Takmicar)drugoMjestoComboBox.SelectedItem;
            Takmicar treci = (Takmicar)treceMjestoComboBox.SelectedItem;
            Takmicar treci2 = (Takmicar)treceMjesto2ComboBox.SelectedItem;
            if (prvi != null && (prvi.Equals(drugi) || prvi.Equals(treci) || prvi.Equals(treci2)) ||
                drugi != null && (drugi.Equals(treci) || drugi.Equals(treci2)) ||
                treci != null &&  treci.Equals(treci2))
            {
                MessageBox.Show("Ne možete na više mjesta staviti iste takmičare");
                return;
            }
            Klub prviKlub = (Klub)prvoMjestoKlubBox.SelectedItem;
            Klub drugiKlub = (Klub)drugoMjestoKlubBox.SelectedItem;
            Klub treciKlub = (Klub)treceMjestoKlubBox.SelectedItem;
            Klub cetvrtiKlub = (Klub)cetvrtoMjestoKlubBox.SelectedItem;
            if (prviKlub != null && (prviKlub.Equals(drugiKlub) || prviKlub.Equals(treciKlub) || prviKlub.Equals(cetvrtiKlub)) ||
                drugiKlub != null && (drugiKlub.Equals(treciKlub) || drugiKlub.Equals(cetvrtiKlub)) ||
                treciKlub != null && treciKlub.Equals(cetvrtiKlub))
            {
                MessageBox.Show("Ne možete na više mjesta staviti isti klub");
                return;
            }
            if (k != null)
            {
                Rezultat r = new Rezultat(takmicenje, k, prvi, drugi, treci, treci2);
                if (rezultati.ContainsKey(k))
                    rezultati.Remove(k);
                rezultati.Add(k, r);
            }
            RezultatDAO rezultatDAO = new RezultatDAO();
            List<Rezultat> rez = rezultati.Values.ToList();
            rezultatDAO.ObrisiRezultateZaTakmicenje(takmicenje);
            rez.ForEach(r =>
            {
                if (r != null && r.PrvoMjesto != null)
                    rezultatDAO.DodajRezultat(r);
            });
            List<Tuple<Klub, int>> rezultatKlubova = new List<Tuple<Klub, int>>
            {
                new Tuple<Klub, int>(prviKlub, 1),
                new Tuple<Klub, int>(drugiKlub, 2),
                new Tuple<Klub, int>(treciKlub, 3),
                new Tuple<Klub, int>(cetvrtiKlub, 4)
            };
            rezultatDAO.ObrisiRezultatKlubovaZaTakmicenje(takmicenje);
            rezultatKlubova.ForEach(r =>
            {
                if (r.Item1 != null)
                    rezultatDAO.DodajRezultatKluba(r.Item1, takmicenje, r.Item2);
            });
            label.Text = "Rezultati za takmičenje su uspješno ažurirani";
            label.ForeColor = Color.Green;
            this.Close();
        }

        private void prvoMjestoComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(prvoMjestoComboBox.SelectedText.Equals(string.Empty))
                drugoMjestoComboBox.Enabled = true;
            else
            {
                drugoMjestoComboBox.SelectedItem = null;
                drugoMjestoComboBox.Enabled = false;
                treceMjestoComboBox.SelectedItem = null;
                treceMjestoComboBox.Enabled = false;
                treceMjesto2ComboBox.SelectedItem = null;
                treceMjesto2ComboBox.Enabled = false;
            }
        }

        private void drugoMjestoComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (prvoMjestoComboBox.SelectedText.Equals(string.Empty))
                treceMjestoComboBox.Enabled = true;
            else
            {
                treceMjestoComboBox.SelectedItem = null;
                treceMjestoComboBox.Enabled = false;
                treceMjesto2ComboBox.SelectedItem = null;
                treceMjesto2ComboBox.Enabled = false;
            }
        }

        private void treceMjestoComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (prvoMjestoComboBox.SelectedText.Equals(string.Empty))
                treceMjesto2ComboBox.Enabled = true;
            else
            {
                treceMjesto2ComboBox.SelectedItem = null;
                treceMjesto2ComboBox.Enabled = false;
            }
        }

        private void prvoMjestoKlubBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (prvoMjestoComboBox.SelectedText.Equals(string.Empty))
                drugoMjestoKlubBox.Enabled = true;
            else
            {
                drugoMjestoKlubBox.SelectedItem = null;
                drugoMjestoKlubBox.Enabled = false;
                treceMjestoKlubBox.SelectedItem = null;
                treceMjestoKlubBox.Enabled = false;
                cetvrtoMjestoKlubBox.SelectedItem = null;
                cetvrtoMjestoKlubBox.Enabled = false;
            }
        }

        private void drugoMjestoKlubBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (prvoMjestoComboBox.SelectedText.Equals(string.Empty))
                treceMjestoKlubBox.Enabled = true;
            else
            {
                treceMjestoKlubBox.SelectedItem = null;
                treceMjestoKlubBox.Enabled = false;
                cetvrtoMjestoKlubBox.SelectedItem = null;
                cetvrtoMjestoKlubBox.Enabled = false;
            }
        }

        private void treceMjestoKlubBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (prvoMjestoComboBox.SelectedText.Equals(string.Empty))
                cetvrtoMjestoKlubBox.Enabled = true;
            else
            {
                cetvrtoMjestoKlubBox.SelectedItem = null;
                cetvrtoMjestoKlubBox.Enabled = false;
            }
        }
    }
}
