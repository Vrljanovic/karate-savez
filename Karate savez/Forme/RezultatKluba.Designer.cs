﻿namespace Karate_savez.Forme
{
    partial class RezultatKluba
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TabelaRezultata = new System.Windows.Forms.DataGridView();
            this.Takmicenje = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OsvojenoMjesto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KlubLabel = new System.Windows.Forms.Label();
            this.Dugme_Zatvori = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TabelaRezultata)).BeginInit();
            this.SuspendLayout();
            // 
            // TabelaRezultata
            // 
            this.TabelaRezultata.AllowUserToAddRows = false;
            this.TabelaRezultata.AllowUserToDeleteRows = false;
            this.TabelaRezultata.AllowUserToResizeColumns = false;
            this.TabelaRezultata.AllowUserToResizeRows = false;
            this.TabelaRezultata.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TabelaRezultata.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Takmicenje,
            this.OsvojenoMjesto});
            this.TabelaRezultata.Location = new System.Drawing.Point(-1, 54);
            this.TabelaRezultata.Name = "TabelaRezultata";
            this.TabelaRezultata.RowHeadersVisible = false;
            this.TabelaRezultata.RowTemplate.Height = 24;
            this.TabelaRezultata.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TabelaRezultata.Size = new System.Drawing.Size(550, 290);
            this.TabelaRezultata.TabIndex = 0;
            // 
            // Takmicenje
            // 
            this.Takmicenje.HeaderText = "Takmicenje";
            this.Takmicenje.Name = "Takmicenje";
            this.Takmicenje.ReadOnly = true;
            this.Takmicenje.Width = 300;
            // 
            // OsvojenoMjesto
            // 
            this.OsvojenoMjesto.HeaderText = "Osvojeno mjesto";
            this.OsvojenoMjesto.Name = "OsvojenoMjesto";
            this.OsvojenoMjesto.ReadOnly = true;
            // 
            // KlubLabel
            // 
            this.KlubLabel.AutoSize = true;
            this.KlubLabel.Location = new System.Drawing.Point(12, 22);
            this.KlubLabel.Name = "KlubLabel";
            this.KlubLabel.Size = new System.Drawing.Size(0, 17);
            this.KlubLabel.TabIndex = 1;
            // 
            // Dugme_Zatvori
            // 
            this.Dugme_Zatvori.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Zatvori.Location = new System.Drawing.Point(375, 350);
            this.Dugme_Zatvori.Name = "Dugme_Zatvori";
            this.Dugme_Zatvori.Size = new System.Drawing.Size(145, 41);
            this.Dugme_Zatvori.TabIndex = 2;
            this.Dugme_Zatvori.Text = "Zatvori";
            this.Dugme_Zatvori.UseVisualStyleBackColor = true;
            this.Dugme_Zatvori.Click += new System.EventHandler(this.Dugme_Zatvori_Click);
            // 
            // RezultatKluba
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(532, 403);
            this.Controls.Add(this.Dugme_Zatvori);
            this.Controls.Add(this.KlubLabel);
            this.Controls.Add(this.TabelaRezultata);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RezultatKluba";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rezultat kluba";
            ((System.ComponentModel.ISupportInitialize)(this.TabelaRezultata)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView TabelaRezultata;
        private System.Windows.Forms.Label KlubLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Takmicenje;
        private System.Windows.Forms.DataGridViewTextBoxColumn OsvojenoMjesto;
        private System.Windows.Forms.Button Dugme_Zatvori;
    }
}