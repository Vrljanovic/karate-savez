﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class DodajSastanak : Form
    {
        public DodajSastanak()
        {
            InitializeComponent();
            PopuniTabelu();
        }

        private void PopuniTabelu()
        {
            ClanOdboraDAO clanOdboraDAO = new ClanOdboraDAO();
            clanOdboraDAO.DohvatiClanoveUpravnogOdbora().FindAll(c => c.ClanOd <= DateTime.Today && c.ClanDo > DateTime.Today).ForEach(c => TabelaPrisutnih.Rows.Add(c.IdOsobe, "upravni", false, c.Ime + " " + c.Prezime));
            clanOdboraDAO.DohvatiClanoveNadzornogOdbora().FindAll(c => c.ClanOd <= DateTime.Today && c.ClanDo > DateTime.Today).ForEach(c => TabelaPrisutnih.Rows.Add(c.IdOsobe, "nadzorni", false, c.Ime + " " + c.Prezime));
        }

        private void Dugme_Odustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Dugme_Potvrdi_Click(object sender, EventArgs e)
        {
            if (DatumOdrzavanjPicker.Value > DateTime.Today)
            {
                MessageBox.Show("Možete dodati podatke o održanim sastancima");
                return;
            }
            if (string.Empty.Equals(TemaTextBox.Text))
            {
                MessageBox.Show("Niste unijeli temu sastanka");
                return;
            }
            if (DialogResult.OK == MessageBox.Show("Da li su unijeti podaci tačni ? Ako ste sigurni stisnite dugme \"OK\"", "Upozorenje", MessageBoxButtons.OKCancel))
            {
                DateTime datum = DatumOdrzavanjPicker.Value;
                string tema = TemaTextBox.Text;
                Sastanak sastanak = new Sastanak(0, datum, tema);
                SastanakDAO sastanakDAO = new SastanakDAO();
                sastanakDAO.DodajSastanak(sastanak);
                for (int i = 0; i < TabelaPrisutnih.Rows.Count; ++i)
                {
                    if ((bool)TabelaPrisutnih.Rows[i].Cells[2].Value)
                    {
                        string odbor = (string)TabelaPrisutnih.Rows[i].Cells[1].Value;
                        int id = (int)TabelaPrisutnih.Rows[i].Cells[0].Value;
                        if ("upravni".Equals(odbor))
                            sastanakDAO.DodajPrisutnogClanaUpravnogOdbora(sastanak, id);
                        else
                            sastanakDAO.DodajPrisutnogClanaNadzornogOdbora(sastanak, id);
                    }
                }
                this.Close();
            }
        }
    }
}
