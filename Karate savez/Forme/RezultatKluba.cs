﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class RezultatKluba : Form
    {
        private Klub klub;
        public RezultatKluba(Klub klub)
        {
            InitializeComponent();
            this.klub = klub;
            this.KlubLabel.Text = "Rezultati za " + klub.ToString();
            PopuniTabelu();
        }

        private void PopuniTabelu()
        {
            List<DTO.RezultatKluba> rezultati = new RezultatDAO().DohvatiRezultateKluba(klub);

            if (rezultati.Count == 0)
                throw new Exception("Nema rezultata");
            rezultati.ForEach(r => TabelaRezultata.Rows.Add(r.Takmicenje.MjestoOdrzavanja + " " +r.Takmicenje.PocetakTakmicenja.ToShortDateString(), r.OsvojenoMjesto));
        }

        private void Dugme_Zatvori_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
