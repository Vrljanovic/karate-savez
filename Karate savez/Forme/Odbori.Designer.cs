﻿namespace Karate_savez.Forme
{
    partial class Odbori
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.PretraziUpravni_TextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TabelaUpravniOdbor = new System.Windows.Forms.DataGridView();
            this.IdClana = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImeIPrezime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Period = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Dugme_DodajClanaUO = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Dugme_Nazad = new System.Windows.Forms.Button();
            this.PretraziNadzorni_TextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Dugme_DodajClanaNO = new System.Windows.Forms.Button();
            this.TabelaNadzorniOdbor = new System.Windows.Forms.DataGridView();
            this.IdClanaNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImeIPrezimeNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PeriodNO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabelaUpravniOdbor)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabelaNadzorniOdbor)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.PretraziUpravni_TextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.TabelaUpravniOdbor);
            this.groupBox1.Controls.Add(this.Dugme_DodajClanaUO);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(551, 636);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Upravni odbor";
            // 
            // PretraziUpravni_TextBox
            // 
            this.PretraziUpravni_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F);
            this.PretraziUpravni_TextBox.Location = new System.Drawing.Point(6, 71);
            this.PretraziUpravni_TextBox.Name = "PretraziUpravni_TextBox";
            this.PretraziUpravni_TextBox.Size = new System.Drawing.Size(400, 38);
            this.PretraziUpravni_TextBox.TabIndex = 1;
            this.PretraziUpravni_TextBox.TextChanged += new System.EventHandler(this.PretraziUpravni_TextBox_TextChanged);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pretraži:";
            // 
            // TabelaUpravniOdbor
            // 
            this.TabelaUpravniOdbor.AllowUserToAddRows = false;
            this.TabelaUpravniOdbor.AllowUserToDeleteRows = false;
            this.TabelaUpravniOdbor.AllowUserToResizeColumns = false;
            this.TabelaUpravniOdbor.AllowUserToResizeRows = false;
            this.TabelaUpravniOdbor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.TabelaUpravniOdbor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TabelaUpravniOdbor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdClana,
            this.ImeIPrezime,
            this.Period});
            this.TabelaUpravniOdbor.Location = new System.Drawing.Point(6, 115);
            this.TabelaUpravniOdbor.Name = "TabelaUpravniOdbor";
            this.TabelaUpravniOdbor.ReadOnly = true;
            this.TabelaUpravniOdbor.RowHeadersVisible = false;
            this.TabelaUpravniOdbor.RowTemplate.Height = 24;
            this.TabelaUpravniOdbor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TabelaUpravniOdbor.Size = new System.Drawing.Size(537, 467);
            this.TabelaUpravniOdbor.TabIndex = 2;
            // 
            // IdClana
            // 
            this.IdClana.HeaderText = "ID člana";
            this.IdClana.Name = "IdClana";
            this.IdClana.ReadOnly = true;
            this.IdClana.Visible = false;
            // 
            // ImeIPrezime
            // 
            this.ImeIPrezime.HeaderText = "Ime i prezime";
            this.ImeIPrezime.Name = "ImeIPrezime";
            this.ImeIPrezime.ReadOnly = true;
            this.ImeIPrezime.Width = 200;
            // 
            // Period
            // 
            this.Period.HeaderText = "Period";
            this.Period.Name = "Period";
            this.Period.ReadOnly = true;
            this.Period.Width = 200;
            // 
            // Dugme_DodajClanaUO
            // 
            this.Dugme_DodajClanaUO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Dugme_DodajClanaUO.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_DodajClanaUO.Location = new System.Drawing.Point(166, 588);
            this.Dugme_DodajClanaUO.Name = "Dugme_DodajClanaUO";
            this.Dugme_DodajClanaUO.Size = new System.Drawing.Size(190, 42);
            this.Dugme_DodajClanaUO.TabIndex = 3;
            this.Dugme_DodajClanaUO.Text = "Dodaj člana";
            this.Dugme_DodajClanaUO.UseVisualStyleBackColor = true;
            this.Dugme_DodajClanaUO.Click += new System.EventHandler(this.Dugme_DodajClanaUO_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.Dugme_Nazad);
            this.groupBox2.Controls.Add(this.PretraziNadzorni_TextBox);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.Dugme_DodajClanaNO);
            this.groupBox2.Controls.Add(this.TabelaNadzorniOdbor);
            this.groupBox2.Location = new System.Drawing.Point(579, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(551, 636);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Nadzorni odbor";
            // 
            // Dugme_Nazad
            // 
            this.Dugme_Nazad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Dugme_Nazad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Nazad.Location = new System.Drawing.Point(403, 21);
            this.Dugme_Nazad.Name = "Dugme_Nazad";
            this.Dugme_Nazad.Size = new System.Drawing.Size(140, 38);
            this.Dugme_Nazad.TabIndex = 4;
            this.Dugme_Nazad.Text = "Nazad";
            this.Dugme_Nazad.UseVisualStyleBackColor = true;
            this.Dugme_Nazad.Click += new System.EventHandler(this.Dugme_Nazad_Click);
            // 
            // PretraziNadzorni_TextBox
            // 
            this.PretraziNadzorni_TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PretraziNadzorni_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F);
            this.PretraziNadzorni_TextBox.Location = new System.Drawing.Point(6, 71);
            this.PretraziNadzorni_TextBox.Name = "PretraziNadzorni_TextBox";
            this.PretraziNadzorni_TextBox.Size = new System.Drawing.Size(400, 38);
            this.PretraziNadzorni_TextBox.TabIndex = 1;
            this.PretraziNadzorni_TextBox.TextChanged += new System.EventHandler(this.PretraziNadzorni_TextBox_TextChanged);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Pretraži:";
            // 
            // Dugme_DodajClanaNO
            // 
            this.Dugme_DodajClanaNO.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Dugme_DodajClanaNO.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_DodajClanaNO.Location = new System.Drawing.Point(182, 588);
            this.Dugme_DodajClanaNO.Name = "Dugme_DodajClanaNO";
            this.Dugme_DodajClanaNO.Size = new System.Drawing.Size(190, 42);
            this.Dugme_DodajClanaNO.TabIndex = 3;
            this.Dugme_DodajClanaNO.Text = "Dodaj člana";
            this.Dugme_DodajClanaNO.UseVisualStyleBackColor = true;
            this.Dugme_DodajClanaNO.Click += new System.EventHandler(this.Dugme_DodajClanaNO_Click);
            // 
            // TabelaNadzorniOdbor
            // 
            this.TabelaNadzorniOdbor.AllowUserToAddRows = false;
            this.TabelaNadzorniOdbor.AllowUserToDeleteRows = false;
            this.TabelaNadzorniOdbor.AllowUserToResizeColumns = false;
            this.TabelaNadzorniOdbor.AllowUserToResizeRows = false;
            this.TabelaNadzorniOdbor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabelaNadzorniOdbor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TabelaNadzorniOdbor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdClanaNO,
            this.ImeIPrezimeNO,
            this.PeriodNO});
            this.TabelaNadzorniOdbor.Location = new System.Drawing.Point(6, 115);
            this.TabelaNadzorniOdbor.Name = "TabelaNadzorniOdbor";
            this.TabelaNadzorniOdbor.ReadOnly = true;
            this.TabelaNadzorniOdbor.RowHeadersVisible = false;
            this.TabelaNadzorniOdbor.RowTemplate.Height = 24;
            this.TabelaNadzorniOdbor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TabelaNadzorniOdbor.Size = new System.Drawing.Size(537, 467);
            this.TabelaNadzorniOdbor.TabIndex = 2;
            // 
            // IdClanaNO
            // 
            this.IdClanaNO.HeaderText = "ID člana";
            this.IdClanaNO.Name = "IdClanaNO";
            this.IdClanaNO.ReadOnly = true;
            this.IdClanaNO.Visible = false;
            // 
            // ImeIPrezimeNO
            // 
            this.ImeIPrezimeNO.HeaderText = "Ime i prezime";
            this.ImeIPrezimeNO.Name = "ImeIPrezimeNO";
            this.ImeIPrezimeNO.ReadOnly = true;
            this.ImeIPrezimeNO.Width = 200;
            // 
            // PeriodNO
            // 
            this.PeriodNO.HeaderText = "Period";
            this.PeriodNO.Name = "PeriodNO";
            this.PeriodNO.ReadOnly = true;
            this.PeriodNO.Width = 200;
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 651);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1142, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // Odbori
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1142, 673);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1160, 720);
            this.Name = "Odbori";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Odbori";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Odbori_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabelaUpravniOdbor)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TabelaNadzorniOdbor)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView TabelaNadzorniOdbor;
        private System.Windows.Forms.DataGridView TabelaUpravniOdbor;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdClana;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImeIPrezime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Period;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdClanaNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImeIPrezimeNO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PeriodNO;
        private System.Windows.Forms.Button Dugme_DodajClanaUO;
        private System.Windows.Forms.Button Dugme_DodajClanaNO;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.TextBox PretraziUpravni_TextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox PretraziNadzorni_TextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Dugme_Nazad;
    }
}