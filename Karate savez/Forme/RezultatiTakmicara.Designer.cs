﻿namespace Karate_savez.Forme
{
    partial class RezultatiTakmicara
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TabelaRezultata = new System.Windows.Forms.DataGridView();
            this.Takmicenje = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Kategorija = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OsvojenoMjesto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TakmicarLabel = new System.Windows.Forms.Label();
            this.Dugme_Zatvori = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TabelaRezultata)).BeginInit();
            this.SuspendLayout();
            // 
            // TabelaRezultata
            // 
            this.TabelaRezultata.AllowUserToAddRows = false;
            this.TabelaRezultata.AllowUserToDeleteRows = false;
            this.TabelaRezultata.AllowUserToResizeColumns = false;
            this.TabelaRezultata.AllowUserToResizeRows = false;
            this.TabelaRezultata.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TabelaRezultata.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Takmicenje,
            this.Kategorija,
            this.OsvojenoMjesto});
            this.TabelaRezultata.Location = new System.Drawing.Point(0, 72);
            this.TabelaRezultata.MultiSelect = false;
            this.TabelaRezultata.Name = "TabelaRezultata";
            this.TabelaRezultata.RowHeadersVisible = false;
            this.TabelaRezultata.RowTemplate.Height = 24;
            this.TabelaRezultata.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TabelaRezultata.Size = new System.Drawing.Size(950, 311);
            this.TabelaRezultata.TabIndex = 0;
            // 
            // Takmicenje
            // 
            this.Takmicenje.HeaderText = "Takmicenje";
            this.Takmicenje.Name = "Takmicenje";
            this.Takmicenje.ReadOnly = true;
            this.Takmicenje.Width = 300;
            // 
            // Kategorija
            // 
            this.Kategorija.HeaderText = "Kategorija";
            this.Kategorija.Name = "Kategorija";
            this.Kategorija.ReadOnly = true;
            this.Kategorija.Width = 300;
            // 
            // OsvojenoMjesto
            // 
            this.OsvojenoMjesto.HeaderText = "Osvojeno mjesto";
            this.OsvojenoMjesto.Name = "OsvojenoMjesto";
            this.OsvojenoMjesto.ReadOnly = true;
            // 
            // TakmicarLabel
            // 
            this.TakmicarLabel.AutoSize = true;
            this.TakmicarLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold);
            this.TakmicarLabel.Location = new System.Drawing.Point(-3, 25);
            this.TakmicarLabel.Name = "TakmicarLabel";
            this.TakmicarLabel.Size = new System.Drawing.Size(0, 17);
            this.TakmicarLabel.TabIndex = 1;
            // 
            // Dugme_Zatvori
            // 
            this.Dugme_Zatvori.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Zatvori.Location = new System.Drawing.Point(774, 400);
            this.Dugme_Zatvori.Name = "Dugme_Zatvori";
            this.Dugme_Zatvori.Size = new System.Drawing.Size(146, 41);
            this.Dugme_Zatvori.TabIndex = 3;
            this.Dugme_Zatvori.Text = "Zatvori";
            this.Dugme_Zatvori.UseVisualStyleBackColor = true;
            this.Dugme_Zatvori.Click += new System.EventHandler(this.Dugme_Zatvori_Click);
            // 
            // RezultatiTakmicara
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(932, 453);
            this.Controls.Add(this.Dugme_Zatvori);
            this.Controls.Add(this.TakmicarLabel);
            this.Controls.Add(this.TabelaRezultata);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RezultatiTakmicara";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rezultati takmičara";
            ((System.ComponentModel.ISupportInitialize)(this.TabelaRezultata)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView TabelaRezultata;
        private System.Windows.Forms.Label TakmicarLabel;
        private System.Windows.Forms.DataGridViewTextBoxColumn Takmicenje;
        private System.Windows.Forms.DataGridViewTextBoxColumn Kategorija;
        private System.Windows.Forms.DataGridViewTextBoxColumn OsvojenoMjesto;
        private System.Windows.Forms.Button Dugme_Zatvori;
    }
}