﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class DodajSudiju : Form
    {
        private static ToolStripStatusLabel statusLabel;

        public DodajSudiju(ToolStripStatusLabel label)
        {
            statusLabel = label;
            InitializeComponent();
        }

        private void Dugme_Odustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Dugme_DodajSudiju_Click(object sender, EventArgs e)
        {
            if(ImeTextBox.Text.Equals(string.Empty) || PrezimeTextBox.Text.Equals(string.Empty) || BrojSudijskeLicenceTextBox.Text.Equals(string.Empty) || DatumRodjenjaPicker.Value == null || DatumRodjenjaPicker.Value == DateTime.Today)
            {
                MessageBox.Show("Popunite sva polja i odaberite datum rodjenja");
                return;
            }
            if (DialogResult.OK == MessageBox.Show("Da li su unijeti podaci tačni ? Ako ste sigurni stisnite dugme \"Ok\"", "Upozorenje", MessageBoxButtons.OKCancel))
            {
                Sudija sudija = new Sudija(0, ImeTextBox.Text, PrezimeTextBox.Text, DatumRodjenjaPicker.Value, (string)PolComboBox.SelectedItem, BrojSudijskeLicenceTextBox.Text);
                if (new SudijaDAO().DodajSudiju(sudija))
                {
                    statusLabel.Text = "Novi sudija je uspješno dodan.";
                    statusLabel.ForeColor = Color.Green;
                }
                else
                {
                    statusLabel.Text = "Neuspješno dodavanje novog sudije.";
                    statusLabel.ForeColor = Color.Red;
                }
                this.Close();
            }
        }
    }
}