﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class PromijeniPojas : Form
    {
        private Takmicar takmicar;
        private ToolStripStatusLabel statusLabel;
        public PromijeniPojas(ToolStripStatusLabel label, Takmicar t)
        {
            takmicar = t;
            statusLabel = label;
            InitializeComponent();
            ImeTakmicaraLabel.Text ="Takmičar: " + t.Ime + " " + t.Prezime;
            ImeKlubaLabel.Text = "Klub: Karate klub \"" + t.Klub.Naziv + "\"";
            for(Pojas pojas = t.Pojas + 1; pojas <= Pojas.Crni; ++pojas)
            {
                PojasComboBox.Items.Add(pojas);
            }
        }

        private void Dugme_Odustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Dugme_Potvrdi_Click(object sender, EventArgs e)
        {
            Pojas noviPojas = (Pojas)PojasComboBox.SelectedItem;
            TakmicarDAO takmicarDAO = new TakmicarDAO();
            if(takmicarDAO.PromijeniPojas(takmicar, noviPojas))
            {
                statusLabel.Text = "Uspješno promjenjen pojas";
                statusLabel.ForeColor = Color.Green;
            }
            else
            {
                statusLabel.Text = "Neuspješna promjena pojasa";
                statusLabel.ForeColor = Color.Red;
            }
            this.Close();
        }
    }
}
