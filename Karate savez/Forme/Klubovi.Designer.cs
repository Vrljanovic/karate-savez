﻿namespace Karate_savez
{
    partial class Klubovi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.TabelaKlubova = new System.Windows.Forms.DataGridView();
            this.JIB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Aktivan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Naziv = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mjesto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Adresa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Predstavnik = new System.Windows.Forms.DataGridViewLinkColumn();
            this.Dugme_Dodaj = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Dugme_PromjenaPredstavnika = new System.Windows.Forms.Button();
            this.Dugme_PromijeniAktivnost = new System.Windows.Forms.Button();
            this.Dugme_IzmijeniPodatke = new System.Windows.Forms.Button();
            this.Dugme_Rezultati = new System.Windows.Forms.Button();
            this.StatusBar = new System.Windows.Forms.StatusStrip();
            this.statusStripLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.Pretrazi_TextBox = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.Pretraga_ComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Dugme_Nazad = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TabelaKlubova)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.StatusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabelaKlubova
            // 
            this.TabelaKlubova.AllowUserToAddRows = false;
            this.TabelaKlubova.AllowUserToDeleteRows = false;
            this.TabelaKlubova.AllowUserToResizeColumns = false;
            this.TabelaKlubova.AllowUserToResizeRows = false;
            this.TabelaKlubova.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabelaKlubova.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TabelaKlubova.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.JIB,
            this.Aktivan,
            this.Naziv,
            this.Mjesto,
            this.Adresa,
            this.Predstavnik});
            this.TabelaKlubova.Location = new System.Drawing.Point(0, 88);
            this.TabelaKlubova.MultiSelect = false;
            this.TabelaKlubova.Name = "TabelaKlubova";
            this.TabelaKlubova.ReadOnly = true;
            this.TabelaKlubova.RowHeadersVisible = false;
            this.TabelaKlubova.RowTemplate.Height = 24;
            this.TabelaKlubova.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TabelaKlubova.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TabelaKlubova.ShowCellErrors = false;
            this.TabelaKlubova.ShowCellToolTips = false;
            this.TabelaKlubova.ShowEditingIcon = false;
            this.TabelaKlubova.Size = new System.Drawing.Size(1650, 409);
            this.TabelaKlubova.TabIndex = 3;
            this.TabelaKlubova.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.TabelaKlubova_CellContentClick);
            // 
            // JIB
            // 
            this.JIB.HeaderText = "JIB";
            this.JIB.Name = "JIB";
            this.JIB.ReadOnly = true;
            this.JIB.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.JIB.Width = 150;
            // 
            // Aktivan
            // 
            this.Aktivan.HeaderText = "Aktivan";
            this.Aktivan.Name = "Aktivan";
            this.Aktivan.ReadOnly = true;
            // 
            // Naziv
            // 
            this.Naziv.HeaderText = "Naziv";
            this.Naziv.Name = "Naziv";
            this.Naziv.ReadOnly = true;
            this.Naziv.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Naziv.Width = 200;
            // 
            // Mjesto
            // 
            this.Mjesto.HeaderText = "Mjesto";
            this.Mjesto.Name = "Mjesto";
            this.Mjesto.ReadOnly = true;
            this.Mjesto.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Mjesto.Width = 200;
            // 
            // Adresa
            // 
            this.Adresa.HeaderText = "Adresa";
            this.Adresa.Name = "Adresa";
            this.Adresa.ReadOnly = true;
            this.Adresa.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Adresa.Width = 200;
            // 
            // Predstavnik
            // 
            this.Predstavnik.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Predstavnik.HeaderText = "Predstavnik";
            this.Predstavnik.Name = "Predstavnik";
            this.Predstavnik.ReadOnly = true;
            this.Predstavnik.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Predstavnik.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Dugme_Dodaj
            // 
            this.Dugme_Dodaj.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_Dodaj.Location = new System.Drawing.Point(25, 21);
            this.Dugme_Dodaj.Name = "Dugme_Dodaj";
            this.Dugme_Dodaj.Size = new System.Drawing.Size(190, 39);
            this.Dugme_Dodaj.TabIndex = 0;
            this.Dugme_Dodaj.Text = "Dodaj klub";
            this.Dugme_Dodaj.UseVisualStyleBackColor = true;
            this.Dugme_Dodaj.Click += new System.EventHandler(this.Dugme_Dodaj_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.Dugme_PromjenaPredstavnika);
            this.groupBox1.Controls.Add(this.Dugme_PromijeniAktivnost);
            this.groupBox1.Controls.Add(this.Dugme_IzmijeniPodatke);
            this.groupBox1.Controls.Add(this.Dugme_Dodaj);
            this.groupBox1.Location = new System.Drawing.Point(12, 536);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(980, 83);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Upravljanje Klubovima";
            // 
            // Dugme_PromjenaPredstavnika
            // 
            this.Dugme_PromjenaPredstavnika.BackColor = System.Drawing.Color.Transparent;
            this.Dugme_PromjenaPredstavnika.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_PromjenaPredstavnika.Location = new System.Drawing.Point(500, 21);
            this.Dugme_PromjenaPredstavnika.Name = "Dugme_PromjenaPredstavnika";
            this.Dugme_PromjenaPredstavnika.Size = new System.Drawing.Size(244, 39);
            this.Dugme_PromjenaPredstavnika.TabIndex = 2;
            this.Dugme_PromjenaPredstavnika.Text = "Promijeni predstavnika";
            this.Dugme_PromjenaPredstavnika.UseVisualStyleBackColor = false;
            this.Dugme_PromjenaPredstavnika.Click += new System.EventHandler(this.Dugme_PromjenaPredstavnika_Click);
            // 
            // Dugme_PromijeniAktivnost
            // 
            this.Dugme_PromijeniAktivnost.BackColor = System.Drawing.Color.Transparent;
            this.Dugme_PromijeniAktivnost.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_PromijeniAktivnost.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Dugme_PromijeniAktivnost.Location = new System.Drawing.Point(236, 21);
            this.Dugme_PromijeniAktivnost.Name = "Dugme_PromijeniAktivnost";
            this.Dugme_PromijeniAktivnost.Size = new System.Drawing.Size(244, 39);
            this.Dugme_PromijeniAktivnost.TabIndex = 1;
            this.Dugme_PromijeniAktivnost.Text = "Promijeni aktivnost";
            this.Dugme_PromijeniAktivnost.UseVisualStyleBackColor = false;
            this.Dugme_PromijeniAktivnost.Click += new System.EventHandler(this.Dugme_PromijeniAktivnost_Click);
            // 
            // Dugme_IzmijeniPodatke
            // 
            this.Dugme_IzmijeniPodatke.BackColor = System.Drawing.Color.Transparent;
            this.Dugme_IzmijeniPodatke.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_IzmijeniPodatke.Location = new System.Drawing.Point(765, 21);
            this.Dugme_IzmijeniPodatke.Name = "Dugme_IzmijeniPodatke";
            this.Dugme_IzmijeniPodatke.Size = new System.Drawing.Size(190, 39);
            this.Dugme_IzmijeniPodatke.TabIndex = 3;
            this.Dugme_IzmijeniPodatke.Text = "Izmijeni podatke";
            this.Dugme_IzmijeniPodatke.UseVisualStyleBackColor = false;
            this.Dugme_IzmijeniPodatke.Click += new System.EventHandler(this.Dugme_IzmijeniPodatke_Click);
            // 
            // Dugme_Rezultati
            // 
            this.Dugme_Rezultati.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Dugme_Rezultati.BackColor = System.Drawing.Color.Transparent;
            this.Dugme_Rezultati.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_Rezultati.Location = new System.Drawing.Point(1195, 557);
            this.Dugme_Rezultati.Name = "Dugme_Rezultati";
            this.Dugme_Rezultati.Size = new System.Drawing.Size(195, 39);
            this.Dugme_Rezultati.TabIndex = 5;
            this.Dugme_Rezultati.Text = "Rezultati kluba";
            this.Dugme_Rezultati.UseVisualStyleBackColor = false;
            this.Dugme_Rezultati.Click += new System.EventHandler(this.Dugme_Rezultati_Click);
            // 
            // StatusBar
            // 
            this.StatusBar.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.StatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusStripLabel});
            this.StatusBar.Location = new System.Drawing.Point(0, 631);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.StatusBar.Size = new System.Drawing.Size(1402, 22);
            this.StatusBar.TabIndex = 6;
            this.StatusBar.Text = "Ready";
            // 
            // statusStripLabel
            // 
            this.statusStripLabel.Name = "statusStripLabel";
            this.statusStripLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // Pretrazi_TextBox
            // 
            this.Pretrazi_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pretrazi_TextBox.Location = new System.Drawing.Point(267, 28);
            this.Pretrazi_TextBox.Name = "Pretrazi_TextBox";
            this.Pretrazi_TextBox.Size = new System.Drawing.Size(400, 38);
            this.Pretrazi_TextBox.TabIndex = 2;
            this.toolTip1.SetToolTip(this.Pretrazi_TextBox, "Unesite ime kluba bez \"Karate klub\"");
            this.Pretrazi_TextBox.TextChanged += new System.EventHandler(this.Pretrazi_TextBox_TextChanged);
            // 
            // toolTip1
            // 
            this.toolTip1.ShowAlways = true;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip1.ToolTipTitle = "Napomena";
            // 
            // Pretraga_ComboBox
            // 
            this.Pretraga_ComboBox.DisplayMember = "Po nazivu";
            this.Pretraga_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Pretraga_ComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.Pretraga_ComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pretraga_ComboBox.FormattingEnabled = true;
            this.Pretraga_ComboBox.Items.AddRange(new object[] {
            "Po gradu",
            "Po nazivu"});
            this.Pretraga_ComboBox.Location = new System.Drawing.Point(12, 28);
            this.Pretraga_ComboBox.Name = "Pretraga_ComboBox";
            this.Pretraga_ComboBox.Size = new System.Drawing.Size(227, 37);
            this.Pretraga_ComboBox.TabIndex = 1;
            this.Pretraga_ComboBox.ValueMember = "Po nazivu";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pretraži klubove:";
            // 
            // Dugme_Nazad
            // 
            this.Dugme_Nazad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Dugme_Nazad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Nazad.Location = new System.Drawing.Point(1242, 27);
            this.Dugme_Nazad.Name = "Dugme_Nazad";
            this.Dugme_Nazad.Size = new System.Drawing.Size(140, 38);
            this.Dugme_Nazad.TabIndex = 7;
            this.Dugme_Nazad.Text = "Nazad";
            this.Dugme_Nazad.UseVisualStyleBackColor = true;
            this.Dugme_Nazad.Click += new System.EventHandler(this.Dugme_Nazad_Click);
            // 
            // Klubovi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1402, 653);
            this.Controls.Add(this.Dugme_Nazad);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Pretraga_ComboBox);
            this.Controls.Add(this.Pretrazi_TextBox);
            this.Controls.Add(this.StatusBar);
            this.Controls.Add(this.Dugme_Rezultati);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TabelaKlubova);
            this.MinimumSize = new System.Drawing.Size(1420, 700);
            this.Name = "Klubovi";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Klubovi";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Klubovi_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.TabelaKlubova)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.StatusBar.ResumeLayout(false);
            this.StatusBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView TabelaKlubova;
        private System.Windows.Forms.Button Dugme_Dodaj;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button Dugme_Rezultati;
        private System.Windows.Forms.Button Dugme_PromijeniAktivnost;
        private System.Windows.Forms.Button Dugme_IzmijeniPodatke;
        private System.Windows.Forms.StatusStrip StatusBar;
        private System.Windows.Forms.ToolStripStatusLabel statusStripLabel;
        private System.Windows.Forms.Button Dugme_PromjenaPredstavnika;
        private System.Windows.Forms.TextBox Pretrazi_TextBox;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ComboBox Pretraga_ComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn JIB;
        private System.Windows.Forms.DataGridViewTextBoxColumn Aktivan;
        private System.Windows.Forms.DataGridViewTextBoxColumn Naziv;
        private System.Windows.Forms.DataGridViewTextBoxColumn Mjesto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Adresa;
        private System.Windows.Forms.DataGridViewLinkColumn Predstavnik;
        private System.Windows.Forms.Button Dugme_Nazad;
    }
}