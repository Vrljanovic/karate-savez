﻿namespace Karate_savez.Forme
{
    partial class DelegirajSudije
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TabelaSudija = new System.Windows.Forms.DataGridView();
            this.Delegiraj = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.BrojSudijskeLicence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Prezime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DatumRodjenja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Uloga = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Dugme_Potvrdi = new System.Windows.Forms.Button();
            this.Dugme_Odustani = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TabelaSudija)).BeginInit();
            this.SuspendLayout();
            // 
            // TabelaSudija
            // 
            this.TabelaSudija.AllowUserToAddRows = false;
            this.TabelaSudija.AllowUserToDeleteRows = false;
            this.TabelaSudija.AllowUserToResizeColumns = false;
            this.TabelaSudija.AllowUserToResizeRows = false;
            this.TabelaSudija.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabelaSudija.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TabelaSudija.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Delegiraj,
            this.BrojSudijskeLicence,
            this.Ime,
            this.Prezime,
            this.DatumRodjenja,
            this.Uloga});
            this.TabelaSudija.Location = new System.Drawing.Point(0, -2);
            this.TabelaSudija.MultiSelect = false;
            this.TabelaSudija.Name = "TabelaSudija";
            this.TabelaSudija.RowHeadersVisible = false;
            this.TabelaSudija.RowTemplate.Height = 24;
            this.TabelaSudija.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TabelaSudija.Size = new System.Drawing.Size(1415, 433);
            this.TabelaSudija.TabIndex = 0;
            // 
            // Delegiraj
            // 
            this.Delegiraj.HeaderText = "Delegiraj";
            this.Delegiraj.Name = "Delegiraj";
            this.Delegiraj.Width = 150;
            // 
            // BrojSudijskeLicence
            // 
            this.BrojSudijskeLicence.HeaderText = "Broj sudijske licence";
            this.BrojSudijskeLicence.Name = "BrojSudijskeLicence";
            this.BrojSudijskeLicence.ReadOnly = true;
            this.BrojSudijskeLicence.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.BrojSudijskeLicence.Width = 200;
            // 
            // Ime
            // 
            this.Ime.HeaderText = "Ime";
            this.Ime.Name = "Ime";
            this.Ime.ReadOnly = true;
            this.Ime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Ime.Width = 200;
            // 
            // Prezime
            // 
            this.Prezime.HeaderText = "Prezime";
            this.Prezime.Name = "Prezime";
            this.Prezime.ReadOnly = true;
            this.Prezime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Prezime.Width = 200;
            // 
            // DatumRodjenja
            // 
            this.DatumRodjenja.HeaderText = "Datum rodjenja";
            this.DatumRodjenja.Name = "DatumRodjenja";
            this.DatumRodjenja.ReadOnly = true;
            this.DatumRodjenja.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.DatumRodjenja.Width = 150;
            // 
            // Uloga
            // 
            this.Uloga.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Uloga.HeaderText = "Uloga";
            this.Uloga.Items.AddRange(new object[] {
            "Pomoćni sudija",
            "Glavni sudija",
            "Vrhovni sudija"});
            this.Uloga.Name = "Uloga";
            this.Uloga.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // Dugme_Potvrdi
            // 
            this.Dugme_Potvrdi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Dugme_Potvrdi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_Potvrdi.Location = new System.Drawing.Point(1008, 449);
            this.Dugme_Potvrdi.Name = "Dugme_Potvrdi";
            this.Dugme_Potvrdi.Size = new System.Drawing.Size(188, 42);
            this.Dugme_Potvrdi.TabIndex = 1;
            this.Dugme_Potvrdi.Text = "Delegiraj sudije";
            this.Dugme_Potvrdi.UseVisualStyleBackColor = true;
            this.Dugme_Potvrdi.Click += new System.EventHandler(this.Dugme_Potvrdi_Click);
            // 
            // Dugme_Odustani
            // 
            this.Dugme_Odustani.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Dugme_Odustani.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_Odustani.Location = new System.Drawing.Point(1202, 449);
            this.Dugme_Odustani.Name = "Dugme_Odustani";
            this.Dugme_Odustani.Size = new System.Drawing.Size(188, 42);
            this.Dugme_Odustani.TabIndex = 2;
            this.Dugme_Odustani.Text = "Odustani";
            this.Dugme_Odustani.UseVisualStyleBackColor = true;
            this.Dugme_Odustani.Click += new System.EventHandler(this.Dugme_Odustani_Click);
            // 
            // DelegirajSudije
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1402, 503);
            this.Controls.Add(this.Dugme_Odustani);
            this.Controls.Add(this.Dugme_Potvrdi);
            this.Controls.Add(this.TabelaSudija);
            this.MinimumSize = new System.Drawing.Size(1420, 500);
            this.Name = "DelegirajSudije";
            this.Text = "Delegiraj sudije";
            ((System.ComponentModel.ISupportInitialize)(this.TabelaSudija)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView TabelaSudija;
        private System.Windows.Forms.Button Dugme_Potvrdi;
        private System.Windows.Forms.Button Dugme_Odustani;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Delegiraj;
        private System.Windows.Forms.DataGridViewTextBoxColumn BrojSudijskeLicence;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ime;
        private System.Windows.Forms.DataGridViewTextBoxColumn Prezime;
        private System.Windows.Forms.DataGridViewTextBoxColumn DatumRodjenja;
        private System.Windows.Forms.DataGridViewComboBoxColumn Uloga;
    }
}