﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class Takmicari : Form
    {
        private Form PocetnaForma;
        public Takmicari(Form PocetnaForma)
        {
            this.PocetnaForma = PocetnaForma;
            InitializeComponent();
            Pretraga_ComboBox.SelectedItem = "Po imenu";
            PopuniTabelu(null);
        }

        private void Takmicari_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.PocetnaForma.Show();
        }

        private void PopuniTabelu(List<Takmicar> takmicari)
        {
            if (takmicari == null)
            {
                TakmicarDAO clanDAO = new TakmicarDAO();
                takmicari = clanDAO.DohvatiSveTakmicare();
            }

            foreach (Takmicar c in takmicari)
                TabelaTakmicara.Rows.Add(c.BrojRegistracijeUSavezu, c.Pol, c.JeAktivan ? "Da" : "Ne", c.Ime, c.Prezime, c.DatumRodjenja.ToLongDateString(), c.Pojas, c.JeAktivan ? c.Klub.ToString() : "Nema kluba");
            TabelaTakmicara.Rows[0].Selected = true;
        }

        private void Dugme_DodajTakmicara_Click(object sender, EventArgs e)
        {
            new DodajTakmicara(StatusLabel, null).ShowDialog();
            TabelaTakmicara.Rows.Clear();
            PopuniTabelu(null);
        }

        private void Dugme_PromijeniAktivnost_Click(object sender, EventArgs e)
        {
            if (TabelaTakmicara.SelectedRows.Count > 0)
            {
                TakmicarDAO takmicarDAO = new TakmicarDAO();
                if (takmicarDAO.PromjeniAktivnost(takmicarDAO.DohvatiPoBrojuRegistracije((string)TabelaTakmicara.SelectedRows[0].Cells[0].Value)))
                {
                    StatusLabel.Text = "Uspješna promjena aktivnosti.";
                    StatusLabel.ForeColor = Color.Green;
                }
                else
                {
                    StatusLabel.Text = "Neuspješna promjena aktivnost.";
                    StatusLabel.ForeColor = Color.Red;
                }
                TabelaTakmicara.Rows.Clear();
                PopuniTabelu(null);
            }
        }

        private void Dugme_PromijeniPojas_Click(object sender, EventArgs e)
        {
            if (TabelaTakmicara.SelectedRows.Count > 0)
            {
                TakmicarDAO takmicarDAO = new TakmicarDAO();
                Takmicar selektovaniTakmicar = takmicarDAO.DohvatiPoBrojuRegistracije((string)TabelaTakmicara.SelectedRows[0].Cells[0].Value);
                new PromijeniPojas(StatusLabel, selektovaniTakmicar).ShowDialog();
                TabelaTakmicara.Rows.Clear();
                PopuniTabelu(null);
            }
        }

        private void TabelaTakmicara_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (TabelaTakmicara.SelectedRows.Count > 0)
            {
                Takmicar t = new TakmicarDAO().DohvatiPoBrojuRegistracije((string)TabelaTakmicara.SelectedRows[0].Cells[0].Value);
                if (t.Pojas == DTO.Pojas.Crni)
                    Dugme_PromijeniPojas.Enabled = false;
                else
                    Dugme_PromijeniPojas.Enabled = true;
            }
        }

        private void Dugme_PromijeniKlub_Click(object sender, EventArgs e)
        {
            if (TabelaTakmicara.SelectedRows.Count > 0)
            {
                Takmicar takmicar = new TakmicarDAO().DohvatiPoBrojuRegistracije((string)TabelaTakmicara.SelectedRows[0].Cells[0].Value);
                new PromijeniKlub(StatusLabel, takmicar).ShowDialog();
                TabelaTakmicara.Rows.Clear();
                PopuniTabelu(null);
            }
        }

        private void Dugme_Rezultati_Click(object sender, EventArgs e)
        {
            if (TabelaTakmicara.SelectedRows.Count > 0)
            {
                Takmicar t = new TakmicarDAO().DohvatiPoBrojuRegistracije((string)TabelaTakmicara.SelectedRows[0].Cells[0].Value);
                try
                {
                    new RezultatiTakmicara(t).ShowDialog();
                }
                catch (Exception)
                {
                    MessageBox.Show("Odabrani takmičar nema zapaženih rezultata");
                }
            }
        }

        private void Pretraga_TextBox_TextChanged(object sender, EventArgs e)
        {
            string str = Pretraga_TextBox.Text;
            TabelaTakmicara.Rows.Clear();
            if (string.Empty.Equals(str))
                PopuniTabelu(null);
            else
            {
                TakmicarDAO takmicarDAO = new TakmicarDAO();
                string nacinPretrage = (string)Pretraga_ComboBox.SelectedItem;
                if ("Po imenu".Equals(nacinPretrage))
                    PopuniTabelu(takmicarDAO.DohvatiPoImenu(str));
                else if ("Po klubu".Equals(nacinPretrage))
                    PopuniTabelu(takmicarDAO.DohvatiPoKlubu(str));
                else
                    PopuniTabelu(takmicarDAO.DohvatiPoPojasu(str));
            }
        }

        private void Dugme_Nazad_Click(object sender, EventArgs e)
        {
            this.PocetnaForma.Show();
            this.Close();
        }

        private void Dugme_IzmijeniPodatke_Click(object sender, EventArgs e)
        {
            if (TabelaTakmicara.SelectedRows.Count > 0)
            {
                Takmicar t = new TakmicarDAO().DohvatiPoBrojuRegistracije((string)TabelaTakmicara.SelectedRows[0].Cells[0].Value);
                new DodajTakmicara(StatusLabel, t).ShowDialog();
            }
        }
    }
}