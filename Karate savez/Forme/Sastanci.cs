﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class Sastanci : Form
    {
        private Form parentForm;
        public Sastanci(Form parentForm)
        {
            this.parentForm = parentForm;
            InitializeComponent();
            PopuniTabelu(null);
        }

        private void PopuniTabelu(List<Sastanak> svi)
        {
            TabelaSastanaka.Rows.Clear();
            if(svi == null)
                 svi = new SastanakDAO().DohvatiSveSastanke();
            svi.ForEach(s => TabelaSastanaka.Rows.Add(s.IdSastanka, s.DatumOdrzavanja.ToShortDateString(), s.Tema, "Učesnici"));
        }

        private void Sastanci_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            parentForm.Show();
        }

        private void Dugme_DodajSastanak_Click(object sender, EventArgs e)
        {
            new DodajSastanak().ShowDialog();
            TabelaSastanaka.Rows.Clear();
            PopuniTabelu(null);
        }

        private void TabelaSastanaka_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn && e.RowIndex >= 0)
            {
                int idSastanka = (int)TabelaSastanaka.Rows[e.RowIndex].Cells[0].Value;
                Sastanak s = new SastanakDAO().DohvatiSastanakPoIDu(idSastanka);
                if(s != null)
                    new PrisutniNaSastanku(s).ShowDialog();
            }
        }

        private void Pretrazi_Sastanak_TextChanged(object sender, EventArgs e)
        {
            string str = Pretrazi_Sastanak.Text;
            if (string.Empty.Equals(str))
                PopuniTabelu(null);
            else
                PopuniTabelu(new SastanakDAO().DohvatiPoTemi(str));
        }

        private void Dugme_Nazad_Click(object sender, EventArgs e)
        {
            this.parentForm.Show();
            this.Close();
        }
    }
}
