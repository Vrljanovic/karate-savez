﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class DelegirajSudije : Form
    {
        private static List<Tuple<Sudija, string>> trenutnoDelegiraneSudije = new List<Tuple<Sudija, string>>();
        private Takmicenje takmicenje;
        private ToolStripStatusLabel label;

        public DelegirajSudije(Takmicenje takmicenje, ToolStripStatusLabel label)
        {
            this.label = label;
            this.takmicenje = takmicenje;
            InitializeComponent();
            PopuniTabelu();
        }

        private void Dugme_Odustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void PopuniTabelu()
        {
            SudijaDAO sudijaDAO = new SudijaDAO();
            List<Sudija> aktivneSudije = sudijaDAO.DohvatiSveAktivneSudije();
            foreach (Sudija s in aktivneSudije)
            {
                string uloga = "Pomoćni sudija";
                bool jeDelegiran = sudijaDAO.JeDelegiranZaTakmicenje(s, takmicenje);
                if (jeDelegiran)
                {
                   
                    uloga = sudijaDAO.UlogaSudijeNaTakmicenju(s, takmicenje);
                    trenutnoDelegiraneSudije.Add(new Tuple<Sudija, string>(s, uloga));
                }
                TabelaSudija.Rows.Add(jeDelegiran, s.BrojSudijskeLicence, s.Ime, s.Prezime, s.DatumRodjenja.ToShortDateString(), uloga);
            }
        }

        private void Dugme_Potvrdi_Click(object sender, EventArgs e)
        {
            List<Tuple<Sudija, string>> delegiraneSudije = new List<Tuple<Sudija, string>>();
            SudijaDAO sudijaDAO = new SudijaDAO();
            for(int i = 0; i < TabelaSudija.RowCount; ++i)
            {
                if ((bool)TabelaSudija.Rows[i].Cells[0].Value)
                    delegiraneSudije.Add(new Tuple<Sudija, string>(sudijaDAO.DohvatiSudijuPoBrojuLicence((string)TabelaSudija.Rows[i].Cells[1].Value), (string)TabelaSudija.Rows[i].Cells[5].Value));
            }
            delegiraneSudije.ForEach(s => Console.WriteLine(s.Item1 + " ---- " + s.Item2));
            if(delegiraneSudije.FindAll(s => "Vrhovni sudija".Equals(s.Item2)).Count != 1)
            {
                MessageBox.Show("Na takmičenju mra biti tačno jedan vrhovni sudija!", "Greška");
                return;
            }
            List<Tuple<Sudija, string>> pom = new List<Tuple<Sudija, string>>();
            pom.AddRange(delegiraneSudije);
            delegiraneSudije.RemoveAll(s => trenutnoDelegiraneSudije.Contains(s));
            trenutnoDelegiraneSudije.RemoveAll(s => pom.Contains(s));
            trenutnoDelegiraneSudije.ForEach(s => sudijaDAO.UkloniSaTakmicenja(s.Item1, takmicenje));
            delegiraneSudije.ForEach(s => sudijaDAO.DelegirajSudiju(s.Item1, takmicenje, s.Item2));
            label.Text = "Sudije uspješno delegirane";
            label.ForeColor = Color.Green;
            this.Close();
        }
    }
}
