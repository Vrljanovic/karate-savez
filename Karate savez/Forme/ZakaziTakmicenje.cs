﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class ZakaziTakmicenje : Form
    {
        public ZakaziTakmicenje()
        {
            InitializeComponent();
        }

        private void Dugme_Odustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Dugme_Potvrdi_Click(object sender, EventArgs e)
        {
            if(MjestoOdrzavanjaTextBox.Text.Equals(string.Empty) || AdresaTextBox.Text.Equals(string.Empty) || BrojBorilistaTextBox.Text.Equals(string.Empty))
            {
                MessageBox.Show("Niste popunili sva polja");
                return;
            }
            if (DialogResult.OK == MessageBox.Show("Da li su unijeti podaci tačni ? Ako ste sigurni stisnite dugme \"OK\"", "Upozorenje", MessageBoxButtons.OKCancel))
            {
                try
                {
                    int brojBorilista = Int32.Parse(BrojBorilistaTextBox.Text);
                    Mjesto mjesto = new Mjesto(0, MjestoOdrzavanjaTextBox.Text, AdresaTextBox.Text);
                    Takmicenje takmicenje = new Takmicenje(0, PocetakTakmicenjaPicker.Value, brojBorilista, mjesto);
                    new TakmicenjeDAO().DodajTakmicenje(takmicenje);
                    this.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    MessageBox.Show("Broj borilišta nije validan.");
                }
            }
        }

        private void PocetakTakmicenjaPicker_ValueChanged(object sender, EventArgs e)
        {
            Dugme_Potvrdi.Enabled = true;
            if (PocetakTakmicenjaPicker.Value < DateTime.Today)
                Dugme_Potvrdi.Enabled = false;
        }
    }
}
