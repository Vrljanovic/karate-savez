﻿namespace Karate_savez.Forme
{
    partial class PromijeniPojas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ImeTakmicaraLabel = new System.Windows.Forms.Label();
            this.ImeKlubaLabel = new System.Windows.Forms.Label();
            this.PojasComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Dugme_Potvrdi = new System.Windows.Forms.Button();
            this.Dugme_Odustani = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ImeTakmicaraLabel
            // 
            this.ImeTakmicaraLabel.AutoSize = true;
            this.ImeTakmicaraLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ImeTakmicaraLabel.Location = new System.Drawing.Point(58, 28);
            this.ImeTakmicaraLabel.Name = "ImeTakmicaraLabel";
            this.ImeTakmicaraLabel.Size = new System.Drawing.Size(0, 17);
            this.ImeTakmicaraLabel.TabIndex = 0;
            // 
            // ImeKlubaLabel
            // 
            this.ImeKlubaLabel.AutoSize = true;
            this.ImeKlubaLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ImeKlubaLabel.Location = new System.Drawing.Point(58, 55);
            this.ImeKlubaLabel.Name = "ImeKlubaLabel";
            this.ImeKlubaLabel.Size = new System.Drawing.Size(0, 17);
            this.ImeKlubaLabel.TabIndex = 1;
            // 
            // PojasComboBox
            // 
            this.PojasComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.PojasComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.PojasComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PojasComboBox.FormattingEnabled = true;
            this.PojasComboBox.Location = new System.Drawing.Point(61, 132);
            this.PojasComboBox.Name = "PojasComboBox";
            this.PojasComboBox.Size = new System.Drawing.Size(268, 24);
            this.PojasComboBox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(58, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Odaberi Pojas";
            // 
            // Dugme_Potvrdi
            // 
            this.Dugme_Potvrdi.BackColor = System.Drawing.Color.Transparent;
            this.Dugme_Potvrdi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_Potvrdi.Location = new System.Drawing.Point(32, 206);
            this.Dugme_Potvrdi.Name = "Dugme_Potvrdi";
            this.Dugme_Potvrdi.Size = new System.Drawing.Size(149, 44);
            this.Dugme_Potvrdi.TabIndex = 4;
            this.Dugme_Potvrdi.Text = "Potvrdi";
            this.Dugme_Potvrdi.UseVisualStyleBackColor = false;
            this.Dugme_Potvrdi.Click += new System.EventHandler(this.Dugme_Potvrdi_Click);
            // 
            // Dugme_Odustani
            // 
            this.Dugme_Odustani.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Odustani.Location = new System.Drawing.Point(204, 207);
            this.Dugme_Odustani.Name = "Dugme_Odustani";
            this.Dugme_Odustani.Size = new System.Drawing.Size(149, 44);
            this.Dugme_Odustani.TabIndex = 5;
            this.Dugme_Odustani.Text = "Odustani";
            this.Dugme_Odustani.UseVisualStyleBackColor = true;
            this.Dugme_Odustani.Click += new System.EventHandler(this.Dugme_Odustani_Click);
            // 
            // PromijeniPojas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 303);
            this.Controls.Add(this.Dugme_Odustani);
            this.Controls.Add(this.Dugme_Potvrdi);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PojasComboBox);
            this.Controls.Add(this.ImeKlubaLabel);
            this.Controls.Add(this.ImeTakmicaraLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PromijeniPojas";
            this.Text = "Promijeni pojas";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ImeTakmicaraLabel;
        private System.Windows.Forms.Label ImeKlubaLabel;
        private System.Windows.Forms.ComboBox PojasComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Dugme_Potvrdi;
        private System.Windows.Forms.Button Dugme_Odustani;
    }
}