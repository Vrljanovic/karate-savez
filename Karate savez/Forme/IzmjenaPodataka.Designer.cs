﻿namespace Karate_savez.Forme
{
    partial class IzmjenaPodataka
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.JIBTextBox = new System.Windows.Forms.TextBox();
            this.NazivKlubaTextBox = new System.Windows.Forms.TextBox();
            this.MjestoTextBox = new System.Windows.Forms.TextBox();
            this.AdresaTextBox = new System.Windows.Forms.TextBox();
            this.Label_ImeKluba = new System.Windows.Forms.Label();
            this.Dugme_Potvrdi = new System.Windows.Forms.Button();
            this.Dugme_Odustani = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "JIB";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Naziv kluba";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(51, 202);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Mjesto";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(51, 258);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Adresa";
            // 
            // JIBTextBox
            // 
            this.JIBTextBox.Location = new System.Drawing.Point(54, 110);
            this.JIBTextBox.Name = "JIBTextBox";
            this.JIBTextBox.Size = new System.Drawing.Size(282, 22);
            this.JIBTextBox.TabIndex = 2;
            // 
            // NazivKlubaTextBox
            // 
            this.NazivKlubaTextBox.Location = new System.Drawing.Point(54, 166);
            this.NazivKlubaTextBox.Name = "NazivKlubaTextBox";
            this.NazivKlubaTextBox.Size = new System.Drawing.Size(282, 22);
            this.NazivKlubaTextBox.TabIndex = 4;
            // 
            // MjestoTextBox
            // 
            this.MjestoTextBox.Location = new System.Drawing.Point(54, 222);
            this.MjestoTextBox.Name = "MjestoTextBox";
            this.MjestoTextBox.Size = new System.Drawing.Size(282, 22);
            this.MjestoTextBox.TabIndex = 6;
            // 
            // AdresaTextBox
            // 
            this.AdresaTextBox.Location = new System.Drawing.Point(54, 278);
            this.AdresaTextBox.Name = "AdresaTextBox";
            this.AdresaTextBox.Size = new System.Drawing.Size(282, 22);
            this.AdresaTextBox.TabIndex = 8;
            // 
            // Label_ImeKluba
            // 
            this.Label_ImeKluba.AutoSize = true;
            this.Label_ImeKluba.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label_ImeKluba.Location = new System.Drawing.Point(49, 37);
            this.Label_ImeKluba.Name = "Label_ImeKluba";
            this.Label_ImeKluba.Size = new System.Drawing.Size(0, 25);
            this.Label_ImeKluba.TabIndex = 0;
            // 
            // Dugme_Potvrdi
            // 
            this.Dugme_Potvrdi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_Potvrdi.Location = new System.Drawing.Point(30, 325);
            this.Dugme_Potvrdi.Name = "Dugme_Potvrdi";
            this.Dugme_Potvrdi.Size = new System.Drawing.Size(145, 49);
            this.Dugme_Potvrdi.TabIndex = 9;
            this.Dugme_Potvrdi.Text = "Potvrdi";
            this.Dugme_Potvrdi.UseVisualStyleBackColor = true;
            this.Dugme_Potvrdi.Click += new System.EventHandler(this.Dugme_Potvrdi_Click);
            // 
            // Dugme_Odustani
            // 
            this.Dugme_Odustani.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Odustani.Location = new System.Drawing.Point(204, 326);
            this.Dugme_Odustani.Name = "Dugme_Odustani";
            this.Dugme_Odustani.Size = new System.Drawing.Size(145, 48);
            this.Dugme_Odustani.TabIndex = 10;
            this.Dugme_Odustani.Text = "Odustani";
            this.Dugme_Odustani.UseVisualStyleBackColor = true;
            this.Dugme_Odustani.Click += new System.EventHandler(this.Dugme_Odustani_Click);
            // 
            // IzmjenaPodataka
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 403);
            this.Controls.Add(this.Dugme_Odustani);
            this.Controls.Add(this.Dugme_Potvrdi);
            this.Controls.Add(this.Label_ImeKluba);
            this.Controls.Add(this.AdresaTextBox);
            this.Controls.Add(this.MjestoTextBox);
            this.Controls.Add(this.NazivKlubaTextBox);
            this.Controls.Add(this.JIBTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "IzmjenaPodataka";
            this.Text = "Izmjena podataka";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox JIBTextBox;
        private System.Windows.Forms.TextBox NazivKlubaTextBox;
        private System.Windows.Forms.TextBox MjestoTextBox;
        private System.Windows.Forms.TextBox AdresaTextBox;
        private System.Windows.Forms.Label Label_ImeKluba;
        private System.Windows.Forms.Button Dugme_Potvrdi;
        private System.Windows.Forms.Button Dugme_Odustani;
    }
}