﻿namespace Karate_savez.Forme
{
    partial class PrisutniNaSastanku
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tekst = new System.Windows.Forms.Label();
            this.PrisutniListBox = new System.Windows.Forms.ListBox();
            this.Dugme_Zatvori = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tekst
            // 
            this.tekst.AutoSize = true;
            this.tekst.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tekst.Location = new System.Drawing.Point(0, 25);
            this.tekst.Name = "tekst";
            this.tekst.Size = new System.Drawing.Size(0, 20);
            this.tekst.TabIndex = 0;
            // 
            // PrisutniListBox
            // 
            this.PrisutniListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PrisutniListBox.FormattingEnabled = true;
            this.PrisutniListBox.ItemHeight = 18;
            this.PrisutniListBox.Location = new System.Drawing.Point(-1, 61);
            this.PrisutniListBox.Name = "PrisutniListBox";
            this.PrisutniListBox.Size = new System.Drawing.Size(385, 256);
            this.PrisutniListBox.TabIndex = 1;
            // 
            // Dugme_Zatvori
            // 
            this.Dugme_Zatvori.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Zatvori.Location = new System.Drawing.Point(242, 332);
            this.Dugme_Zatvori.Name = "Dugme_Zatvori";
            this.Dugme_Zatvori.Size = new System.Drawing.Size(128, 40);
            this.Dugme_Zatvori.TabIndex = 2;
            this.Dugme_Zatvori.Text = "Zatvori";
            this.Dugme_Zatvori.UseVisualStyleBackColor = true;
            this.Dugme_Zatvori.Click += new System.EventHandler(this.Dugme_Zatvori_Click);
            // 
            // PrisutniNaSastanku
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 384);
            this.Controls.Add(this.Dugme_Zatvori);
            this.Controls.Add(this.PrisutniListBox);
            this.Controls.Add(this.tekst);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PrisutniNaSastanku";
            this.Text = "Prisutni na sastanku";
            this.Load += new System.EventHandler(this.PrisutniNaSastanku_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label tekst;
        private System.Windows.Forms.ListBox PrisutniListBox;
        private System.Windows.Forms.Button Dugme_Zatvori;
    }
}