﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class DodajTakmicara : Form
    {
        private static ToolStripStatusLabel statusLabel;
        private Takmicar zaPromjenu;

        public DodajTakmicara(ToolStripStatusLabel label, Takmicar zaPromjenu)
        {
            statusLabel = label;
            this.zaPromjenu = zaPromjenu;
            InitializeComponent();
            List<Klub> sviKlubovi = new KlubDAO().DohvatiSveAktivneKlubove();
            foreach (Klub k in sviKlubovi)
                KlubComboBox.Items.Add(k);
            PojasComboBox.Items.AddRange(new object[]{ Pojas.Bijeli, Pojas.Zuti, Pojas.Narandzasti, Pojas.Zeleni, Pojas.Plavi, Pojas.Smedji, Pojas.Crni});
        }

        private void Dugme_DodajTakmicara_Click(object sender, EventArgs e)
        {
            if (zaPromjenu == null)
            {
                if (ImeTextBox.Text.Equals(string.Empty) || PrezimeTextBox.Text.Equals(string.Empty) || BrojRegistracijeTextBox.Text.Equals(string.Empty) || KlubComboBox.SelectedItem == null || PojasComboBox.SelectedItem == null || DatumRodjenjaPicker.Value == DateTime.Today || PolComboBox.SelectedItem == null)
                {
                    MessageBox.Show("Popunite sva polja, odaberite pojas, klub i datum rodjenja.");
                    return;
                }
                if (DialogResult.OK == MessageBox.Show("Da li su unijeti podaci tačni ? Ako ste sigurni stisnite dugme \"OK\"", "Upozorenje", MessageBoxButtons.OKCancel))
                {
                    string Pol = (string)PolComboBox.SelectedItem;
                    Klub k = (Klub)KlubComboBox.SelectedItem;
                    Pojas p = (Pojas)PojasComboBox.SelectedItem;
                    Takmicar t = new Takmicar(0, ImeTextBox.Text, PrezimeTextBox.Text, DatumRodjenjaPicker.Value, Pol, BrojRegistracijeTextBox.Text, k, p);
                    if (new TakmicarDAO().DodajTakmicara(t))
                    {
                        statusLabel.Text = "Novi takmičar je uspješno dodan";
                        statusLabel.ForeColor = Color.Green;
                    }
                    else
                    {
                        statusLabel.Text = "Neuspješno dodavanje takmičara.";
                        statusLabel.ForeColor = Color.Red;
                    }
                }
            }
        }

        private void Dugme_Odustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}