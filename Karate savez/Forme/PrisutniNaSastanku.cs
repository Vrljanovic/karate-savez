﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class PrisutniNaSastanku : Form
    {
        private Sastanak sastanak;
        public PrisutniNaSastanku(Sastanak sastanak)
        {
            InitializeComponent();
            this.sastanak = sastanak;
            tekst.Text = "Prisutni na sastanku održanom " + sastanak.DatumOdrzavanja.ToShortDateString() + ":";
        }

        private void PrisutniNaSastanku_Load(object sender, EventArgs e)
        {
            List<Osoba> prisutni = new SastanakDAO().DohvatiPrisutneNaSastanku(sastanak);
            prisutni.ForEach(p => PrisutniListBox.Items.Add(p.Ime + " " + p.Prezime));
        }

        private void Dugme_Zatvori_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
