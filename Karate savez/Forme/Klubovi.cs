﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Karate_savez.DAO;
using Karate_savez.DTO;
using Karate_savez.Forme;
using MySql.Data.MySqlClient;

namespace Karate_savez
{
    public partial class Klubovi : Form
    {
        private Form pocetnaForma;
        public Klubovi(Form pocetnaForma)
        {
            this.pocetnaForma = pocetnaForma;
            InitializeComponent();
            Pretraga_ComboBox.SelectedItem = "Po nazivu";
            PopuniTabelu(null);

        }

        public void PopuniTabelu(List<Klub> klubovi)
        {
            if (klubovi == null)
            {
                KlubDAO klubDao = new KlubDAO();
                klubovi = klubDao.DohvatiKlubove();
            }
            
            foreach(Klub k in klubovi)
            {
                TabelaKlubova.Rows.Add(k.JIB, k.JeAktivan ? "Da" : "Ne", k.Naziv, k.Sjediste.Naziv, k.Sjediste.Adresa, k.PredstavnikKluba.Ime + " " + k.PredstavnikKluba.Prezime);
            }
        }

        private void Dugme_Dodaj_Click(object sender, EventArgs e)
        {
            KlubInfo klubInfo = new KlubInfo(statusStripLabel);
            klubInfo.ShowDialog();
            TabelaKlubova.Rows.Clear();
            PopuniTabelu(null);
        }

        private void Klubovi_FormClosed(object sender, FormClosedEventArgs e)
        {
            pocetnaForma.Show();
        }

        private void Dugme_PromijeniAktivnost_Click(object sender, EventArgs e)
        {
            if (TabelaKlubova.SelectedRows.Count > 0)
            {
                KlubDAO klubDAO = new KlubDAO();
                Klub klub = klubDAO.DohvatiKlubPoJIBu((string)TabelaKlubova.SelectedRows[0].Cells[0].Value);
                if (klubDAO.PromjeniAktivnost(klub))
                {
                    statusStripLabel.Text = "Aktivnost kluba je promjenjena.";
                    statusStripLabel.ForeColor = Color.Green;
                }
                else
                {
                    statusStripLabel.Text = "Neuspješna promjena aktivnosti kluba";
                    statusStripLabel.ForeColor = Color.Red;
                }
                TabelaKlubova.Rows.Clear();
                PopuniTabelu(null);
            }
        }

        private void Dugme_IzmijeniPodatke_Click(object sender, EventArgs e)
        {
            if (TabelaKlubova.SelectedRows.Count > 0)
            {
                Klub k = new KlubDAO().DohvatiKlubPoJIBu((string)TabelaKlubova.SelectedRows[0].Cells[0].Value);
                new IzmjenaPodataka(k, this.statusStripLabel).ShowDialog();
                TabelaKlubova.Rows.Clear();
                PopuniTabelu(null);
            }
        }

        private void Dugme_PromjenaPredstavnika_Click(object sender, EventArgs e)
        {
            if (TabelaKlubova.SelectedRows.Count > 0)
            {
                Klub k = new KlubDAO().DohvatiKlubPoJIBu((string)TabelaKlubova.SelectedRows[0].Cells[0].Value);
                new PromjenaPredstavnika(k, this.statusStripLabel).ShowDialog();
                TabelaKlubova.Rows.Clear();
                PopuniTabelu(null);
            }
        }

        private void Dugme_Rezultati_Click(object sender, EventArgs e)
        {
            if (TabelaKlubova.SelectedRows.Count > 0)
            {
                Klub k = new KlubDAO().DohvatiKlubPoJIBu((string)TabelaKlubova.SelectedRows[0].Cells[0].Value);
                try
                {
                    new Forme.RezultatKluba(k).ShowDialog();
                }
                catch (Exception)
                {
                    MessageBox.Show("Odabrani klub nema zapaženih rezultata.", "Informacija");
                }
            }
        }

        private void Pretrazi_TextBox_TextChanged(object sender, EventArgs e)
        {
            TabelaKlubova.Rows.Clear();
            string str = Pretrazi_TextBox.Text;
            if (string.Empty.Equals(str))
                PopuniTabelu(null);
            else
            {
                if (Pretraga_ComboBox.SelectedItem.Equals("Po nazivu"))
                {
                    if (str.ToLower().Contains("karate klub "))
                        str = str.ToLower().Replace("karate klub ", string.Empty);

                    PopuniTabelu(new KlubDAO().DohvatiKluboveCijiNazivSadrzi(str));
                }
                else
                    PopuniTabelu(new KlubDAO().DohvatiKluboveIzGrada(str));
            }
        }

        private void TabelaKlubova_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewLinkColumn && e.RowIndex >= 0)
            {
                string jib = (string)TabelaKlubova.Rows[e.RowIndex].Cells[0].Value;
                Klub klub = new KlubDAO().DohvatiKlubPoJIBu(jib);
                MessageBox.Show("Predstavnik karate kluba \"" + klub.Naziv + "\"\n\nIme i prezime: " + klub.PredstavnikKluba.Ime + " " + klub.PredstavnikKluba.Prezime + "\nBroj telefona: " + klub.PredstavnikKluba.BrojTelefona
                    + "\nE-mail: " + klub.PredstavnikKluba.EmailAdresa, "Podaci o predstavniku", MessageBoxButtons.OK);
            }
        }

        private void Dugme_Nazad_Click(object sender, EventArgs e)
        {
            this.pocetnaForma.Show();
            this.Close();
        }
    }
}