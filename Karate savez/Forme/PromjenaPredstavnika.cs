﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class PromjenaPredstavnika : Form
    {
        private static Klub klub = null;
        private static ToolStripStatusLabel statusLabel = null;
        public PromjenaPredstavnika(Klub k, ToolStripStatusLabel l)
        {
            klub = k;
            statusLabel = l;
            InitializeComponent();
            Label_Info.Text = "Karate klub \"" + klub.Naziv + "\"";
        }

        private void Dugme_Potvrdi_Click(object sender, EventArgs e)
        {
            if (ImeTextBox.Text.Equals(string.Empty) || PrezimeTextBox.Text.Equals(string.Empty) || EmailAdresaTextBox.Text.Equals(string.Empty) || BrojTelefonaTextBox.Text.Equals(string.Empty))
            {
                MessageBox.Show("Niste popunili sva polja.");
                return;
            }
            if (DialogResult.OK == MessageBox.Show("Da li su unijeti podaci tačni ? Ako ste sigurni stisnite dugme \"OK\"", "Upozorenje", MessageBoxButtons.OKCancel))
            {
                PredstavnikKluba noviPredstavnik = new PredstavnikKluba(0, ImeTextBox.Text, PrezimeTextBox.Text, new DateTime(1900, 1, 1), (string)PolComboBox.SelectedValue, EmailAdresaTextBox.Text, BrojTelefonaTextBox.Text);

                KlubDAO klubDAO = new KlubDAO();
                if (klubDAO.PromjeniPredstavnika(klub, noviPredstavnik))
                {
                    statusLabel.Text = "Predstavnik karate kluba \"" + klub.Naziv + "\" je uspješno promjenjen.";
                    statusLabel.ForeColor = Color.Green;
                }
                else
                {
                    statusLabel.Text = "Neuspješna promjena predstavnika kluba";
                    statusLabel.ForeColor = Color.Red;
                }
                this.Close();
            }
        }

        private void Dugme_Odustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}