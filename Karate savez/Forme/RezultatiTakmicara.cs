﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class RezultatiTakmicara : Form
    {
        public Takmicar takmicar;
        public RezultatiTakmicara(Takmicar takmicar)
        {
            InitializeComponent();
            this.takmicar = takmicar;
            this.TakmicarLabel.Text = "Takmičar: " + takmicar.Ime + " " + takmicar.Prezime + "\nKlub: " + takmicar.Klub.ToString();
            PopuniTabelu();
        }

        private void PopuniTabelu()
        {
            List<RezultatTakmicara> rezultati = new RezultatDAO().DohvatiRezultateTakmicara(takmicar);
            if (rezultati.Count == 0)
                throw new Exception("Nema rezultata");
            rezultati.ForEach(r => TabelaRezultata.Rows.Add(r.Takmicenje.MjestoOdrzavanja + " " + r.Takmicenje.PocetakTakmicenja.ToShortDateString(), r.Kategorija, r.OsvojenoMjesto));
        }

        private void Dugme_Zatvori_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
