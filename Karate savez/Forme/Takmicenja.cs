﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class Takmicenja : Form
    {
        private Form parentForm;
        public Takmicenja(Form parentForm)
        {
            this.parentForm = parentForm;
            InitializeComponent();
            PopuniTabelu(null);
            TabelaTakmicenja.Rows[0].Selected = true;
        }

        private void Takmicenja_FormClosed(object sender, FormClosedEventArgs e)
        {
            parentForm.Show();
        }

        private void PopuniTabelu(List<Takmicenje> takmicenja)
        {
            TabelaTakmicenja.Rows.Clear();
            TakmicenjeDAO takmicenjeDAO = new TakmicenjeDAO();
            if (takmicenja == null)
                takmicenja = takmicenjeDAO.DohvatiSvaTakmicenja();
            takmicenja.ForEach(t => TabelaTakmicenja.Rows.Add(t.IdTakmicenja, t.PocetakTakmicenja.ToLongDateString(), t.PocetakTakmicenja.ToLongTimeString(), t.MjestoOdrzavanja, t.BrojBorilista));
        }

        private void TakmicenjaCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            Pretraga_TextBox_TextChanged(sender, e);
        }

        private void Dugme_DodajTakmicenje_Click(object sender, EventArgs e)
        {
            new ZakaziTakmicenje().ShowDialog();
            Pretraga_TextBox_TextChanged(sender, e);
        }

        private void TabelaTakmicenja_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (TabelaTakmicenja.SelectedRows.Count == 1)
            {
                if (new TakmicenjeDAO().DohvatiTakmicenjePoIDu((int)TabelaTakmicenja.SelectedRows[0].Cells[0].Value).PocetakTakmicenja < DateTime.Today)
                {
                    Dugme_DelegirajSudije.Enabled = false;
                    Dugme_RezultatiTakmicenja.Enabled = true;

                }
                else
                {
                    Dugme_RezultatiTakmicenja.Enabled = false;
                    Dugme_DelegirajSudije.Enabled = true;
                }
            }
        }

        private void Dugme_DelegirajSudije_Click(object sender, EventArgs e)
        {
            new DelegirajSudije(new TakmicenjeDAO().DohvatiTakmicenjePoIDu((int)TabelaTakmicenja.SelectedRows[0].Cells[0].Value), StatusLabel).ShowDialog();
        }

        private void Dugme_RezultatiTakmicenja_Click(object sender, EventArgs e)
        {
            if (TabelaTakmicenja.SelectedRows.Count > 0)
            {
                Takmicenje takmicenje = new TakmicenjeDAO().DohvatiTakmicenjePoIDu((int)TabelaTakmicenja.SelectedRows[0].Cells[0].Value);
                new RezultatiTakmicenja(takmicenje, StatusLabel).ShowDialog();
            }
        }

        private void Pretraga_TextBox_TextChanged(object sender, EventArgs e)
        {
            string str = Pretraga_TextBox.Text;
            TakmicenjeDAO takmicenjeDAO = new TakmicenjeDAO();
            if (TakmicenjaCheckBox.Checked)
            {
                if (string.Empty.Equals(str))
                    PopuniTabelu(takmicenjeDAO.DohvatiOdrzanaTakmicenja());
                else
                    PopuniTabelu(takmicenjeDAO.DohvatiOdrzanaTakmicenjaPoMjestu(str));
            }
            else {
                if (string.Empty.Equals(str))
                    PopuniTabelu(takmicenjeDAO.DohvatiSvaTakmicenja());
                else
                    PopuniTabelu(takmicenjeDAO.DohvatiTakmicenjaPoMjestu(str));
            }

        }

        private void Dugme_Nazad_Click(object sender, EventArgs e)
        {
            this.parentForm.Show();
            this.Close();
        }
    }
}
