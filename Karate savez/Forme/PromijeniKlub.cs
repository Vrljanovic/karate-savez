﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class PromijeniKlub : Form
    {
        private Takmicar takmicar;
        private ToolStripStatusLabel statusLabel;
        public PromijeniKlub(ToolStripStatusLabel label, Takmicar t)
        {
            statusLabel = label;
            takmicar = t;
            InitializeComponent();
            List<Klub> sviAktivniKlubovi = new KlubDAO().DohvatiSveAktivneKlubove();
            Klub stariKlub = sviAktivniKlubovi.First(k => k.IdKluba == t.Klub.IdKluba);
            StariKlubLabel.Text = "Stari klub: Karate klub \""+ stariKlub.Naziv + "\"";
            ImeTakmicaraLabel.Text = "Takmicar: " + t.Ime + " " + t.Prezime;
            foreach (Klub k in sviAktivniKlubovi)
            {
                if (!k.Equals(takmicar.Klub))
                    KlubComboBox.Items.Add(k);
            }
        }

        private void Dugme_Odustani_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Dugme_Potvrdi_Click(object sender, EventArgs e)
        {
            if(KlubComboBox.SelectedItem == null)
            {
                MessageBox.Show("Odaberite novi klub.");
                return;
            }
            Klub noviKlub = (Klub)KlubComboBox.SelectedItem;
            if(new TakmicarDAO().PromijeniKlub(takmicar, noviKlub))
            {
                statusLabel.Text = "Klub takmičara je uspješno promijenjen";
                statusLabel.ForeColor = Color.Green;
            }
            else
            {
                statusLabel.Text = "Neuspješna promjena kluba takmičara";
                statusLabel.ForeColor = Color.Red;
            }
            this.Close();
        }
    }
}
