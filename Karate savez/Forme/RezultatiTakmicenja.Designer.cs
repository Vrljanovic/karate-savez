﻿namespace Karate_savez.Forme
{
    partial class RezultatiTakmicenja
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treceMjestoComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.drugoMjestoComboBox = new System.Windows.Forms.ComboBox();
            this.prvoMjestoComboBox = new System.Windows.Forms.ComboBox();
            this.kategorijaComboBox = new System.Windows.Forms.ComboBox();
            this.treceMjesto2ComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.Dugme_Potvrdi = new System.Windows.Forms.Button();
            this.Dugme_Odustani = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cetvrtoMjestoKlubBox = new System.Windows.Forms.ComboBox();
            this.treceMjestoKlubBox = new System.Windows.Forms.ComboBox();
            this.drugoMjestoKlubBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.prvoMjestoKlubBox = new System.Windows.Forms.ComboBox();
            this.customInstaller1 = new MySql.Data.MySqlClient.CustomInstaller();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // treceMjestoComboBox
            // 
            this.treceMjestoComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.treceMjestoComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.treceMjestoComboBox.FormattingEnabled = true;
            this.treceMjestoComboBox.Location = new System.Drawing.Point(150, 103);
            this.treceMjestoComboBox.Name = "treceMjestoComboBox";
            this.treceMjestoComboBox.Size = new System.Drawing.Size(402, 24);
            this.treceMjestoComboBox.TabIndex = 5;
            this.treceMjestoComboBox.SelectedIndexChanged += new System.EventHandler(this.treceMjestoComboBox_SelectedIndexChanged);
            this.treceMjestoComboBox.TextChanged += new System.EventHandler(this.treceMjestoComboBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Odaberi kategoriju:";
            // 
            // drugoMjestoComboBox
            // 
            this.drugoMjestoComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.drugoMjestoComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drugoMjestoComboBox.FormattingEnabled = true;
            this.drugoMjestoComboBox.Location = new System.Drawing.Point(150, 72);
            this.drugoMjestoComboBox.Name = "drugoMjestoComboBox";
            this.drugoMjestoComboBox.Size = new System.Drawing.Size(402, 24);
            this.drugoMjestoComboBox.TabIndex = 3;
            this.drugoMjestoComboBox.SelectedIndexChanged += new System.EventHandler(this.drugoMjestoComboBox_SelectedIndexChanged);
            this.drugoMjestoComboBox.TextChanged += new System.EventHandler(this.drugoMjestoComboBox_SelectedIndexChanged);
            // 
            // prvoMjestoComboBox
            // 
            this.prvoMjestoComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.prvoMjestoComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.prvoMjestoComboBox.FormattingEnabled = true;
            this.prvoMjestoComboBox.Location = new System.Drawing.Point(150, 40);
            this.prvoMjestoComboBox.Name = "prvoMjestoComboBox";
            this.prvoMjestoComboBox.Size = new System.Drawing.Size(402, 24);
            this.prvoMjestoComboBox.TabIndex = 1;
            this.prvoMjestoComboBox.SelectedIndexChanged += new System.EventHandler(this.prvoMjestoComboBox_SelectedIndexChanged);
            this.prvoMjestoComboBox.TextChanged += new System.EventHandler(this.prvoMjestoComboBox_SelectedIndexChanged);
            // 
            // kategorijaComboBox
            // 
            this.kategorijaComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.kategorijaComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.kategorijaComboBox.FormattingEnabled = true;
            this.kategorijaComboBox.Location = new System.Drawing.Point(144, 30);
            this.kategorijaComboBox.Name = "kategorijaComboBox";
            this.kategorijaComboBox.Size = new System.Drawing.Size(426, 24);
            this.kategorijaComboBox.TabIndex = 1;
            this.kategorijaComboBox.SelectedIndexChanged += new System.EventHandler(this.kategorijaComboBox_SelectedIndexChanged);
            this.kategorijaComboBox.Enter += new System.EventHandler(this.kategorijaComboBox_Enter);
            // 
            // treceMjesto2ComboBox
            // 
            this.treceMjesto2ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.treceMjesto2ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.treceMjesto2ComboBox.FormattingEnabled = true;
            this.treceMjesto2ComboBox.Location = new System.Drawing.Point(150, 133);
            this.treceMjesto2ComboBox.Name = "treceMjesto2ComboBox";
            this.treceMjesto2ComboBox.Size = new System.Drawing.Size(402, 24);
            this.treceMjesto2ComboBox.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Prvo mjesto";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Drugo mjesto";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Treće mjesto";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.drugoMjestoComboBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.prvoMjestoComboBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.treceMjestoComboBox);
            this.groupBox1.Controls.Add(this.treceMjesto2ComboBox);
            this.groupBox1.Location = new System.Drawing.Point(12, 72);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(558, 191);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Osvajači medalja";
            this.groupBox1.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 133);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Treće mjesto";
            // 
            // Dugme_Potvrdi
            // 
            this.Dugme_Potvrdi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_Potvrdi.Location = new System.Drawing.Point(403, 347);
            this.Dugme_Potvrdi.Name = "Dugme_Potvrdi";
            this.Dugme_Potvrdi.Size = new System.Drawing.Size(194, 44);
            this.Dugme_Potvrdi.TabIndex = 2;
            this.Dugme_Potvrdi.Text = "Potvrdi";
            this.Dugme_Potvrdi.UseVisualStyleBackColor = true;
            this.Dugme_Potvrdi.Click += new System.EventHandler(this.Dugme_Potvrdi_Click);
            // 
            // Dugme_Odustani
            // 
            this.Dugme_Odustani.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_Odustani.Location = new System.Drawing.Point(617, 347);
            this.Dugme_Odustani.Name = "Dugme_Odustani";
            this.Dugme_Odustani.Size = new System.Drawing.Size(195, 44);
            this.Dugme_Odustani.TabIndex = 3;
            this.Dugme_Odustani.Text = "Odustani";
            this.Dugme_Odustani.UseVisualStyleBackColor = true;
            this.Dugme_Odustani.Click += new System.EventHandler(this.Dugme_Odustani_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.kategorijaComboBox);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(585, 285);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Rezultati takmičara";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.cetvrtoMjestoKlubBox);
            this.groupBox3.Controls.Add(this.treceMjestoKlubBox);
            this.groupBox3.Controls.Add(this.drugoMjestoKlubBox);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.prvoMjestoKlubBox);
            this.groupBox3.Location = new System.Drawing.Point(617, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(553, 285);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Rezultati klubova";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 208);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 17);
            this.label9.TabIndex = 6;
            this.label9.Text = "Četvrto mjesto";
            // 
            // cetvrtoMjestoKlubBox
            // 
            this.cetvrtoMjestoKlubBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cetvrtoMjestoKlubBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cetvrtoMjestoKlubBox.FormattingEnabled = true;
            this.cetvrtoMjestoKlubBox.Location = new System.Drawing.Point(122, 205);
            this.cetvrtoMjestoKlubBox.Name = "cetvrtoMjestoKlubBox";
            this.cetvrtoMjestoKlubBox.Size = new System.Drawing.Size(425, 24);
            this.cetvrtoMjestoKlubBox.TabIndex = 7;
            // 
            // treceMjestoKlubBox
            // 
            this.treceMjestoKlubBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.treceMjestoKlubBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.treceMjestoKlubBox.FormattingEnabled = true;
            this.treceMjestoKlubBox.Location = new System.Drawing.Point(122, 175);
            this.treceMjestoKlubBox.Name = "treceMjestoKlubBox";
            this.treceMjestoKlubBox.Size = new System.Drawing.Size(425, 24);
            this.treceMjestoKlubBox.TabIndex = 5;
            this.treceMjestoKlubBox.SelectedIndexChanged += new System.EventHandler(this.treceMjestoKlubBox_SelectedIndexChanged);
            this.treceMjestoKlubBox.TextChanged += new System.EventHandler(this.treceMjestoKlubBox_SelectedIndexChanged);
            // 
            // drugoMjestoKlubBox
            // 
            this.drugoMjestoKlubBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.drugoMjestoKlubBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.drugoMjestoKlubBox.FormattingEnabled = true;
            this.drugoMjestoKlubBox.Location = new System.Drawing.Point(122, 144);
            this.drugoMjestoKlubBox.Name = "drugoMjestoKlubBox";
            this.drugoMjestoKlubBox.Size = new System.Drawing.Size(425, 24);
            this.drugoMjestoKlubBox.TabIndex = 3;
            this.drugoMjestoKlubBox.SelectedIndexChanged += new System.EventHandler(this.drugoMjestoKlubBox_SelectedIndexChanged);
            this.drugoMjestoKlubBox.TextChanged += new System.EventHandler(this.drugoMjestoKlubBox_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 178);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 17);
            this.label6.TabIndex = 4;
            this.label6.Text = "Treće mjesto";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 147);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 17);
            this.label7.TabIndex = 2;
            this.label7.Text = "Drugo mjesto";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 115);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 17);
            this.label8.TabIndex = 0;
            this.label8.Text = "Prvo mjesto";
            // 
            // prvoMjestoKlubBox
            // 
            this.prvoMjestoKlubBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.prvoMjestoKlubBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.prvoMjestoKlubBox.FormattingEnabled = true;
            this.prvoMjestoKlubBox.Location = new System.Drawing.Point(122, 112);
            this.prvoMjestoKlubBox.Name = "prvoMjestoKlubBox";
            this.prvoMjestoKlubBox.Size = new System.Drawing.Size(425, 24);
            this.prvoMjestoKlubBox.TabIndex = 1;
            this.prvoMjestoKlubBox.SelectedIndexChanged += new System.EventHandler(this.prvoMjestoKlubBox_SelectedIndexChanged);
            this.prvoMjestoKlubBox.TextChanged += new System.EventHandler(this.prvoMjestoKlubBox_SelectedIndexChanged);
            // 
            // RezultatiTakmicenja
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1182, 403);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.Dugme_Potvrdi);
            this.Controls.Add(this.Dugme_Odustani);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RezultatiTakmicenja";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Rezultati takmičenja";
            this.Load += new System.EventHandler(this.RezultatiTakmicenja_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox treceMjestoComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox drugoMjestoComboBox;
        private System.Windows.Forms.ComboBox prvoMjestoComboBox;
        private System.Windows.Forms.ComboBox kategorijaComboBox;
        private System.Windows.Forms.ComboBox treceMjesto2ComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Dugme_Potvrdi;
        private System.Windows.Forms.Button Dugme_Odustani;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cetvrtoMjestoKlubBox;
        private System.Windows.Forms.ComboBox treceMjestoKlubBox;
        private System.Windows.Forms.ComboBox drugoMjestoKlubBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox prvoMjestoKlubBox;
        private MySql.Data.MySqlClient.CustomInstaller customInstaller1;
        private System.Windows.Forms.Label label9;
    }
}