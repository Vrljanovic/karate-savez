﻿namespace Karate_savez.Forme
{
    partial class Sudije
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TabelaSudija = new System.Windows.Forms.DataGridView();
            this.BrojSudijskeLicence = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Pol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Aktivan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IMe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Prezime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DatumRodjenja = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Dugme_PromijeniAktivnost = new System.Windows.Forms.Button();
            this.Dugme_DodajSudiju = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.Pretraga_TextBox = new System.Windows.Forms.TextBox();
            this.Dugme_Nazad = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.TabelaSudija)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabelaSudija
            // 
            this.TabelaSudija.AllowUserToAddRows = false;
            this.TabelaSudija.AllowUserToDeleteRows = false;
            this.TabelaSudija.AllowUserToResizeColumns = false;
            this.TabelaSudija.AllowUserToResizeRows = false;
            this.TabelaSudija.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TabelaSudija.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TabelaSudija.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BrojSudijskeLicence,
            this.Pol,
            this.Aktivan,
            this.IMe,
            this.Prezime,
            this.DatumRodjenja});
            this.TabelaSudija.Location = new System.Drawing.Point(0, 87);
            this.TabelaSudija.MultiSelect = false;
            this.TabelaSudija.Name = "TabelaSudija";
            this.TabelaSudija.ReadOnly = true;
            this.TabelaSudija.RowHeadersVisible = false;
            this.TabelaSudija.RowTemplate.Height = 24;
            this.TabelaSudija.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TabelaSudija.Size = new System.Drawing.Size(1140, 340);
            this.TabelaSudija.TabIndex = 2;
            // 
            // BrojSudijskeLicence
            // 
            this.BrojSudijskeLicence.HeaderText = "Broj sudijske licence";
            this.BrojSudijskeLicence.Name = "BrojSudijskeLicence";
            this.BrojSudijskeLicence.ReadOnly = true;
            this.BrojSudijskeLicence.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.BrojSudijskeLicence.Width = 200;
            // 
            // Pol
            // 
            this.Pol.HeaderText = "Pol";
            this.Pol.Name = "Pol";
            this.Pol.ReadOnly = true;
            this.Pol.Width = 50;
            // 
            // Aktivan
            // 
            this.Aktivan.HeaderText = "Aktivan";
            this.Aktivan.Name = "Aktivan";
            this.Aktivan.ReadOnly = true;
            this.Aktivan.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Aktivan.Width = 50;
            // 
            // IMe
            // 
            this.IMe.HeaderText = "Ime";
            this.IMe.Name = "IMe";
            this.IMe.ReadOnly = true;
            this.IMe.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.IMe.Width = 200;
            // 
            // Prezime
            // 
            this.Prezime.HeaderText = "Prezime";
            this.Prezime.Name = "Prezime";
            this.Prezime.ReadOnly = true;
            this.Prezime.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Prezime.Width = 200;
            // 
            // DatumRodjenja
            // 
            this.DatumRodjenja.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DatumRodjenja.HeaderText = "Datum rodjenja";
            this.DatumRodjenja.Name = "DatumRodjenja";
            this.DatumRodjenja.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.Dugme_PromijeniAktivnost);
            this.groupBox1.Controls.Add(this.Dugme_DodajSudiju);
            this.groupBox1.Location = new System.Drawing.Point(12, 433);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(394, 86);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Opcije";
            // 
            // Dugme_PromijeniAktivnost
            // 
            this.Dugme_PromijeniAktivnost.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Dugme_PromijeniAktivnost.BackColor = System.Drawing.Color.Transparent;
            this.Dugme_PromijeniAktivnost.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_PromijeniAktivnost.Location = new System.Drawing.Point(170, 21);
            this.Dugme_PromijeniAktivnost.Name = "Dugme_PromijeniAktivnost";
            this.Dugme_PromijeniAktivnost.Size = new System.Drawing.Size(216, 45);
            this.Dugme_PromijeniAktivnost.TabIndex = 1;
            this.Dugme_PromijeniAktivnost.Text = "Promijeni aktivnost";
            this.Dugme_PromijeniAktivnost.UseVisualStyleBackColor = false;
            this.Dugme_PromijeniAktivnost.Click += new System.EventHandler(this.Dugme_PromijeniAktivnost_Click);
            // 
            // Dugme_DodajSudiju
            // 
            this.Dugme_DodajSudiju.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Dugme_DodajSudiju.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dugme_DodajSudiju.Location = new System.Drawing.Point(6, 21);
            this.Dugme_DodajSudiju.Name = "Dugme_DodajSudiju";
            this.Dugme_DodajSudiju.Size = new System.Drawing.Size(145, 45);
            this.Dugme_DodajSudiju.TabIndex = 0;
            this.Dugme_DodajSudiju.Text = "Dodaj sudiju";
            this.Dugme_DodajSudiju.UseVisualStyleBackColor = true;
            this.Dugme_DodajSudiju.Click += new System.EventHandler(this.Dugme_DodajSudiju_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 531);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1127, 22);
            this.statusStrip1.TabIndex = 4;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pretraži po imenu:";
            // 
            // Pretraga_TextBox
            // 
            this.Pretraga_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pretraga_TextBox.Location = new System.Drawing.Point(195, 22);
            this.Pretraga_TextBox.Name = "Pretraga_TextBox";
            this.Pretraga_TextBox.Size = new System.Drawing.Size(400, 38);
            this.Pretraga_TextBox.TabIndex = 1;
            this.Pretraga_TextBox.TextChanged += new System.EventHandler(this.Pretraga_TextBox_TextChanged);
            // 
            // Dugme_Nazad
            // 
            this.Dugme_Nazad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Dugme_Nazad.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Dugme_Nazad.Location = new System.Drawing.Point(975, 26);
            this.Dugme_Nazad.Name = "Dugme_Nazad";
            this.Dugme_Nazad.Size = new System.Drawing.Size(140, 38);
            this.Dugme_Nazad.TabIndex = 5;
            this.Dugme_Nazad.Text = "Nazad";
            this.Dugme_Nazad.UseVisualStyleBackColor = true;
            this.Dugme_Nazad.Click += new System.EventHandler(this.Dugme_Nazad_Click);
            // 
            // Sudije
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1127, 553);
            this.Controls.Add(this.Dugme_Nazad);
            this.Controls.Add(this.Pretraga_TextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.TabelaSudija);
            this.MinimumSize = new System.Drawing.Size(1145, 600);
            this.Name = "Sudije";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sudije";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Sudije_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.TabelaSudija)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView TabelaSudija;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button Dugme_PromijeniAktivnost;
        private System.Windows.Forms.Button Dugme_DodajSudiju;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Pretraga_TextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn BrojSudijskeLicence;
        private System.Windows.Forms.DataGridViewTextBoxColumn Pol;
        private System.Windows.Forms.DataGridViewTextBoxColumn Aktivan;
        private System.Windows.Forms.DataGridViewTextBoxColumn IMe;
        private System.Windows.Forms.DataGridViewTextBoxColumn Prezime;
        private System.Windows.Forms.DataGridViewTextBoxColumn DatumRodjenja;
        private System.Windows.Forms.Button Dugme_Nazad;
    }
}