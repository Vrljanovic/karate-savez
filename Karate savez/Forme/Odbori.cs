﻿using Karate_savez.DAO;
using Karate_savez.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Karate_savez.Forme
{
    public partial class Odbori : Form
    {
        private Form parentForm;
        public Odbori(Form parentForm)
        {
            this.parentForm = parentForm;
            InitializeComponent();
            PopuniTabeluNadzornogOdbora(null);
            PopuniTabeluUpravnogOdbora(null);
        }

        private void Odbori_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.parentForm.Show();
            this.Hide();
        }

        private void PopuniTabeluNadzornogOdbora(List<ClanNadzornogOdbora> clanovi)
        {
            TabelaNadzorniOdbor.Rows.Clear();
            if(clanovi == null)
                clanovi = new ClanOdboraDAO().DohvatiClanoveNadzornogOdbora();
            clanovi.ForEach(c => TabelaNadzorniOdbor.Rows.Add(c.IdOsobe, c.Ime + " " + c.Prezime, c.ClanOd.ToShortDateString() + " - " + c.ClanDo.Date.ToShortDateString()));
        }

        private void PopuniTabeluUpravnogOdbora(List<ClanUpravnogOdbora> clanovi)
        {
            TabelaUpravniOdbor.Rows.Clear();
            if(clanovi == null)
                clanovi = new ClanOdboraDAO().DohvatiClanoveUpravnogOdbora();
            clanovi.ForEach(c => TabelaUpravniOdbor.Rows.Add(c.IdOsobe, c.Ime + " " + c.Prezime, c.ClanOd.Date.ToShortDateString() + " - " + c.ClanDo.Date.ToShortDateString()));
        }

        private void Dugme_DodajClanaUO_Click(object sender, EventArgs e)
        {
            new DodajClanaOdbora(StatusLabel, "upravni").ShowDialog();
            PopuniTabeluUpravnogOdbora(null);
        }

        private void Dugme_DodajClanaNO_Click(object sender, EventArgs e)
        {
            new DodajClanaOdbora(StatusLabel, "nadzorni").ShowDialog();
            PopuniTabeluNadzornogOdbora(null);
        }

        private void PretraziUpravni_TextBox_TextChanged(object sender, EventArgs e)
        {
            string str = PretraziUpravni_TextBox.Text;
            if (string.Empty.Equals(str))
                PopuniTabeluUpravnogOdbora(null);
            else
                PopuniTabeluUpravnogOdbora(new ClanOdboraDAO().DohvatiClanaUpravnogOdboraPoImenu(str));
        }

        private void PretraziNadzorni_TextBox_TextChanged(object sender, EventArgs e)
        {
            string str = PretraziNadzorni_TextBox.Text;
            if (string.Empty.Equals(str))
                PopuniTabeluNadzornogOdbora(null);
            else
                PopuniTabeluNadzornogOdbora(new ClanOdboraDAO().DohvataClanaNadzornogOdboraPoImenu(str));

        }

        private void Dugme_Nazad_Click(object sender, EventArgs e)
        {
            this.parentForm.Show();
            this.Close();
        }
    }
}
