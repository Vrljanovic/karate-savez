﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using Karate_savez.DTO;
using Karate_savez.DAO;
using Karate_savez.Forme;

namespace Karate_savez
{
    
    public partial class KarateSavez : Form
    { 
        public KarateSavez()
        { 
            InitializeComponent();
        }

        private void Dugme_Klubovi_Click(object sender, EventArgs e)
        {
            Hide();
            new Klubovi(this).Show();
        }

        private void KarateSavez_FormClosed(object sender, FormClosedEventArgs e)
        {
            Database.DatabaseConnection.GetConnection().CloseConnection();
            Environment.Exit(0);
        }

        private void Dugme_Sudije_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Sudije(this).Show();
        }

        private void Dugme_Takmicari_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Takmicari(this).Show();
        }

        private void Dugme_Takmicenja_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Takmicenja(this).Show();
        }

        private void Dugme_Odbori_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Odbori(this).Show();
        }

        private void Dugme_Sastanci_Click(object sender, EventArgs e)
        {
            this.Hide();
            new Sastanci(this).Show();
        }

        private void Dugme_Zatvori_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
    }
}