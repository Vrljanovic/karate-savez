﻿using Karate_savez.Database;
using Karate_savez.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DAO
{
    class PredstavnikKlubaDAO
    {
        public bool DodajPredstavnika(PredstavnikKluba predstavnik)
        {
            OsobaDAO osobaDAO = new OsobaDAO();
            MySqlCommand iskazDodajPredstavnika = DatabaseConnection.GetConnection().CreateCommand();

            MySqlParameter brojTelefonaParametar = new MySqlParameter("@BrojTelefona", MySqlDbType.VarChar)
            {
                Value = predstavnik.BrojTelefona
            };
            MySqlParameter emailParametar = new MySqlParameter("@EmailAdresa", MySqlDbType.VarChar)
            {
                Value = predstavnik.EmailAdresa
            };

            iskazDodajPredstavnika.CommandText = "insert into predstavnik (idosobe, brojtelefona, email) values (@IdOsobe, @BrojTelefona, @EmailAdresa)";
            iskazDodajPredstavnika.Parameters.AddRange(new object[] { brojTelefonaParametar, emailParametar });
            if (osobaDAO.OsobaPostojiUBazi(predstavnik))
            {
                try
                {
                    MySqlParameter idOsobeParametar = new MySqlParameter("@IdOsobe", MySqlDbType.Int32)
                    {
                        Value = predstavnik.IdOsobe
                    };
                    iskazDodajPredstavnika.Parameters.Add(idOsobeParametar);
                    iskazDodajPredstavnika.Prepare();
                    iskazDodajPredstavnika.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return false;
                }
            }
            else
            {
                if (osobaDAO.DodajOsobu(predstavnik))
                {
                    MySqlParameter idOsobeParametar = new MySqlParameter("@IdOsobe", MySqlDbType.Int32)
                    {
                        Value = predstavnik.IdOsobe
                    };
                    iskazDodajPredstavnika.Parameters.Add(idOsobeParametar);
                    iskazDodajPredstavnika.Prepare();
                    try
                    {
                        iskazDodajPredstavnika.ExecuteNonQuery();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        if(!osobaDAO.UkloniOsobu(predstavnik)){
                            Console.WriteLine("Unexpected Error");
                            Environment.Exit(-1);
                        }
                        Console.WriteLine(ex.Message);
                        return false;
                    }
                }
                else return false;
            }
        }

        public bool JePredstavnik(PredstavnikKluba p)
        {
            MySqlCommand upitZaProvjeru = DatabaseConnection.GetConnection().CreateCommand();
            upitZaProvjeru.CommandText = "select * from predstavnik where brojtelefona = @Broj or email = @Email";
            MySqlParameter brojParametar = new MySqlParameter("@Broj", MySqlDbType.VarChar)
            {
                Value = p.BrojTelefona
            };
            MySqlParameter emailParametar = new MySqlParameter("@Email", MySqlDbType.VarChar)
            {
                Value = p.EmailAdresa
            };
            upitZaProvjeru.Parameters.AddRange(new object[] { brojParametar, emailParametar });
            upitZaProvjeru.Prepare();
            MySqlDataReader rezultatUpita = upitZaProvjeru.ExecuteReader();
            bool jePredstavnik = rezultatUpita.HasRows;
            if (jePredstavnik)
            {
                rezultatUpita.Read();
                p.IdOsobe = rezultatUpita.GetInt32("IdOsobe");
            }
            rezultatUpita.Close();
            return jePredstavnik;
        }

        public bool UkloniPredstavnika(PredstavnikKluba p)
        {
            MySqlCommand iskazZaUklanjanje = DatabaseConnection.GetConnection().CreateCommand();
            iskazZaUklanjanje.CommandText = "delete from predstavnik where idosobe = @ID or email = @Email and brojtelefona = @BrojTelefona";
            MySqlParameter idParametar = new MySqlParameter("@ID", MySqlDbType.Int32)
            {
                Value = p.IdOsobe
            };
            MySqlParameter emailParametar = new MySqlParameter("@Email", MySqlDbType.VarChar)
            {
                Value = p.EmailAdresa
            };
            MySqlParameter brojTelefonaParametar = new MySqlParameter("@BrojTelefona", MySqlDbType.VarChar)
            {
                Value = p.BrojTelefona
            };
            iskazZaUklanjanje.Parameters.AddRange(new object[] { idParametar, emailParametar, brojTelefonaParametar });
            iskazZaUklanjanje.Prepare();
            try
            {
                int brojRedova = iskazZaUklanjanje.ExecuteNonQuery();
                if (new OsobaDAO().UkloniOsobu(p))
                    return brojRedova == 1;
                else
                    return false;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public PredstavnikKluba DohvatiPredstavnikaPoIDu(int id)
        {
            PredstavnikKluba predstavnik = null;

            MySqlCommand upitZaPredstavnika = DatabaseConnection.GetConnection().CreateCommand();
            upitZaPredstavnika.CommandText = "select * from predstavnik where idosobe = @ID";
            MySqlParameter idParametar = new MySqlParameter("@ID", MySqlDbType.Int32)
            {
                Value = id
            };
            upitZaPredstavnika.Parameters.Add(idParametar);
            upitZaPredstavnika.Prepare();
            MySqlDataReader rezultatUpita = upitZaPredstavnika.ExecuteReader();
            if (rezultatUpita.Read())
            {
                int idOsobe = id;
                string email = rezultatUpita.GetString("Email");
                string brojTelefona = rezultatUpita.GetString("BrojTelefona");
                rezultatUpita.Close();
                Osoba osoba = new OsobaDAO().DohvatiOsobuPoIDu(idOsobe);
                predstavnik = new PredstavnikKluba(osoba.IdOsobe, osoba.Ime, osoba.Prezime, osoba.DatumRodjenja, osoba.Pol, email, brojTelefona);
            }
            else
                rezultatUpita.Close();

            return predstavnik;
        }
    }
}