﻿using Karate_savez.Database;
using Karate_savez.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DAO
{
    class KlubDAO
    {
        public List<Klub> DohvatiKlubove()
        {
            List<Klub> sviKlubovi = new List<Klub>();

            MySqlCommand selektujSve = DatabaseConnection.GetConnection().CreateCommand();
            selektujSve.CommandText = "select * from sviklubovi";
            MySqlDataReader rezultatiUpita = selektujSve.ExecuteReader();
            while (rezultatiUpita.Read())
            {
                Mjesto mjesto = new Mjesto(rezultatiUpita.GetInt32("IdMjesta"), rezultatiUpita.GetString("Naziv"), rezultatiUpita.GetString("Adresa"));
                PredstavnikKluba predstavnik = new PredstavnikKluba(rezultatiUpita.GetInt32("IdOsobe"), rezultatiUpita.GetString("Ime"), rezultatiUpita.GetString("Prezime"), rezultatiUpita.GetDateTime("DatumRodjenja"), rezultatiUpita.GetString("Pol"), rezultatiUpita.GetString("Email"), rezultatiUpita.GetString("BrojTelefona"));
                Klub klub = new Klub(rezultatiUpita.GetInt32("IdKluba"), rezultatiUpita.GetString("NazivKluba"), rezultatiUpita.GetString("JIB"), mjesto, predstavnik, rezultatiUpita.GetBoolean("JeAktivan"));
                sviKlubovi.Add(klub);
            }
            rezultatiUpita.Close();

            return sviKlubovi;
        }

        public List<Klub> DohvatiKlubovePoImenu(string Ime)
        {
            List<Klub> klubovi = new List<Klub>();
            MySqlCommand selektujPoImenu = DatabaseConnection.GetConnection().CreateCommand();
            selektujPoImenu.CommandText = "select * from sviklubovi where nazivkluba = @Naziv";
            MySqlParameter parametarNaziv = new MySqlParameter("@Naziv", MySqlDbType.VarChar)
            {
                Value = Ime
            };

            selektujPoImenu.Parameters.Add(parametarNaziv);
            selektujPoImenu.Prepare();
            MySqlDataReader rezultatUpita = selektujPoImenu.ExecuteReader();
            while (rezultatUpita.Read())
            {
                Mjesto mjesto = new Mjesto(rezultatUpita.GetInt32("IdMjesta"), rezultatUpita.GetString("Naziv"), rezultatUpita.GetString("Adresa"));
                PredstavnikKluba predstavnik = new PredstavnikKluba(rezultatUpita.GetInt32("IdOsobe"), rezultatUpita.GetString("Ime"), rezultatUpita.GetString("Prezime"), rezultatUpita.GetDateTime("DatumRodjenja"), rezultatUpita.GetString("Pol"), rezultatUpita.GetString("Email"), rezultatUpita.GetString("BrojTelefona"));
                Klub klub = new Klub(rezultatUpita.GetInt32("IdKluba"), rezultatUpita.GetString("NazivKluba"), rezultatUpita.GetString("JIB"), mjesto, predstavnik);
                klubovi.Add(klub);
            }
            rezultatUpita.Close();
            return klubovi;
        }

        public Klub DohvatiKlubPoJIBu(string JIB)
        {
            Klub klub = null;

            MySqlCommand selektujPoJIBu = DatabaseConnection.GetConnection().CreateCommand();
            selektujPoJIBu.CommandText = "select * from sviklubovi where jib= @JIB";
            MySqlParameter parametarNaziv = new MySqlParameter("@JIB", MySqlDbType.VarChar)
            {
                Value = JIB
            };
            selektujPoJIBu.Parameters.Add(parametarNaziv);
            selektujPoJIBu.Prepare();
            MySqlDataReader rezultatUpita = selektujPoJIBu.ExecuteReader();
            if (rezultatUpita.Read())
            {
                Mjesto mjesto = new Mjesto(rezultatUpita.GetInt32("IdMjesta"), rezultatUpita.GetString("Naziv"), rezultatUpita.GetString("Adresa"));
                PredstavnikKluba predstavnik = new PredstavnikKluba(rezultatUpita.GetInt32("IdOsobe"), rezultatUpita.GetString("Ime"), rezultatUpita.GetString("Prezime"), rezultatUpita.GetDateTime("DatumRodjenja"), rezultatUpita.GetString("Pol"), rezultatUpita.GetString("Email"), rezultatUpita.GetString("BrojTelefona"));
                klub = new Klub(rezultatUpita.GetInt32("IdKluba"), rezultatUpita.GetString("NazivKluba"), rezultatUpita.GetString("JIB"), mjesto, predstavnik, rezultatUpita.GetBoolean("JeAktivan"));
            }
            rezultatUpita.Close();
            return klub;
        }

        public List<Klub> DohvatiKluboveIzMjesta(string NazivMjesta)
        {
            List<Klub> klubovi = new List<Klub>();
            MySqlCommand selektujPoImenu = DatabaseConnection.GetConnection().CreateCommand();
            selektujPoImenu.CommandText = "select * from svi klubovi where naziv = @NazivMjesta";
            MySqlParameter parametarNazivMjesta = new MySqlParameter("@NazivMjesta", MySqlDbType.VarChar)
            {
                Value = NazivMjesta
            };

            selektujPoImenu.Parameters.Add(parametarNazivMjesta);
            selektujPoImenu.Prepare();
            MySqlDataReader rezultatUpita = selektujPoImenu.ExecuteReader();
            while (rezultatUpita.Read())
            {
                Mjesto mjesto = new Mjesto(rezultatUpita.GetInt32("IdMjesta"), rezultatUpita.GetString("Naziv"), rezultatUpita.GetString("Adresa"));
                PredstavnikKluba predstavnik = new PredstavnikKluba(rezultatUpita.GetInt32("IdOsobe"), rezultatUpita.GetString("Ime"), rezultatUpita.GetString("Prezime"), rezultatUpita.GetDateTime("DatumRodjenja"), rezultatUpita.GetString("Pol"), rezultatUpita.GetString("EmailAdresa"), rezultatUpita.GetString("BrojTelefona"));
                Klub klub = new Klub(rezultatUpita.GetInt32("IdKluba"), rezultatUpita.GetString("NazivKluba"), rezultatUpita.GetString("JIB"), mjesto, predstavnik, rezultatUpita.GetBoolean("JeAktivan"));
                klubovi.Add(klub);
            }
            rezultatUpita.Close();
            return klubovi;
        }

        public bool KlubPostoji(Klub k)
        {
            MySqlCommand upitZaKlub = DatabaseConnection.GetConnection().CreateCommand();
            upitZaKlub.CommandText = "select * from klub where jib = @JIB";
            MySqlParameter idParametar = new MySqlParameter("@JIB", MySqlDbType.VarChar)
            {
                Value = k.JIB
            };
            upitZaKlub.Parameters.Add(idParametar);
            upitZaKlub.Prepare();
            MySqlDataReader rezultatUpita = upitZaKlub.ExecuteReader();
            bool postoji = rezultatUpita.HasRows;
            rezultatUpita.Read();
            if (postoji)
                k.IdKluba = rezultatUpita.GetInt32("IdKluba");
            rezultatUpita.Close();
            return postoji;
        }

        public bool DodajKlub(Klub k)
        {
            if (KlubPostoji(k))
                return false;
            PredstavnikKlubaDAO predstavnikKlubaDAO = new PredstavnikKlubaDAO();
            if (predstavnikKlubaDAO.JePredstavnik(k.PredstavnikKluba))
                return false;
            predstavnikKlubaDAO.DodajPredstavnika(k.PredstavnikKluba);
            MjestoDAO mjestoDAO = new MjestoDAO();
            if (!mjestoDAO.DodajMjesto(k.Sjediste))
                return false;
            MySqlCommand iskazZaDodavanje = DatabaseConnection.GetConnection().CreateCommand();
            iskazZaDodavanje.CommandText = "insert into klub (nazivkluba, jib, idmjesta, idpredstavnika)values(@NazivKluba, @JIB, @IdMjesta, @IdPredstavnika)";
            MySqlParameter nazivKlubaParametar = new MySqlParameter("@NazivKluba", MySqlDbType.VarChar)
            {
                Value = k.Naziv
            };
            MySqlParameter jibParameter = new MySqlParameter("@JIB", MySqlDbType.VarChar)
            {
                Value = k.JIB
            };
            MySqlParameter idMjestaParametar = new MySqlParameter("@IdMjesta", MySqlDbType.Int32)
            {
                Value = k.Sjediste.IdMjesta
            };
            MySqlParameter idPredstavnikaParametar = new MySqlParameter("@IdPredstavnika", MySqlDbType.Int32)
            {
                Value = k.PredstavnikKluba.IdOsobe
            };
            iskazZaDodavanje.Parameters.AddRange(new object[] { nazivKlubaParametar, jibParameter, idMjestaParametar, idPredstavnikaParametar });
            iskazZaDodavanje.Prepare();
            try
            {
                iskazZaDodavanje.ExecuteNonQuery();
                k.IdKluba = (int)iskazZaDodavanje.LastInsertedId;
                return true;
            }
            catch (Exception ex)
            {
                if (predstavnikKlubaDAO.UkloniPredstavnika(k.PredstavnikKluba))
                {
                    OsobaDAO osobaDAO = new OsobaDAO();
                    if (!osobaDAO.UkloniOsobu(k.PredstavnikKluba))
                    {
                        Console.WriteLine("Unexpected Error");
                        Environment.Exit(-1);
                    }
                }
                else
                {
                    Console.WriteLine("Unexpected Error");
                    Environment.Exit(-1);
                }
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool PromjeniAktivnost(Klub k)
        {
            k.JeAktivan = !k.JeAktivan;
            MySqlCommand iskazAzuriraj = DatabaseConnection.GetConnection().CreateCommand();
            iskazAzuriraj.CommandText = "update klub set jeaktivan = @JeAktivan where idkluba = @ID";
            MySqlParameter idParametar = new MySqlParameter("@ID", MySqlDbType.Int32)
            {
                Value = k.IdKluba
            };
            MySqlParameter jeAktivanParametar = new MySqlParameter("@JeAktivan", MySqlDbType.Int16)
            {
                Value = k.JeAktivan ? 1 : 0
            };
            iskazAzuriraj.Parameters.AddRange(new object[] { idParametar, jeAktivanParametar });
            iskazAzuriraj.Prepare();
            try
            {
                iskazAzuriraj.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                k.JeAktivan = !k.JeAktivan;
                return false;
            }
        }

        public bool IzmjeniPodatke(Klub k)
        {
            MySqlCommand iskazPromjeniPodatke = DatabaseConnection.GetConnection().CreateCommand();
            MjestoDAO mjestoDAO = new MjestoDAO();

            if (!mjestoDAO.MjestoPostoji(k.Sjediste))
            {
                if (!mjestoDAO.DodajMjesto(k.Sjediste))
                    return false;
            }
            iskazPromjeniPodatke.CommandText = "update klub set jib = @JIB, nazivkluba = @Naziv, idmjesta = @IdMjesta where idkluba = @ID";
            MySqlParameter jibparametar = new MySqlParameter("@JIB", MySqlDbType.VarChar)
            {
                Value = k.JIB
            };
            MySqlParameter nazivParametar = new MySqlParameter("@Naziv", MySqlDbType.VarChar)
            {
                Value = k.Naziv
            };
            MySqlParameter idMjestaParametar = new MySqlParameter("@IdMjesta", MySqlDbType.Int32)
            {
                Value = k.Sjediste.IdMjesta
            };
            MySqlParameter idParametar = new MySqlParameter("@ID", MySqlDbType.Int32)
            {
                Value = k.IdKluba
            };
            iskazPromjeniPodatke.Parameters.AddRange(new object[] { idParametar, idMjestaParametar, nazivParametar, jibparametar });
            iskazPromjeniPodatke.Prepare();

            try
            {
                iskazPromjeniPodatke.ExecuteNonQuery();
                return true;
            }
            catch(MySqlException mysqlex)
            {
                Console.WriteLine(mysqlex.Message);
                return false;
            }
        }

        public bool PromjeniPredstavnika(Klub k, PredstavnikKluba noviPredstavnik)
        {
            PredstavnikKlubaDAO predstavnikKlubaDAO = new PredstavnikKlubaDAO();
            PredstavnikKluba stariPredstavnik = null;
            if (predstavnikKlubaDAO.JePredstavnik(noviPredstavnik))
                return false;
            if (predstavnikKlubaDAO.DodajPredstavnika(noviPredstavnik))
            {
                stariPredstavnik = k.PredstavnikKluba;
                k.PredstavnikKluba = noviPredstavnik;
            }
            MySqlCommand iskazPromjenaPredstavnika = DatabaseConnection.GetConnection().CreateCommand();
            iskazPromjenaPredstavnika.CommandText = "update klub set idpredstavnika = @IdPredstavnika where idkluba = @IdKluba";
            MySqlParameter idPredstavnikaParametar = new MySqlParameter("@IdPredstavnika", MySqlDbType.Int32)
            {
                Value = k.PredstavnikKluba.IdOsobe
            };
            MySqlParameter idKlubaPrametar = new MySqlParameter("@IdKluba", MySqlDbType.Int32)
            {
                Value = k.IdKluba
            };
            iskazPromjenaPredstavnika.Parameters.AddRange(new object[] { idPredstavnikaParametar, idKlubaPrametar });
            iskazPromjenaPredstavnika.Prepare();

            try
            {
                iskazPromjenaPredstavnika.ExecuteNonQuery();
            }
            catch(MySqlException mysqlex)
            {
                Console.WriteLine(mysqlex.Message);
                return false;
            }

            if (predstavnikKlubaDAO.UkloniPredstavnika(stariPredstavnik))
                return true;
            return false;
        }

        public Klub DohvatiKlubPoIDu(int id)
        {
            Klub klub = null;

            MySqlCommand upitZaKlub = DatabaseConnection.GetConnection().CreateCommand(); ;
            upitZaKlub.CommandText = "select * from sviklubovi where idkluba = @ID";
            MySqlParameter idParametar = new MySqlParameter("@ID", MySqlDbType.Int32)
            {
                Value = id
            };
            upitZaKlub.Parameters.Add(idParametar);
            upitZaKlub.Prepare();
            MySqlDataReader rezultatUpita = upitZaKlub.ExecuteReader();
            if (rezultatUpita.Read())
            {
                Mjesto mjesto = new Mjesto(rezultatUpita.GetInt32("IdMjesta"), rezultatUpita.GetString("Naziv"), rezultatUpita.GetString("Adresa"));
                PredstavnikKluba predstavnik = new PredstavnikKluba(rezultatUpita.GetInt32("IdOsobe"), rezultatUpita.GetString("Ime"), rezultatUpita.GetString("Prezime"), rezultatUpita.GetDateTime("DatumRodjenja"), rezultatUpita.GetString("Pol"), rezultatUpita.GetString("Email"), rezultatUpita.GetString("BrojTelefona"));
                klub = new Klub(rezultatUpita.GetInt32("IdKluba"), rezultatUpita.GetString("NazivKluba"), rezultatUpita.GetString("JIB"), mjesto, predstavnik, rezultatUpita.GetBoolean("JeAktivan"));
            }
            rezultatUpita.Close();
            return klub;
        }

        public List<Klub> DohvatiSveAktivneKlubove()
        {
            List<Klub> sviAktivniKlubovi = new List<Klub>();

            MySqlCommand upitZaSveAktivne = DatabaseConnection.GetConnection().CreateCommand();
            upitZaSveAktivne.CommandText = "select * from sviklubovi where jeaktivan >= 1";
            MySqlDataReader rezultatUpita = upitZaSveAktivne.ExecuteReader();
            while (rezultatUpita.Read())
            {
                Mjesto mjesto = new Mjesto(rezultatUpita.GetInt32("IdMjesta"), rezultatUpita.GetString("Naziv"), rezultatUpita.GetString("Adresa"));
                PredstavnikKluba predstavnik = new PredstavnikKluba(rezultatUpita.GetInt32("IdOsobe"), rezultatUpita.GetString("Ime"), rezultatUpita.GetString("Prezime"), rezultatUpita.GetDateTime("DatumRodjenja"), rezultatUpita.GetString("Pol"), rezultatUpita.GetString("Email"), rezultatUpita.GetString("BrojTelefona"));
                Klub klub = new Klub(rezultatUpita.GetInt32("IdKluba"), rezultatUpita.GetString("NazivKluba"), rezultatUpita.GetString("JIB"), mjesto, predstavnik, rezultatUpita.GetBoolean("JeAktivan"));
                sviAktivniKlubovi.Add(klub);
            }
            rezultatUpita.Close();
            return sviAktivniKlubovi;
        }

        public List<Klub> DohvatiKluboveCijiNazivSadrzi(string str)
        {
            List<Klub> klubovi = new List<Klub>();
            MySqlCommand upitZaKlubove = DatabaseConnection.GetConnection().CreateCommand();

            upitZaKlubove.CommandText = "select * from sviklubovi where nazivkluba like '%" + str + "%'";

            MySqlDataReader rezultatiUpita = upitZaKlubove.ExecuteReader();
            while (rezultatiUpita.Read())
            {
                Mjesto mjesto = new Mjesto(rezultatiUpita.GetInt32("IdMjesta"), rezultatiUpita.GetString("Naziv"), rezultatiUpita.GetString("Adresa"));
                PredstavnikKluba predstavnik = new PredstavnikKluba(rezultatiUpita.GetInt32("IdOsobe"), rezultatiUpita.GetString("Ime"), rezultatiUpita.GetString("Prezime"), rezultatiUpita.GetDateTime("DatumRodjenja"), rezultatiUpita.GetString("Pol"), rezultatiUpita.GetString("Email"), rezultatiUpita.GetString("BrojTelefona"));
                Klub klub = new Klub(rezultatiUpita.GetInt32("IdKluba"), rezultatiUpita.GetString("NazivKluba"), rezultatiUpita.GetString("JIB"), mjesto, predstavnik, rezultatiUpita.GetBoolean("JeAktivan"));
                klubovi.Add(klub);
            }
            rezultatiUpita.Close();
            return klubovi;
        }

        public List<Klub> DohvatiKluboveIzGrada(string grad)
        {
            List<Klub> klubovi = new List<Klub>();
            MySqlCommand upitZaKluboveIzGrada = DatabaseConnection.GetConnection().CreateCommand();
            upitZaKluboveIzGrada.CommandText = "select * from sviklubovi where naziv like '%" + grad + "%'";

            MySqlDataReader rezultatiUpita = upitZaKluboveIzGrada.ExecuteReader();
            while (rezultatiUpita.Read())
            {
                Mjesto mjesto = new Mjesto(rezultatiUpita.GetInt32("IdMjesta"), rezultatiUpita.GetString("Naziv"), rezultatiUpita.GetString("Adresa"));
                PredstavnikKluba predstavnik = new PredstavnikKluba(rezultatiUpita.GetInt32("IdOsobe"), rezultatiUpita.GetString("Ime"), rezultatiUpita.GetString("Prezime"), rezultatiUpita.GetDateTime("DatumRodjenja"), rezultatiUpita.GetString("Pol"), rezultatiUpita.GetString("Email"), rezultatiUpita.GetString("BrojTelefona"));
                Klub klub = new Klub(rezultatiUpita.GetInt32("IdKluba"), rezultatiUpita.GetString("NazivKluba"), rezultatiUpita.GetString("JIB"), mjesto, predstavnik, rezultatiUpita.GetBoolean("JeAktivan"));
                klubovi.Add(klub);
            }
            rezultatiUpita.Close();
            return klubovi;
        }
    }
}