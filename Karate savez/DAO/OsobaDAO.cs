﻿using Karate_savez.Database;
using Karate_savez.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DAO
{
    class OsobaDAO
    {
        public bool DodajOsobu(Osoba o)
        {
            MySqlCommand iskazDodajOsobu = DatabaseConnection.GetConnection().CreateCommand();
            iskazDodajOsobu.CommandText = "insert into osoba (ime, prezime, datumrodjenja, pol) values (@Ime, @Prezime, @DatumRodjenja, @Pol)";
            MySqlParameter imeParametar = new MySqlParameter("@Ime", MySqlDbType.VarChar)
            {
                Value = o.Ime
            };
            MySqlParameter prezimeParametar = new MySqlParameter("@Prezime", MySqlDbType.VarChar)
            {
                Value = o.Prezime
            };
            MySqlParameter datumRodjenjaParametar = new MySqlParameter("@DatumRodjenja", MySqlDbType.Date)
            {
                Value = o.DatumRodjenja
            };
            MySqlParameter polParametar = new MySqlParameter("@Pol", MySqlDbType.VarChar)
            {
                Value = o.Pol
            };

            iskazDodajOsobu.Parameters.AddRange(new object[] { imeParametar, prezimeParametar, datumRodjenjaParametar, polParametar });
            iskazDodajOsobu.Prepare();
            try
            {
                iskazDodajOsobu.ExecuteNonQuery();
                o.IdOsobe = (int)iskazDodajOsobu.LastInsertedId;
                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public List<Osoba> TraziPoImenu(string Ime, string Prezime)
        {
            List<Osoba> osobe = new List<Osoba>();

            MySqlCommand upitNadjiSvePoImenu = DatabaseConnection.GetConnection().CreateCommand();
            upitNadjiSvePoImenu.CommandText = "select * from osoba where Ime = @Ime and Prezime = @Prezime";
            MySqlParameter imeParametar = new MySqlParameter("@Ime", MySqlDbType.VarChar)
            {
                Value = Ime
            };
            MySqlParameter prezimeParametar = new MySqlParameter("@Prezime", MySqlDbType.VarChar)
            {
                Value = Prezime
            };
            upitNadjiSvePoImenu.Parameters.AddRange(new object[] { imeParametar, prezimeParametar });
            upitNadjiSvePoImenu.Prepare();
            MySqlDataReader rezultatUpita = upitNadjiSvePoImenu.ExecuteReader();

            while (rezultatUpita.Read())
            {
                Osoba o = new Osoba(rezultatUpita.GetInt32("IdOsobe"), rezultatUpita.GetString("Ime"), rezultatUpita.GetString("Prezime"), rezultatUpita.GetDateTime("DatumRodjenja"), rezultatUpita.GetString("Pol"));
                osobe.Add(o);
            }
            rezultatUpita.Close();
            return osobe;
        }

        public bool OsobaPostojiUBazi(Osoba o)
        {
            MySqlCommand upitZaOsobu = DatabaseConnection.GetConnection().CreateCommand();
            upitZaOsobu.CommandText = "select * from osoba where idosobe = @id";
            MySqlParameter idParametar = new MySqlParameter("@id", MySqlDbType.Int32)
            {
                Value = o.IdOsobe
            };
            upitZaOsobu.Parameters.Add(idParametar);
            upitZaOsobu.Prepare();

            MySqlDataReader rezultatUpita = upitZaOsobu.ExecuteReader();

            bool postoji = rezultatUpita.HasRows;
            rezultatUpita.Close();
            return postoji;
        }

        public bool UkloniOsobu(Osoba o)
        {
            MySqlCommand iskazZaUklanjanje = DatabaseConnection.GetConnection().CreateCommand();
            iskazZaUklanjanje.CommandText = "delete from osoba where idosobe = @ID";
            MySqlParameter idParametar = new MySqlParameter("@ID", MySqlDbType.Int32)
            {
                Value = o.IdOsobe
            };
            iskazZaUklanjanje.Parameters.Add(idParametar);
            iskazZaUklanjanje.Prepare();
            try
            {
                int brojRedova = iskazZaUklanjanje.ExecuteNonQuery();
                return brojRedova == 1;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public Osoba DohvatiOsobuPoIDu(int id)
        {
            Osoba osoba = null;

            MySqlCommand upitZaOsobu = DatabaseConnection.GetConnection().CreateCommand();
            upitZaOsobu.CommandText = "select * from osoba where idosoba = @ID";
            MySqlParameter idParametar = new MySqlParameter("@ID", MySqlDbType.Int32)
            {
                Value = id
            };
            upitZaOsobu.Parameters.Add(idParametar);
            upitZaOsobu.Prepare();
            MySqlDataReader rezultatUpita = upitZaOsobu.ExecuteReader();
            if (rezultatUpita.Read())
                osoba = new Osoba(rezultatUpita.GetInt32("IdOsobe"), rezultatUpita.GetString("Ime"), rezultatUpita.GetString("Prezime"), rezultatUpita.GetDateTime("DatumRodjenja"), rezultatUpita.GetString("Pol"));
            rezultatUpita.Close();

            return osoba;
        }
    }
}