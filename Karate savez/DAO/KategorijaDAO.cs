﻿using Karate_savez.Database;
using Karate_savez.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DAO
{
    class KategorijaDAO
    {

        public List<KategorijaKata> DohvatiSveKataKategorije()
        {
            List<KategorijaKata> sveKataKategorije = new List<KategorijaKata>();

            MySqlCommand upitZaKataKategorije = DatabaseConnection.GetConnection().CreateCommand();
            upitZaKataKategorije.CommandText = "select * from kategorijakata natural inner join kategorija";

            MySqlDataReader rezultatUpita = upitZaKataKategorije.ExecuteReader();

            while (rezultatUpita.Read())
            {
                KategorijaKata kategorijaKata = new KategorijaKata(rezultatUpita.GetInt32("IdKategorije"), rezultatUpita.GetInt32("GodineOd"), rezultatUpita.GetInt32("GodineDo"), rezultatUpita.GetString("Pol"), rezultatUpita.GetInt16("Nivo"));
                sveKataKategorije.Add(kategorijaKata);
            }

            rezultatUpita.Close();

            return sveKataKategorije;
        }

        public List<KategorijaBorba> DohvatiSveBorbaKategorije()
        {
            List<KategorijaBorba> sveBorbaKategorije = new List<KategorijaBorba>();

            MySqlCommand upitZaBorbaKategorije = DatabaseConnection.GetConnection().CreateCommand();
            upitZaBorbaKategorije.CommandText = "select * from kategorijaborba natural inner join kategorija";

            MySqlDataReader rezultatUpita = upitZaBorbaKategorije.ExecuteReader();
            while (rezultatUpita.Read())
            {
                KategorijaBorba kategorijaBorba = new KategorijaBorba(rezultatUpita.GetInt32("IdKategorije"), rezultatUpita.GetInt32("GodineOd"), rezultatUpita.GetInt32("GodineDo"), rezultatUpita.GetString("Pol"), rezultatUpita.GetInt32("MasaOd"), rezultatUpita.GetInt32("MasaDo"));
                sveBorbaKategorije.Add(kategorijaBorba);
            }
            rezultatUpita.Close();

            return sveBorbaKategorije;
        }
        
        public List<Kategorija> DohvatiSveKategorije()
        {
            List<Kategorija> sveKategorije = new List<Kategorija>();
            sveKategorije.AddRange(DohvatiSveBorbaKategorije());
            sveKategorije.AddRange(DohvatiSveKataKategorije());
            return sveKategorije;
        }
    }
}
