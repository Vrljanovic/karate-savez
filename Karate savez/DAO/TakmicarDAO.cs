﻿using Karate_savez.Database;
using Karate_savez.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DAO
{
    public class TakmicarDAO
    {

        public List<Takmicar> DohvatiSveTakmicare()
        {
            List<Takmicar> sviTakmicari = new List<Takmicar>();

            MySqlCommand upitZaSveTakmicare = DatabaseConnection.GetConnection().CreateCommand();
            upitZaSveTakmicare.CommandText = "select * from svitakmicari";
            List<Klub> sviKlubovi = new KlubDAO().DohvatiKlubove();
            MySqlDataReader rezultatUpita = upitZaSveTakmicare.ExecuteReader();

            while (rezultatUpita.Read())
            {
                Klub klub = sviKlubovi.First(k => k.IdKluba == rezultatUpita.GetInt32("IdKluba"));
                Takmicar takmicar = new Takmicar(rezultatUpita.GetInt32("IdOsobe"), rezultatUpita.GetString("Ime"), rezultatUpita.GetString("Prezime"), rezultatUpita.GetDateTime("DatumRodjenja"), rezultatUpita.GetString("Pol"), rezultatUpita.GetString("BrojRegistracijeUSavezu"), klub, PronadjiPojas.Pojas(rezultatUpita.GetString("Pojas")),rezultatUpita.GetBoolean("JeAktivan"));
                sviTakmicari.Add(takmicar);
            }
            rezultatUpita.Close();
            return sviTakmicari;
        }

        public bool DodajTakmicara (Takmicar t)
        {
            if (!new OsobaDAO().DodajOsobu(t))
                return false;
            MySqlCommand iskazZaDodavanje = DatabaseConnection.GetConnection().CreateCommand();
            iskazZaDodavanje.CommandText = "insert into clan (idosobe, brojregistracijeusavezu, pojas, idkluba, jeaktivan) values(@IdOsobe, @BrojRegistracijeUSavezu, @Pojas, @IdKluba, @JeAktivan)";
            MySqlParameter idOsobeParametar = new MySqlParameter("@IdOsobe", MySqlDbType.Int32)
            {
                Value = t.IdOsobe
            };
            MySqlParameter brojRegistracijeUSavezuParametar = new MySqlParameter("@BrojRegistracijeUSavezu", MySqlDbType.VarChar)
            {
                Value = t.BrojRegistracijeUSavezu
            };
            MySqlParameter pojasParametar = new MySqlParameter("@Pojas", MySqlDbType.VarChar)
            {
                Value = t.Pojas
            };
            MySqlParameter IdKlubaParametar = new MySqlParameter("@IdKluba", MySqlDbType.Int32)
            {
                Value = t.Klub.IdKluba
            };
            MySqlParameter jeAktivanParametar = new MySqlParameter("@JeAktivan", MySqlDbType.Int16)
            {
                Value = t.JeAktivan
            };
            iskazZaDodavanje.Parameters.AddRange(new object[] { idOsobeParametar, brojRegistracijeUSavezuParametar, pojasParametar, IdKlubaParametar, jeAktivanParametar });
            iskazZaDodavanje.Prepare();
            try
            {
                iskazZaDodavanje.ExecuteNonQuery();
                return true;
            }
            catch(MySqlException mysqlex)
            {
                Console.WriteLine(mysqlex.Message);
                return false;
            }
        }

        public bool PromjeniAktivnost(Takmicar t)
        {
            MySqlCommand iskazZaPromjenuAktivnost = DatabaseConnection.GetConnection().CreateCommand();
            iskazZaPromjenuAktivnost.CommandText = "update clan set jeaktivan = @JeAktivan where idosobe = @IdOsobe";
            t.JeAktivan = !t.JeAktivan;
            MySqlParameter jeAktivanParametar = new MySqlParameter("@JeAktivan", MySqlDbType.Int16)
            {
                Value = t.JeAktivan
            };
            MySqlParameter idParametar = new MySqlParameter("@IdOsobe", MySqlDbType.Int32)
            {
                Value = t.IdOsobe
            };
            iskazZaPromjenuAktivnost.Parameters.AddRange(new object[] { jeAktivanParametar, idParametar });
            try
            {
                iskazZaPromjenuAktivnost.ExecuteNonQuery();
                return true;
            }
            catch(MySqlException mysqlex)
            {
                Console.WriteLine(mysqlex.Message);
                t.JeAktivan = !t.JeAktivan;
                return false;
            }
        }
        
        public Takmicar DohvatiPoBrojuRegistracije(string BrojRegistracije)
        {
            MySqlCommand upitZaTakmicara = DatabaseConnection.GetConnection().CreateCommand();
            upitZaTakmicara.CommandText = "select * from sviTakmicari where brojregistracijeusavezu = @BrojRegistracije";
            MySqlParameter brojRegistracijeParametar = new MySqlParameter("@BrojRegistracije", MySqlDbType.VarChar)
            {
                Value = BrojRegistracije
            };
            upitZaTakmicara.Parameters.Add(brojRegistracijeParametar);
            upitZaTakmicara.Prepare();

            MySqlDataReader rezultatUpita = upitZaTakmicara.ExecuteReader();

            if(rezultatUpita.Read())
            {
                int idKluba = rezultatUpita.GetInt32("IdKluba");
                string ime = rezultatUpita.GetString("Ime");
                DateTime datumRodjenja = rezultatUpita.GetDateTime("DatumRodjenja");
                string prezime = rezultatUpita.GetString("Prezime");
                bool jeAktivan = rezultatUpita.GetBoolean("JeAktivan");
                Pojas pojas = PronadjiPojas.Pojas(rezultatUpita.GetString("Pojas"));
                int idOsobe = rezultatUpita.GetInt32("IdOsobe");
                string pol = rezultatUpita.GetString("Pol");
                rezultatUpita.Close();
                Klub klub = new KlubDAO().DohvatiKlubPoIDu(idKluba);
                return new Takmicar(idOsobe, ime, prezime, datumRodjenja, BrojRegistracije, pol, klub, pojas, jeAktivan);
            }
            rezultatUpita.Close();
            return null;
        }

        public bool PromijeniPojas(Takmicar t, Pojas noviPojas)
        {
            MySqlCommand iskazPromjeniPojas = DatabaseConnection.GetConnection().CreateCommand();
            iskazPromjeniPojas.CommandText = "update clan set pojas = @Pojas where idosobe = @ID";
            MySqlParameter pojasParametar = new MySqlParameter("@Pojas", MySqlDbType.VarChar)
            {
                Value = noviPojas
            };
            MySqlParameter idParametar = new MySqlParameter("@ID", MySqlDbType.Int32)
            {
                Value = t.IdOsobe
            };
            iskazPromjeniPojas.Parameters.AddRange(new object[] { pojasParametar, idParametar });
            iskazPromjeniPojas.Prepare();
            try
            {
                iskazPromjeniPojas.ExecuteNonQuery();
                t.Pojas = noviPojas;
                return true;
            }
            catch (MySqlException mysqlex)
            {
                Console.WriteLine(mysqlex.Message);
                return false;
            }
        }

        public bool PromijeniKlub(Takmicar t, Klub noviKlub)
        {
            MySqlCommand iskazPromjenaKluba = DatabaseConnection.GetConnection().CreateCommand();
            iskazPromjenaKluba.CommandText = "update clan set idkluba = @IdKluba where idosobe = @IdOsobe";
            MySqlParameter idKlubaParametar = new MySqlParameter("@IdKluba", MySqlDbType.Int32)
            {
                Value = noviKlub.IdKluba
            };
            MySqlParameter idOsobeParametar = new MySqlParameter("IdOsobe", MySqlDbType.Int32)
            {
                Value = t.IdOsobe
            };
            iskazPromjenaKluba.Parameters.AddRange(new object[] { idKlubaParametar, idOsobeParametar });
            iskazPromjenaKluba.Prepare();
            try
            {
                iskazPromjenaKluba.ExecuteNonQuery();
                t.Klub = noviKlub;
                return true;
            }
            catch(MySqlException mysqlex)
            {
                Console.WriteLine(mysqlex.Message);
                return false;
            }
        }

        public Takmicar DohvatiPoIDu(int id)
        {
            MySqlCommand upitZaTakmicara = DatabaseConnection.GetConnection().CreateCommand();
            upitZaTakmicara.CommandText = "select * from sviTakmicari where idosobe = @Id";
            MySqlParameter idParametar = new MySqlParameter("@Id", MySqlDbType.Int32)
            {
                Value = id
            };
            upitZaTakmicara.Parameters.Add(idParametar);
            upitZaTakmicara.Prepare();

            MySqlDataReader rezultatUpita = upitZaTakmicara.ExecuteReader();

            if (rezultatUpita.Read())
            {
                int idKluba = rezultatUpita.GetInt32("IdKluba");
                string ime = rezultatUpita.GetString("Ime");
                DateTime datumRodjenja = rezultatUpita.GetDateTime("DatumRodjenja");
                string prezime = rezultatUpita.GetString("Prezime");
                bool jeAktivan = rezultatUpita.GetBoolean("JeAktivan");
                Pojas pojas = PronadjiPojas.Pojas(rezultatUpita.GetString("Pojas"));
                string brojRegistracije = rezultatUpita.GetString("BrojRegistracijeUSavezu");
                string pol = rezultatUpita.GetString("Pol");
                rezultatUpita.Close();
                Klub klub = new KlubDAO().DohvatiKlubPoIDu(idKluba);
                return new Takmicar(id, ime, prezime, datumRodjenja, brojRegistracije, pol, klub, pojas, jeAktivan);
            }
            rezultatUpita.Close();
            return null;
        }

        public List<Takmicar> DohvatiPoImenu(string str)
        {
            List<Takmicar> takmicari = new List<Takmicar>();
            MySqlCommand upitZaTakmicare = DatabaseConnection.GetConnection().CreateCommand();
            upitZaTakmicare.CommandText = "select jeaktivan, idkluba, idosobe, ime, prezime, datumrodjenja, pol, brojregistracijeusavezu, pojas, nazivkluba from svitakmicari s natural left join klub k where concat(ime, ' ', prezime) like '%" + str + "%'";
            List<Klub> sviKlubovi = new KlubDAO().DohvatiKlubove();

            MySqlDataReader rezultatUpita = upitZaTakmicare.ExecuteReader();

            while (rezultatUpita.Read())
            {
                Klub klub = sviKlubovi.First(k => k.IdKluba == rezultatUpita.GetInt32("IdKluba"));
                Takmicar takmicar = new Takmicar(rezultatUpita.GetInt32("IdOsobe"), rezultatUpita.GetString("Ime"), rezultatUpita.GetString("Prezime"), rezultatUpita.GetDateTime("DatumRodjenja"), rezultatUpita.GetString("Pol"), rezultatUpita.GetString("BrojRegistracijeUSavezu"), klub, PronadjiPojas.Pojas(rezultatUpita.GetString("Pojas")), rezultatUpita.GetBoolean("JeAktivan"));
                takmicari.Add(takmicar);
            }
            rezultatUpita.Close();
            return takmicari;
        }

        public List<Takmicar> DohvatiPoKlubu(string kl)
        {
            List<Takmicar> takmicari = new List<Takmicar>();
            MySqlCommand upitZaTakmicareIzKluba = DatabaseConnection.GetConnection().CreateCommand();
            upitZaTakmicareIzKluba.CommandText = "select * from svitakmicari s natural inner join klub k where k.nazivkluba like '%" + kl + "%'";
            List<Klub> sviKlubovi = new KlubDAO().DohvatiKlubove();

            MySqlDataReader rezultatUpita = upitZaTakmicareIzKluba.ExecuteReader();

            while (rezultatUpita.Read())
            {
                Klub klub = sviKlubovi.First(k => k.IdKluba == rezultatUpita.GetInt32("IdKluba"));
                Takmicar takmicar = new Takmicar(rezultatUpita.GetInt32("IdOsobe"), rezultatUpita.GetString("Ime"), rezultatUpita.GetString("Prezime"), rezultatUpita.GetDateTime("DatumRodjenja"), rezultatUpita.GetString("Pol"), rezultatUpita.GetString("BrojRegistracijeUSavezu"), klub, PronadjiPojas.Pojas(rezultatUpita.GetString("Pojas")), rezultatUpita.GetBoolean("JeAktivan"));
                takmicari.Add(takmicar);
            }
            rezultatUpita.Close();
            return takmicari;
        }

        public List<Takmicar> DohvatiPoPojasu(string pojas)
        {
            List<Takmicar> takmicari = new List<Takmicar>();
            MySqlCommand upitZaTakmicare = DatabaseConnection.GetConnection().CreateCommand();
            upitZaTakmicare.CommandText = "select * from svitakmicari where pojas like '%" + pojas + "%'";
            List<Klub> sviKlubovi = new KlubDAO().DohvatiKlubove();

            MySqlDataReader rezultatUpita = upitZaTakmicare.ExecuteReader();

            while (rezultatUpita.Read())
            {
                Klub klub = sviKlubovi.First(k => k.IdKluba == rezultatUpita.GetInt32("IdKluba"));
                Takmicar takmicar = new Takmicar(rezultatUpita.GetInt32("IdOsobe"), rezultatUpita.GetString("Ime"), rezultatUpita.GetString("Prezime"), rezultatUpita.GetDateTime("DatumRodjenja"), rezultatUpita.GetString("Pol"), rezultatUpita.GetString("BrojRegistracijeUSavezu"), klub, PronadjiPojas.Pojas(rezultatUpita.GetString("Pojas")), rezultatUpita.GetBoolean("JeAktivan"));
                takmicari.Add(takmicar);
            }
            rezultatUpita.Close();
            return takmicari;
        }
    }
}