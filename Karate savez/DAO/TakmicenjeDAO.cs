﻿using Karate_savez.Database;
using Karate_savez.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DAO
{
    class TakmicenjeDAO
    {
        public List<Takmicenje> DohvatiSvaTakmicenja()
        {
            List<Takmicenje> svaTakmicenja = new List<Takmicenje>();

            MySqlCommand upitZaSvaTakmicenja = DatabaseConnection.GetConnection().CreateCommand();
            upitZaSvaTakmicenja.CommandText = "select IdTakmicenja, MjestoOdrzavanja, Naziv, Adresa, BrojBorilista, PocetakTakmicenja from takmicenje t inner join mjesto m on t.mjestoodrzavanja = m.idmjesta";
            MySqlDataReader rezultatUpita = upitZaSvaTakmicenja.ExecuteReader();
            while (rezultatUpita.Read())
            {
                Mjesto mjesto = new Mjesto(rezultatUpita.GetInt32("MjestoOdrzavanja"), rezultatUpita.GetString("Naziv"), rezultatUpita.GetString("Adresa"));
                Takmicenje takmicenje = new Takmicenje(rezultatUpita.GetInt32("IdTakmicenja"), rezultatUpita.GetDateTime("PocetakTakmicenja"), rezultatUpita.GetInt16("BrojBorilista"), mjesto);
                svaTakmicenja.Add(takmicenje);
            }
            rezultatUpita.Close();

            return svaTakmicenja;
        }

        public List<Takmicenje> DohvatiOdrzanaTakmicenja()
        {
            List<Takmicenje> odrzanaTakmicenja = new List<Takmicenje>();

            MySqlCommand upitZaOdrzanaTakmicenja = DatabaseConnection.GetConnection().CreateCommand();
            upitZaOdrzanaTakmicenja.CommandText = "select * from odrzanatakmicenja";
            MySqlDataReader rezultatUpita = upitZaOdrzanaTakmicenja.ExecuteReader();
            while (rezultatUpita.Read())
            {
                Mjesto mjesto = new Mjesto(rezultatUpita.GetInt32("MjestoOdrzavanja"), rezultatUpita.GetString("Naziv"), rezultatUpita.GetString("Adresa"));
                Takmicenje takmicenje = new Takmicenje(rezultatUpita.GetInt32("IdTakmicenja"), rezultatUpita.GetDateTime("PocetakTakmicenja"), rezultatUpita.GetInt16("BrojBorilista"), mjesto);
                odrzanaTakmicenja.Add(takmicenje);
            }
            rezultatUpita.Close();

            return odrzanaTakmicenja;
        }

        public List<Takmicenje> DohvatiBuducaTakmicenja()
        {
            List<Takmicenje> odrzanaTakmicenja = new List<Takmicenje>();

            MySqlCommand upitZaOdrzanaTakmicenja = DatabaseConnection.GetConnection().CreateCommand();
            upitZaOdrzanaTakmicenja.CommandText = "select * from buducatakmicenja";
            MySqlDataReader rezultatUpita = upitZaOdrzanaTakmicenja.ExecuteReader();
            while (rezultatUpita.Read())
            {
                Mjesto mjesto = new Mjesto(rezultatUpita.GetInt32("MjestoOdrzavanja"), rezultatUpita.GetString("Naziv"), rezultatUpita.GetString("Adresa"));
                Takmicenje takmicenje = new Takmicenje(rezultatUpita.GetInt32("IdTakmicenja"), rezultatUpita.GetDateTime("PocetakTakmicenja"), rezultatUpita.GetInt16("BrojBorilista"), mjesto);
                odrzanaTakmicenja.Add(takmicenje);
            }
            rezultatUpita.Close();

            return odrzanaTakmicenja;
        }

        public bool DodajTakmicenje(Takmicenje takmicenje)
        {
            MySqlCommand iskazDodajTakmicenje = DatabaseConnection.GetConnection().CreateCommand();
            iskazDodajTakmicenje.CommandText = "insert into takmicenje (PocetakTakmicenja, BrojBorilista, MjestoOdrzavanja) values (@PocetakTakmicenja, @BrojBorilista, @MjestoOdrzavanja)";
            MjestoDAO mjestoDAO = new MjestoDAO();
            if (!mjestoDAO.MjestoPostoji(takmicenje.MjestoOdrzavanja))
                mjestoDAO.DodajMjesto(takmicenje.MjestoOdrzavanja);
            MySqlParameter pocetakTakmicenjaParametar = new MySqlParameter("@PocetakTakmicenja", MySqlDbType.DateTime)
            {
                Value = takmicenje.PocetakTakmicenja
            };
            MySqlParameter brojBorilistaParametar = new MySqlParameter("@BrojBorilista", MySqlDbType.Int16)
            {
                Value = takmicenje.BrojBorilista
            };
            MySqlParameter mjestoOdrzavanjaParametar = new MySqlParameter("@MjestoOdrzavanja", MySqlDbType.Int32)
            {
                Value = takmicenje.MjestoOdrzavanja.IdMjesta
            };
            iskazDodajTakmicenje.Parameters.AddRange(new object[] { pocetakTakmicenjaParametar, brojBorilistaParametar, mjestoOdrzavanjaParametar });
            iskazDodajTakmicenje.Prepare();
            try
            {
                iskazDodajTakmicenje.ExecuteNonQuery();
                takmicenje.IdTakmicenja = (int)iskazDodajTakmicenje.LastInsertedId;
                return true;
            }
            catch (MySqlException mysqlex)
            {
                Console.WriteLine(mysqlex.Message);
                return false;
            }
        }

        public Takmicenje DohvatiTakmicenjePoIDu(int id)
        {
            Takmicenje takmicenje = null;
            MySqlCommand upitZaTakmicenje = DatabaseConnection.GetConnection().CreateCommand();
            upitZaTakmicenje.CommandText = "select IdTakmicenja, MjestoOdrzavanja, Naziv, Adresa, BrojBorilista, PocetakTakmicenja from takmicenje t inner join mjesto m on t.mjestoodrzavanja = m.idmjesta where idtakmicenja = @ID";
            MySqlParameter idParametar = new MySqlParameter("@ID", MySqlDbType.Int32) { Value = id };
            upitZaTakmicenje.Parameters.Add(idParametar);
            upitZaTakmicenje.Prepare();
            MySqlDataReader rezultatUpita = upitZaTakmicenje.ExecuteReader();

            if (rezultatUpita.Read())
            {
                Mjesto mjesto = new Mjesto(rezultatUpita.GetInt32("MjestoOdrzavanja"), rezultatUpita.GetString("Naziv"), rezultatUpita.GetString("Adresa"));
                takmicenje = new Takmicenje(rezultatUpita.GetInt32("IdTakmicenja"), rezultatUpita.GetDateTime("PocetakTakmicenja"), rezultatUpita.GetInt16("BrojBorilista"), mjesto);
            }
            rezultatUpita.Close();

            return takmicenje;
        }

        public List<Takmicenje> DohvatiTakmicenjaPoMjestu(string mjesto)
        {
            List<Takmicenje> takmicenja = new List<Takmicenje>();
            MySqlCommand upitZaTakmicenja = DatabaseConnection.GetConnection().CreateCommand();
            List<Mjesto> mjesta = new MjestoDAO().DohvatiSvaMjesta();
            upitZaTakmicenja.CommandText = "select * from takmicenje inner join mjesto on mjestoodrzavanja = idmjesta where concat(adresa, ' ', naziv) like '%" + mjesto + "%'";
            MySqlDataReader rezultatUpita = upitZaTakmicenja.ExecuteReader();

            while (rezultatUpita.Read())
            {
                Mjesto mj = mjesta.First(m => m.IdMjesta == rezultatUpita.GetInt32("IdMjesta"));
                int id = rezultatUpita.GetInt32("IdTakmicenja");
                int brojBorilista = rezultatUpita.GetInt32("BrojBorilista");
                DateTime pocetak = rezultatUpita.GetDateTime("PocetakTakmicenja");
                takmicenja.Add(new Takmicenje(id, pocetak, brojBorilista, mj));
            }
            rezultatUpita.Close();

            return takmicenja;
        }

        public List<Takmicenje> DohvatiOdrzanaTakmicenjaPoMjestu(string mjesto)
        {
            List<Takmicenje> takmicenja = new List<Takmicenje>();
            MySqlCommand upitZaTakmicenja = DatabaseConnection.GetConnection().CreateCommand();
            List<Mjesto> mjesta = new MjestoDAO().DohvatiSvaMjesta();
            upitZaTakmicenja.CommandText = "select * from takmicenje inner join mjesto on mjestoodrzavanja = idmjesta where pocetaktakmicenja < sysdate() and concat(adresa, ' ', naziv) like '%" + mjesto + "%'";
            MySqlDataReader rezultatUpita = upitZaTakmicenja.ExecuteReader();

            while (rezultatUpita.Read())
            {
                Mjesto mj = mjesta.First(m => m.IdMjesta == rezultatUpita.GetInt32("IdMjesta"));
                int id = rezultatUpita.GetInt32("IdTakmicenja");
                int brojBorilista = rezultatUpita.GetInt32("BrojBorilista");
                DateTime pocetak = rezultatUpita.GetDateTime("PocetakTakmicenja");
                takmicenja.Add(new Takmicenje(id, pocetak, brojBorilista, mj));
            }
            rezultatUpita.Close();

            return takmicenja;
        }
    }
}