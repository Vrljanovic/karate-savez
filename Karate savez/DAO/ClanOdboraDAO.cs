﻿using Karate_savez.Database;
using Karate_savez.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DAO
{
    class ClanOdboraDAO
    {
        public List<ClanUpravnogOdbora> DohvatiClanoveUpravnogOdbora()
        {
            List<ClanUpravnogOdbora> sviClanovi = new List<ClanUpravnogOdbora>();

            MySqlCommand upitZaClanoveUO = DatabaseConnection.GetConnection().CreateCommand();
            upitZaClanoveUO.CommandText = "select * from osoba natural inner join clanupravnogodbora";
            MySqlDataReader rezultatUpita = upitZaClanoveUO.ExecuteReader();

            while (rezultatUpita.Read())
            {
                int idOsobe = rezultatUpita.GetInt32("IdOsobe");
                string ime = rezultatUpita.GetString("Ime");
                string prezime = rezultatUpita.GetString("Prezime");
                string pol = rezultatUpita.GetString("Pol");
                DateTime datumRodjenja = rezultatUpita.GetDateTime("DatumRodjenja");
                DateTime clanOd = rezultatUpita.GetDateTime("Od");
                DateTime clanDo = rezultatUpita.GetDateTime("Do");

                sviClanovi.Add(new ClanUpravnogOdbora(idOsobe, ime, prezime, pol, datumRodjenja, clanOd, clanDo));
            }
            rezultatUpita.Close();

            return sviClanovi;
        }

        public List<ClanNadzornogOdbora> DohvatiClanoveNadzornogOdbora()
        {
            List<ClanNadzornogOdbora> sviClanovi = new List<ClanNadzornogOdbora>();

            MySqlCommand upitZaClanoveUO = DatabaseConnection.GetConnection().CreateCommand();
            upitZaClanoveUO.CommandText = "select * from osoba natural inner join clannadzornogodbora";
            MySqlDataReader rezultatUpita = upitZaClanoveUO.ExecuteReader();

            while (rezultatUpita.Read())
            {
                int idOsobe = rezultatUpita.GetInt32("IdOsobe");
                string ime = rezultatUpita.GetString("Ime");
                string prezime = rezultatUpita.GetString("Prezime");
                string pol = rezultatUpita.GetString("Pol");
                DateTime datumRodjenja = rezultatUpita.GetDateTime("DatumRodjenja");
                DateTime clanOd = rezultatUpita.GetDateTime("Od");
                DateTime clanDo = rezultatUpita.GetDateTime("Do");

                sviClanovi.Add(new ClanNadzornogOdbora(idOsobe, ime, prezime, pol, datumRodjenja, clanOd, clanDo));
            }
            rezultatUpita.Close();

            return sviClanovi;
        }

        public bool DodajClanaUpravnogOdbora(ClanUpravnogOdbora c)
        {
            OsobaDAO osobaDAO = new OsobaDAO();
            if (osobaDAO.DodajOsobu(c))
            {
                MySqlCommand iskazZaDodavanje = DatabaseConnection.GetConnection().CreateCommand();
                iskazZaDodavanje.CommandText = "insert into clanupravnogodbora values(@Id, @Od, @Do)";
                MySqlParameter idParametar = new MySqlParameter("@Id", MySqlDbType.Int32) { Value = c.IdOsobe };
                MySqlParameter odParametar = new MySqlParameter("@Od", MySqlDbType.Date) { Value = c.ClanOd.Date };
                MySqlParameter doParametar = new MySqlParameter("@Do", MySqlDbType.Date) { Value = c.ClanDo.Date };
                iskazZaDodavanje.Parameters.AddRange(new object[] { idParametar, odParametar, doParametar });
                iskazZaDodavanje.Prepare();
                try
                {
                    iskazZaDodavanje.ExecuteNonQuery();
                    return true;
                }
                catch (MySqlException mysqlex)
                {
                    Console.WriteLine(mysqlex.Message);
                    return false;
                }
            }
            return false;
        }

        public bool DodajClanaNadzornogOdbora(ClanNadzornogOdbora c)
        {
            OsobaDAO osobaDAO = new OsobaDAO();
            if (osobaDAO.DodajOsobu(c))
            {
                MySqlCommand iskazZaDodavanje = DatabaseConnection.GetConnection().CreateCommand();
                iskazZaDodavanje.CommandText = "insert into clannadzornogodbora values(@Id, @Od, @Do)";
                MySqlParameter idParametar = new MySqlParameter("@Id", MySqlDbType.Int32) { Value = c.IdOsobe };
                MySqlParameter odParametar = new MySqlParameter("@Od", MySqlDbType.Date) { Value = c.ClanOd.Date };
                MySqlParameter doParametar = new MySqlParameter("@Do", MySqlDbType.Date) { Value = c.ClanDo.Date };
                iskazZaDodavanje.Parameters.AddRange(new object[] { idParametar, odParametar, doParametar });
                iskazZaDodavanje.Prepare();
                try
                {
                    iskazZaDodavanje.ExecuteNonQuery();
                    return true;
                }
                catch (MySqlException mysqlex)
                {
                    Console.WriteLine(mysqlex.Message);
                    return false;
                }
            }
            return false;
        }

        public ClanUpravnogOdbora DohvatiClanaUpravnogOdboraPoIdu(int id)
        {
            MySqlCommand upitZaClanaSaIDom = DatabaseConnection.GetConnection().CreateCommand();
            upitZaClanaSaIDom.CommandText = "select * from osoba natural inner join clanupravnogodbora where idosobe = @IdOsobe";
            MySqlParameter idOsobeParametar = new MySqlParameter("@IdOsobe", MySqlDbType.Int32) { Value = id };
            upitZaClanaSaIDom.Parameters.Add(idOsobeParametar);
            upitZaClanaSaIDom.Prepare();
            MySqlDataReader rezultatUpita = upitZaClanaSaIDom.ExecuteReader();
            ClanUpravnogOdbora clan = null;
            if (rezultatUpita.Read())
            {
                string ime = rezultatUpita.GetString("Ime");
                string prezime = rezultatUpita.GetString("Prezime");
                string pol = rezultatUpita.GetString("Pol");
                DateTime datumRodjenja = rezultatUpita.GetDateTime("DatumRodjenja");
                DateTime od = rezultatUpita.GetDateTime("OD");
                DateTime _do = rezultatUpita.GetDateTime("DO");
                clan = new ClanUpravnogOdbora(id, ime, prezime, pol, datumRodjenja, od, _do);
            }
            rezultatUpita.Close();
            return clan;
        }

        public ClanNadzornogOdbora DohvatiClanaNadzornogOdboraPoIdu(int id)
        {
            MySqlCommand upitZaClanaSaIDom = DatabaseConnection.GetConnection().CreateCommand();
            upitZaClanaSaIDom.CommandText = "select * from osoba natural inner join clannadzornogodbora where idosobe = @IdOsobe";
            MySqlParameter idOsobeParametar = new MySqlParameter("@IdOsobe", MySqlDbType.Int32) { Value = id };
            upitZaClanaSaIDom.Parameters.Add(idOsobeParametar);
            upitZaClanaSaIDom.Prepare();
            MySqlDataReader rezultatUpita = upitZaClanaSaIDom.ExecuteReader();
            ClanNadzornogOdbora clan = null;
            if (rezultatUpita.Read())
            {
                string ime = rezultatUpita.GetString("Ime");
                string prezime = rezultatUpita.GetString("Prezime");
                string pol = rezultatUpita.GetString("Pol");
                DateTime datumRodjenja = rezultatUpita.GetDateTime("DatumRodjenja");
                DateTime od = rezultatUpita.GetDateTime("OD");
                DateTime _do = rezultatUpita.GetDateTime("DO");
                clan = new ClanNadzornogOdbora(id, ime, prezime, pol, datumRodjenja, od, _do);
            }
            rezultatUpita.Close();
            return clan;
        }

        public List<ClanUpravnogOdbora> DohvatiClanaUpravnogOdboraPoImenu(string str)
        {
            List<ClanUpravnogOdbora> clanovi = new List<ClanUpravnogOdbora>();
            MySqlCommand upitZaClanove = DatabaseConnection.GetConnection().CreateCommand();
            upitZaClanove.CommandText = "select * from osoba natural inner join clanupravnogodbora where concat(ime, ' ', prezime) like '%" + str + "%'";

            MySqlDataReader rezultatUpita = upitZaClanove.ExecuteReader();
            while (rezultatUpita.Read())
            {
                int id = rezultatUpita.GetInt32("IdOsobe");
                string ime = rezultatUpita.GetString("Ime");
                string prezime = rezultatUpita.GetString("Prezime");
                string pol = rezultatUpita.GetString("Pol");
                DateTime datumRodjenja = rezultatUpita.GetDateTime("DatumRodjenja");
                DateTime od = rezultatUpita.GetDateTime("OD");
                DateTime _do = rezultatUpita.GetDateTime("DO");
                clanovi.Add(new ClanUpravnogOdbora(id, ime, prezime, pol, datumRodjenja, od, _do));
            }
            rezultatUpita.Close();

            return clanovi;
        }

        public List<ClanNadzornogOdbora> DohvataClanaNadzornogOdboraPoImenu(string str)
        {
            List<ClanNadzornogOdbora> clanovi = new List<ClanNadzornogOdbora>();
            MySqlCommand upitZaClanove = DatabaseConnection.GetConnection().CreateCommand();
            upitZaClanove.CommandText = "select * from osoba natural inner join clannadzornogodbora where concat(ime, ' ', prezime) like '%" + str + "%'";

            MySqlDataReader rezultatUpita = upitZaClanove.ExecuteReader();
            while (rezultatUpita.Read())
            {
                int id = rezultatUpita.GetInt32("IdOsobe");
                string ime = rezultatUpita.GetString("Ime");
                string prezime = rezultatUpita.GetString("Prezime");
                string pol = rezultatUpita.GetString("Pol");
                DateTime datumRodjenja = rezultatUpita.GetDateTime("DatumRodjenja");
                DateTime od = rezultatUpita.GetDateTime("OD");
                DateTime _do = rezultatUpita.GetDateTime("DO");
                clanovi.Add(new ClanNadzornogOdbora(id, ime, prezime, pol, datumRodjenja, od, _do));
            }
            rezultatUpita.Close();

            return clanovi;
        }
    }
}
