﻿using Karate_savez.Database;
using Karate_savez.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace Karate_savez.DAO
{
    class SastanakDAO
    {
        public List<Sastanak> DohvatiSveSastanke()
        {
            List<Sastanak> sastanci = new List<Sastanak>();

            MySqlCommand upitZaSastanke = DatabaseConnection.GetConnection().CreateCommand();
            upitZaSastanke.CommandText = "select * from sastanak";

            MySqlDataReader rezultatUpita = upitZaSastanke.ExecuteReader();
            while (rezultatUpita.Read())
            {
                int id = rezultatUpita.GetInt32("IdSastanka");
                DateTime datumOdrzavanja = rezultatUpita.GetDateTime("DatumOdrzavanja");
                string tema = rezultatUpita.GetString("Tema");
                sastanci.Add(new Sastanak(id, datumOdrzavanja, tema));
            }
            rezultatUpita.Close();

            return sastanci;
        }

        public void DodajSastanak(Sastanak s)
        {
            MySqlCommand iskazZaDodavanjeSastanka = DatabaseConnection.GetConnection().CreateCommand();
            iskazZaDodavanjeSastanka.CommandText = "insert into sastanak (datumodrzavanja, tema) values (@Datum, @Tema)";
            MySqlParameter datumParametar = new MySqlParameter("@Datum", MySqlDbType.DateTime) { Value = s.DatumOdrzavanja };
            MySqlParameter temaParametar = new MySqlParameter("@Tema", MySqlDbType.VarChar) { Value = s.Tema };
            iskazZaDodavanjeSastanka.Parameters.AddRange(new object[] { datumParametar, temaParametar });
            iskazZaDodavanjeSastanka.Prepare();
            try
            {
                iskazZaDodavanjeSastanka.ExecuteNonQuery();
                s.IdSastanka = (int)iskazZaDodavanjeSastanka.LastInsertedId;
            }
            catch (MySqlException mysqlex)
            {
                Console.WriteLine(mysqlex.Message);
            }
        }
        
        public void DodajPrisutnogClanaUpravnogOdbora(Sastanak s, int idClana)
        {
            ClanUpravnogOdbora c = new ClanOdboraDAO().DohvatiClanaUpravnogOdboraPoIdu(idClana);
            MySqlCommand iskazZaPrisustvo = DatabaseConnection.GetConnection().CreateCommand();
            iskazZaPrisustvo.CommandText = "insert into ucestvujecuo values (@IdClana, @Od, @Do, @Idsastanka)";
            MySqlParameter idClanaParametar = new MySqlParameter("@IdClana", MySqlDbType.Int32) { Value = c.IdOsobe };
            MySqlParameter odParametar = new MySqlParameter("@Od", MySqlDbType.Date) { Value = c.ClanOd };
            MySqlParameter doParametar = new MySqlParameter("@Do", MySqlDbType.Date) { Value = c.ClanDo };
            MySqlParameter idSastankaParametar = new MySqlParameter("@IdSastanka", MySqlDbType.Int32) { Value = s.IdSastanka };
            iskazZaPrisustvo.Parameters.AddRange(new object[] { idClanaParametar, odParametar, doParametar, idSastankaParametar });
            iskazZaPrisustvo.Prepare();
            try
            {
                iskazZaPrisustvo.ExecuteNonQuery();
            }
            catch(MySqlException mysqlex)
            {
                Console.WriteLine(mysqlex.Message);
            }
        }

        public void DodajPrisutnogClanaNadzornogOdbora(Sastanak s, int idClana)
        {
            ClanNadzornogOdbora c = new ClanOdboraDAO().DohvatiClanaNadzornogOdboraPoIdu(idClana);
            MySqlCommand iskazZaPrisustvo = DatabaseConnection.GetConnection().CreateCommand();
            iskazZaPrisustvo.CommandText = "insert into ucestvujecno values (@IdClana, @Od, @Do, @Idsastanka)";
            MySqlParameter idClanaParametar = new MySqlParameter("@IdClana", MySqlDbType.Int32) { Value = c.IdOsobe };
            MySqlParameter odParametar = new MySqlParameter("@Od", MySqlDbType.DateTime) { Value = c.ClanOd };
            MySqlParameter doParametar = new MySqlParameter("@Do", MySqlDbType.DateTime) { Value = c.ClanDo };
            MySqlParameter idSastankaParametar = new MySqlParameter("@IdSastanka", MySqlDbType.Int32) { Value = s.IdSastanka };
            iskazZaPrisustvo.Parameters.AddRange(new object[] { idClanaParametar, odParametar, doParametar, idSastankaParametar });
            iskazZaPrisustvo.Prepare();
            try
            {
                iskazZaPrisustvo.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                Console.WriteLine(mysqlex.Message);
            }
        }

        public List<Osoba> DohvatiPrisutneNaSastanku(Sastanak s)
        {
            List<Osoba> prisutni = new List<Osoba>();
            List<Osoba> sveOsobe = new List<Osoba>();
            ClanOdboraDAO clanOdboraDAO = new ClanOdboraDAO();
            sveOsobe.AddRange(clanOdboraDAO.DohvatiClanoveNadzornogOdbora());
            sveOsobe.AddRange(clanOdboraDAO.DohvatiClanoveUpravnogOdbora());

            MySqlCommand upitZaPrisutne = DatabaseConnection.GetConnection().CreateCommand();
            upitZaPrisutne.CommandText = "select IdClanaUpravnogOdbora from ucestvujecuo where IdSastanka = @Id";
            MySqlParameter idParametar = new MySqlParameter("@Id", MySqlDbType.Int32) { Value = s.IdSastanka };
            upitZaPrisutne.Parameters.Add(idParametar);
            upitZaPrisutne.Prepare();
            MySqlDataReader rezultatUpita = upitZaPrisutne.ExecuteReader();

            while (rezultatUpita.Read())
            {
                int idOsobe = rezultatUpita.GetInt32("IdClanaUpravnogOdbora");
                prisutni.Add(sveOsobe.Find(o => o.IdOsobe == idOsobe));
            }
            rezultatUpita.Close();

            upitZaPrisutne.CommandText = "select IdClanaNadzornogOdbora from ucestvujecno where IdSastanka = @Id";
            upitZaPrisutne.Prepare();
            rezultatUpita = upitZaPrisutne.ExecuteReader();

            while (rezultatUpita.Read())
                prisutni.Add(sveOsobe.Find(o => o.IdOsobe == rezultatUpita.GetInt32("IdClanaNadzornogOdbora")));
            rezultatUpita.Close();

            return prisutni;
        }

        public Sastanak DohvatiSastanakPoIDu(int id)
        {
            Sastanak s = null;
            MySqlCommand upitZaSastanak = DatabaseConnection.GetConnection().CreateCommand();
            upitZaSastanak.CommandText = "select * from sastanak where idSastanka = @Id";
            MySqlParameter idParametar = new MySqlParameter("@Id", MySqlDbType.Int32) { Value = id };
            upitZaSastanak.Parameters.Add(idParametar);
            upitZaSastanak.Prepare();
            MySqlDataReader rezultatUpita = upitZaSastanak.ExecuteReader();

            if (rezultatUpita.Read())
            {
                DateTime datum = rezultatUpita.GetDateTime("DatumOdrzavanja");
                string tema = rezultatUpita.GetString("Tema");
                s = new Sastanak(id, datum, tema);
            }
            rezultatUpita.Close();
            return s;
        }

        public List<Sastanak> DohvatiPoTemi(string str)
        {
            List<Sastanak> sastanci = new List<Sastanak>();
            MySqlCommand upitZaSastanke = DatabaseConnection.GetConnection().CreateCommand();
            upitZaSastanke.CommandText = "select * from sastanak where tema like '%" + str + "%'";
            MySqlDataReader rezultatUpita = upitZaSastanke.ExecuteReader();
            while(rezultatUpita.Read())
            {
                int id = rezultatUpita.GetInt32("IdSastanka");
                DateTime datum = rezultatUpita.GetDateTime("DatumOdrzavanja");
                string tema = rezultatUpita.GetString("Tema");
                sastanci.Add(new Sastanak(id, datum, tema));
            }
            rezultatUpita.Close();
            return sastanci;
        }
    }
}
