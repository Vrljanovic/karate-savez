﻿using Karate_savez.Database;
using Karate_savez.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DAO
{
    class SudijaDAO
    {
        public List<Sudija> DohvatiSveSudije()
        {
            List<Sudija> sveSudije = new List<Sudija>();

            MySqlCommand upitZaSveSudije = DatabaseConnection.GetConnection().CreateCommand();
            upitZaSveSudije.CommandText = "select * from svesudije";
            MySqlDataReader rezultatUpita = upitZaSveSudije.ExecuteReader();
            while (rezultatUpita.Read())
            {
                Sudija sudija = new Sudija(rezultatUpita.GetInt32("IdOsobe"), rezultatUpita.GetString("Ime"), rezultatUpita.GetString("Prezime"), rezultatUpita.GetDateTime("DatumRodjenja"), rezultatUpita.GetString("Pol"), rezultatUpita.GetString("BrojSudijskeLicence"), rezultatUpita.GetBoolean("JeAktivan"));
                sveSudije.Add(sudija);
            }
            rezultatUpita.Close();
            return sveSudije;
        }

        public bool DodajSudiju(Sudija s)
        {
            if (JeSudija(s))
                return false;
            MySqlCommand iskazDodajSudiju = DatabaseConnection.GetConnection().CreateCommand();
            iskazDodajSudiju.CommandText = "insert into sudija (idosobe, brojsudijskelicence, jeaktivan) values (@IdOsobe, @BrojSudijskeLicence, @JeAktivan)";
            OsobaDAO osobaDAO = new OsobaDAO();
            if (osobaDAO.DodajOsobu(s))
            {
                MySqlParameter idOsobeParametar = new MySqlParameter("@IdOsobe", MySqlDbType.Int32)
                {
                    Value = s.IdOsobe
                };
                MySqlParameter brojSudijskeLicenceParametar = new MySqlParameter("@BrojSudijskeLicence", MySqlDbType.VarChar)
                {
                    Value = s.BrojSudijskeLicence
                };
                MySqlParameter jeAktivanParametar = new MySqlParameter("@JeAktivan", MySqlDbType.Int16)
                {
                    Value = s.JeAktivan ? 1 : 0
                };
                iskazDodajSudiju.Parameters.Add(idOsobeParametar);
                iskazDodajSudiju.Parameters.Add(brojSudijskeLicenceParametar);
                iskazDodajSudiju.Parameters.Add(jeAktivanParametar);
                iskazDodajSudiju.Prepare();
                try
                {
                    iskazDodajSudiju.ExecuteNonQuery();
                    return true;
                }
                catch(MySqlException mysqlex)
                {
                    osobaDAO.UkloniOsobu(s);
                    Console.WriteLine(mysqlex.Message);
                    return false;
                }
            }
            return false;
        }

        public Sudija DohvatiSudijuPoBrojuLicence(string BrojSudijskeLicence)
        {
            Sudija sudija = null;

            MySqlCommand upitZaSudiju = DatabaseConnection.GetConnection().CreateCommand();
            upitZaSudiju.CommandText = "select * from svesudije where brojsudijskelicence = @BrojLicence";
            MySqlParameter brojLicenceParametar = new MySqlParameter("@BrojLicence", MySqlDbType.VarChar)
            {
                Value = BrojSudijskeLicence
            };
            upitZaSudiju.Parameters.Add(brojLicenceParametar);
            upitZaSudiju.Prepare();
            MySqlDataReader rezultatUpita = upitZaSudiju.ExecuteReader();
            if (rezultatUpita.Read())
            {
                sudija = new Sudija(rezultatUpita.GetInt32("IdOsobe"), rezultatUpita.GetString("Ime"), rezultatUpita.GetString("Prezime"), rezultatUpita.GetDateTime("DatumRodjenja"), rezultatUpita.GetString("Pol"), rezultatUpita.GetString("BrojSudijskeLicence"), rezultatUpita.GetBoolean("JeAktivan"));
                rezultatUpita.Close();
            }
            return sudija;
        }

        public bool PromjeniAktivnost(Sudija s)
        {
            MySqlCommand iskazPromjenaAktivnosti = DatabaseConnection.GetConnection().CreateCommand();
            iskazPromjenaAktivnosti.CommandText = "update sudija set jeaktivan =  @JeAktivan where idosobe = @ID";

            s.JeAktivan = !s.JeAktivan;
            MySqlParameter jeAktivanParametar = new MySqlParameter("@JeAktivan", MySqlDbType.Int16)
            {
                Value = s.JeAktivan
            };
            MySqlParameter idParametar = new MySqlParameter("@ID", MySqlDbType.Int32)
            {
                Value = s.IdOsobe
            };
            iskazPromjenaAktivnosti.Parameters.Add(jeAktivanParametar);
            iskazPromjenaAktivnosti.Parameters.Add(idParametar);
            iskazPromjenaAktivnosti.Prepare();
            try
            {
                iskazPromjenaAktivnosti.ExecuteNonQuery();
                return true;
            }
            catch (MySqlException mysqlex)
            {
                Console.WriteLine(mysqlex.Message);
                s.JeAktivan = !s.JeAktivan;
                return false;
            }
        }

        public bool JeSudija(Sudija s)
        {
            MySqlCommand upitZaSudiju = DatabaseConnection.GetConnection().CreateCommand();
            upitZaSudiju.CommandText = "select * from sudija where brojsudijskelicence = @BrojSudijskeLicence";
            MySqlParameter brojSudijskeLicenceParametar = new MySqlParameter("@BrojSudijskeLicence", MySqlDbType.VarChar)
            {
                Value = s.BrojSudijskeLicence
            };
            upitZaSudiju.Parameters.Add(brojSudijskeLicenceParametar);
            upitZaSudiju.Prepare();
            MySqlDataReader rezultatUpita = upitZaSudiju.ExecuteReader();

            if (rezultatUpita.Read())
            {
                s.IdOsobe = rezultatUpita.GetInt32("IdOsobe");
                rezultatUpita.Close();
                return true;
            }
            rezultatUpita.Close();
            return false;
        }

        public List<Sudija> DohvatiSveAktivneSudije()
        {
            List<Sudija> sveSudije = new List<Sudija>();

            MySqlCommand upitZaSveSudije = DatabaseConnection.GetConnection().CreateCommand();
            upitZaSveSudije.CommandText = "select * from svesudije where jeAktivan" +
                " = 1";
            MySqlDataReader rezultatUpita = upitZaSveSudije.ExecuteReader();
            while (rezultatUpita.Read())
            {
                Sudija sudija = new Sudija(rezultatUpita.GetInt32("IdOsobe"), rezultatUpita.GetString("Ime"), rezultatUpita.GetString("Prezime"), rezultatUpita.GetDateTime("DatumRodjenja"), rezultatUpita.GetString("Pol"), rezultatUpita.GetString("BrojSudijskeLicence"), rezultatUpita.GetBoolean("JeAktivan"));
                sveSudije.Add(sudija);
            }
            rezultatUpita.Close();
            return sveSudije;
        }

        public bool JeDelegiranZaTakmicenje(Sudija sudija, Takmicenje takmicenje)
        {
            MySqlCommand upitZaSudi = DatabaseConnection.GetConnection().CreateCommand();
            upitZaSudi.CommandText = "select * from sudi where idsudije = @IdSudije and idtakmicenja = @IdTakmicenja";
            MySqlParameter idSudijeParametar = new MySqlParameter("@IdSudije", MySqlDbType.Int32) { Value = sudija.IdOsobe };
            MySqlParameter idtakmicenjaParametar = new MySqlParameter("@IdTakmicenja", MySqlDbType.Int32) { Value = takmicenje.IdTakmicenja };
            upitZaSudi.Parameters.AddRange(new object[] { idSudijeParametar, idtakmicenjaParametar });
            upitZaSudi.Prepare();
            MySqlDataReader rezultatUpita = upitZaSudi.ExecuteReader();

            bool rezultat = rezultatUpita.Read();
            rezultatUpita.Close();
            return rezultat;
        }

        public string UlogaSudijeNaTakmicenju(Sudija sudija, Takmicenje takmicenje)
        {
            string uloga = "Pomoćni sudija";

            MySqlCommand upitZaUlogu = DatabaseConnection.GetConnection().CreateCommand();
            upitZaUlogu.CommandText = "select * from sudi where idsudije = @IdSudije and idtakmicenja = @IdTakmicenja";
            MySqlParameter idSudijeParametar = new MySqlParameter("@IdSudije", MySqlDbType.Int32) { Value = sudija.IdOsobe };
            MySqlParameter idTakmicenjaParametar = new MySqlParameter("@IdTakmicenja", MySqlDbType.Int32) { Value = takmicenje.IdTakmicenja };
            upitZaUlogu.Parameters.AddRange(new object[] { idSudijeParametar, idTakmicenjaParametar });
            upitZaUlogu.Prepare();
            MySqlDataReader rezultatUpita = upitZaUlogu.ExecuteReader();
            if (rezultatUpita.Read())
            {
                bool jeVrhovni = rezultatUpita.GetBoolean("JeVrhovniSudija");
                bool jeGlavni = rezultatUpita.GetBoolean("JeGlavniSudija");

                if (jeVrhovni)
                    uloga = "Vrhovni sudija";
                else if (jeGlavni)
                    uloga = "Glavni sudija";
            }
            rezultatUpita.Close();
            return uloga;
        }

        public void DelegirajSudiju(Sudija sudija, Takmicenje takmicenje, string uloga)
        {
            MySqlCommand iskazZaDelegiranje = DatabaseConnection.GetConnection().CreateCommand();
            iskazZaDelegiranje.CommandText = "insert into sudi values (@IdSudije, @IdTakmicenja, @JeGlavni, @JeVrhovni)";
            MySqlParameter idSudijeParametar = new MySqlParameter("@IdSudije", MySqlDbType.Int32) { Value = sudija.IdOsobe };
            MySqlParameter idTakmicenjaParametar = new MySqlParameter("@IdTakmicenja", MySqlDbType.Int32) { Value = takmicenje.IdTakmicenja };
            MySqlParameter jeGlavniParametar = new MySqlParameter("@JeGlavni", MySqlDbType.Int16) { Value = "Glavni sudija".Equals(uloga) ? 1 : 0 };
            MySqlParameter jeVrhovniParametar = new MySqlParameter("@JeVrhovni", MySqlDbType.Int16) { Value = "Vrhovni sudija".Equals(uloga) ? 1 : 0 };

            iskazZaDelegiranje.Parameters.AddRange(new object[] { idSudijeParametar, idTakmicenjaParametar, jeGlavniParametar, jeVrhovniParametar });
            iskazZaDelegiranje.Prepare();
            try
            {
                iskazZaDelegiranje.ExecuteNonQuery();
            }
            catch(MySqlException mysqlex)
            {
                Console.WriteLine(mysqlex.Message);
            }
        }

        public void UkloniSaTakmicenja(Sudija sudija, Takmicenje takmicenje)
        {
            MySqlCommand iskazZaUklanjanje = DatabaseConnection.GetConnection().CreateCommand();
            iskazZaUklanjanje.CommandText = "delete from sudi where idSudije = @IdSudije and idTakmicenja = @IdTakmicenja";
            MySqlParameter idSudijeParametar = new MySqlParameter("@IdSudije", MySqlDbType.Int32) { Value = sudija.IdOsobe };
            MySqlParameter idTakmicenjaParamter = new MySqlParameter("@IdTakmicenja", MySqlDbType.Int32) { Value = takmicenje.IdTakmicenja };
            iskazZaUklanjanje.Parameters.AddRange(new object[] { idSudijeParametar, idTakmicenjaParamter });
            iskazZaUklanjanje.Prepare();
            iskazZaUklanjanje.ExecuteNonQuery();
        }

        public List<Sudija> DohvatiPoImenu(string str)
        {
            List<Sudija> sudije = new List<Sudija>();
            MySqlCommand upitZaSudijePoImenu = DatabaseConnection.GetConnection().CreateCommand();
            upitZaSudijePoImenu.CommandText = "select * from svesudije where concat(ime, ' ', prezime) like'%" + str + "%'";

            MySqlDataReader rezultatUpita = upitZaSudijePoImenu.ExecuteReader();

            while (rezultatUpita.Read())
            {
                Sudija sudija = new Sudija(rezultatUpita.GetInt32("IdOsobe"), rezultatUpita.GetString("Ime"), rezultatUpita.GetString("Prezime"), rezultatUpita.GetDateTime("DatumRodjenja"), rezultatUpita.GetString("Pol"), rezultatUpita.GetString("BrojSudijskeLicence"), rezultatUpita.GetBoolean("JeAktivan"));
                sudije.Add(sudija);
            }
            rezultatUpita.Close();
            return sudije;
        }
    }
}