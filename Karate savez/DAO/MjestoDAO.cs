﻿using Karate_savez.Database;
using Karate_savez.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DAO
{
    class MjestoDAO
    {
        public bool MjestoPostoji(Mjesto m)
        {
            MySqlCommand upitZaMjesto = DatabaseConnection.GetConnection().CreateCommand();
            upitZaMjesto.CommandText = "select * from mjesto where naziv = @Naziv and adresa = @Adresa";
            MySqlParameter nazivParametar = new MySqlParameter("@Naziv", MySqlDbType.VarChar)
            {
                Value = m.Naziv
            };
            MySqlParameter adresaParametar = new MySqlParameter("@Adresa", MySqlDbType.VarChar)
            {
                Value = m.Adresa
            };
            upitZaMjesto.Parameters.AddRange(new object[] { nazivParametar, adresaParametar });
            upitZaMjesto.Prepare();

            MySqlDataReader rezultatUpita = upitZaMjesto.ExecuteReader();
            bool postoji = rezultatUpita.HasRows;
            rezultatUpita.Read();
            if(postoji)
                 m.IdMjesta = rezultatUpita.GetInt32("IdMjesta");
            rezultatUpita.Close();
            return postoji;
        }

        public bool DodajMjesto(Mjesto m)
        {
            if (MjestoPostoji(m))
                return false;
            else
            {
                MySqlCommand iskazDodaj = DatabaseConnection.GetConnection().CreateCommand();
                iskazDodaj.CommandText = "insert into mjesto (naziv, adresa) values (@Naziv, @Adresa)";
                MySqlParameter nazivParametar = new MySqlParameter("@Naziv", MySqlDbType.VarChar)
                {
                    Value = m.Naziv
                };
                MySqlParameter adresaParametar = new MySqlParameter("@Adresa", MySqlDbType.VarChar)
                {
                    Value = m.Adresa
                };
                iskazDodaj.Parameters.AddRange(new object[] { nazivParametar, adresaParametar });
                iskazDodaj.Prepare();
                try
                {
                    iskazDodaj.ExecuteNonQuery();
                    int poslednjiId = (int)iskazDodaj.LastInsertedId;
                    m.IdMjesta = poslednjiId;
                    return true;
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return false;
                }
            }
        }

        public List<Mjesto> PronadjiPoNazivu(string Naziv)
        {
            List<Mjesto> pronadjenaMjesta = new List<Mjesto>();

            MySqlCommand upitZaMjesta = DatabaseConnection.GetConnection().CreateCommand();
            upitZaMjesta.CommandText = "select * from mjesto where naziv = @Naziv";
            MySqlParameter nazivParameter = new MySqlParameter("@Naziv", MySqlDbType.VarChar)
            {
                Value = Naziv
            };
            upitZaMjesta.Parameters.Add(nazivParameter);
            upitZaMjesta.Prepare();
            MySqlDataReader rezultatUpita = upitZaMjesta.ExecuteReader();

            while (rezultatUpita.Read())
            {
                Mjesto m = new Mjesto(rezultatUpita.GetInt32("IdMjesta"), rezultatUpita.GetString("Naziv"), rezultatUpita.GetString("Adresa"));
                pronadjenaMjesta.Add(m);
            }
            rezultatUpita.Close();

            return pronadjenaMjesta;
        }

        public Mjesto DohvatiMjestoPoIDu(int id)
        {
            Mjesto mjesto = null;

            MySqlCommand upitZaMjesto = DatabaseConnection.GetConnection().CreateCommand();
            upitZaMjesto.CommandText = "select * from mjesto where idmjesta = @ID";
            MySqlParameter idParametar = new MySqlParameter("@ID", MySqlDbType.Int32)
            {
                Value = id
            };
            upitZaMjesto.Parameters.Add(idParametar);
            upitZaMjesto.Prepare();
            MySqlDataReader rezultatUpita = upitZaMjesto.ExecuteReader();
            while (rezultatUpita.Read())
                mjesto = new Mjesto(rezultatUpita.GetInt32("IdMjesta"), rezultatUpita.GetString("Naziv"), rezultatUpita.GetString("Adresa"));
            rezultatUpita.Close();

            return mjesto;
        }

        public List<Mjesto> DohvatiSvaMjesta()
        {
            List<Mjesto> svaMjesta = new List<Mjesto>();

            MySqlCommand upitZaSvaMjesta = DatabaseConnection.GetConnection().CreateCommand();
            upitZaSvaMjesta.CommandText = "select * from mjesto";
            MySqlDataReader rezultatUpita = upitZaSvaMjesta.ExecuteReader();
            while (rezultatUpita.Read())
            {
                Mjesto mjesto = new Mjesto(rezultatUpita.GetInt32("IdMjesta"), rezultatUpita.GetString("Naziv"), rezultatUpita.GetString("Adresa"));
                svaMjesta.Add(mjesto);
            }
            rezultatUpita.Close();

            return svaMjesta;
        }
    }
}