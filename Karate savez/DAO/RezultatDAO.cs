﻿using Karate_savez.Database;
using Karate_savez.DTO;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.DAO
{
    class RezultatDAO
    {
        public void DodajRezultat(Rezultat rezultat)
        {
            MySqlCommand iskazZaDodavanje = DatabaseConnection.GetConnection().CreateCommand();
            iskazZaDodavanje.CommandText = "insert into rezultat values (@IdTakmicenja, @IdClana, @OsvojenoMjesto, @IdKategorije)";
            MySqlParameter idTakmicenjaParametar = new MySqlParameter("@IdTakmicenja", MySqlDbType.Int32) { Value = rezultat.Takmicenje.IdTakmicenja };
            MySqlParameter idKategorijeParametar = new MySqlParameter("@IdKategorije", MySqlDbType.Int32) { Value = rezultat.Kategorija.IdKategorije };

            MySqlParameter idClanaParametar = new MySqlParameter("@IdClana", MySqlDbType.Int32) { Value = rezultat.PrvoMjesto.IdOsobe };
            MySqlParameter osvojenoMjestoParametar = new MySqlParameter("@OsvojenoMjesto", MySqlDbType.Int16) { Value = 1 };

            iskazZaDodavanje.Parameters.AddRange(new object[] { idTakmicenjaParametar, idClanaParametar, osvojenoMjestoParametar, idKategorijeParametar });
            iskazZaDodavanje.Prepare();
            iskazZaDodavanje.ExecuteNonQuery();

            iskazZaDodavanje.Parameters.Remove(idClanaParametar);
            iskazZaDodavanje.Parameters.Remove(osvojenoMjestoParametar);
            if (rezultat.DrugoMjesto != null)
            {
                idClanaParametar.Value = rezultat.DrugoMjesto.IdOsobe;
                osvojenoMjestoParametar.Value = 2;
                iskazZaDodavanje.Parameters.AddRange(new object[] { idClanaParametar, osvojenoMjestoParametar });
                iskazZaDodavanje.Prepare();
                iskazZaDodavanje.ExecuteNonQuery();

                iskazZaDodavanje.Parameters.Remove(idClanaParametar);
                iskazZaDodavanje.Parameters.Remove(osvojenoMjestoParametar);
                if (rezultat.TreceMjesto != null)
                {
                    idClanaParametar.Value = rezultat.TreceMjesto.IdOsobe;
                    osvojenoMjestoParametar.Value = 3;
                    iskazZaDodavanje.Parameters.AddRange(new object[] { idClanaParametar, osvojenoMjestoParametar });
                    iskazZaDodavanje.Prepare();
                    iskazZaDodavanje.ExecuteNonQuery();

                    iskazZaDodavanje.Parameters.Remove(idClanaParametar);
                    if (rezultat.TreceMjesto2 != null)
                    {
                        idClanaParametar.Value = rezultat.TreceMjesto2.IdOsobe;
                        iskazZaDodavanje.Parameters.Add(idClanaParametar);
                        iskazZaDodavanje.Prepare();
                        iskazZaDodavanje.ExecuteNonQuery();
                    }
                }
            }
        }

        public void DodajRezultatKluba(Klub k, Takmicenje t, int rezultat)
        {
            MySqlCommand iskazZaDodavanje = DatabaseConnection.GetConnection().CreateCommand();
            iskazZaDodavanje.CommandText = "insert into rezultatkluba values (@IdKluba, @IdTakmicenja, @OsvojenoMjesto)";
            MySqlParameter idKlubaParametar = new MySqlParameter("@IdKluba", MySqlDbType.Int32) { Value = k.IdKluba };
            MySqlParameter idTakmicenjaParametar = new MySqlParameter("@IdTakmicenja", MySqlDbType.Int32) { Value = t.IdTakmicenja };
            MySqlParameter osvojenoMjestoParametar = new MySqlParameter("@OsvojenoMjesto", MySqlDbType.Int16) { Value = rezultat };
            iskazZaDodavanje.Parameters.AddRange(new object[] { idKlubaParametar, idTakmicenjaParametar, osvojenoMjestoParametar });
            iskazZaDodavanje.Prepare();
            iskazZaDodavanje.ExecuteNonQuery();
        }

        public Rezultat RezultatTakmicenjaZaKategoriju (Takmicenje takmicenje, Kategorija kategorija)
        {
            Rezultat rezultat = null;
            MySqlCommand upitZaRezultate = DatabaseConnection.GetConnection().CreateCommand();
            upitZaRezultate.CommandText = "select * from rezultat where idtakmicenja = @IdTakmicenja and idkategorije = @IdKategorije and osvojenomjesto = @Mjesto";
            TakmicarDAO takmicarDAO = new TakmicarDAO();
            List<Takmicar> takmicari = takmicarDAO.DohvatiSveTakmicare();
            MySqlParameter idTakmicenjaParametar = new MySqlParameter("@IdTakmicenja", MySqlDbType.Int32) { Value = takmicenje.IdTakmicenja };
            MySqlParameter idKategorijeParametar = new MySqlParameter("@IdKategorije", MySqlDbType.Int32) { Value = kategorija.IdKategorije };
            MySqlParameter mjestoParametar = new MySqlParameter("@Mjesto", MySqlDbType.Int16) { Value = 1 };
            upitZaRezultate.Parameters.AddRange(new object[] { idTakmicenjaParametar, idKategorijeParametar, mjestoParametar });
            upitZaRezultate.Prepare();
            Takmicar prvoMjesto = null;
            Takmicar drugoMjesto = null;
            List<Takmicar> treceMjesto = new List<Takmicar>() ;
            MySqlDataReader rezultatUpita = upitZaRezultate.ExecuteReader();
            if (rezultatUpita.Read())
            {
                int idTakmicara = rezultatUpita.GetInt32("IdClana");
                prvoMjesto = takmicari.Find(e => e.IdOsobe == idTakmicara);
            }
            rezultatUpita.Close();


            int index = upitZaRezultate.Parameters.IndexOf("@Mjesto");
            upitZaRezultate.Parameters[index].Value = 2;
            upitZaRezultate.Prepare();

            rezultatUpita = upitZaRezultate.ExecuteReader();
            if (rezultatUpita.Read())
            {
                int idTakmicara = rezultatUpita.GetInt32("IdClana");
                drugoMjesto = takmicari.Find(e => e.IdOsobe == idTakmicara);
            }
            rezultatUpita.Close();

            upitZaRezultate.Parameters[index].Value = 3;
            upitZaRezultate.Prepare();
            rezultatUpita = upitZaRezultate.ExecuteReader();
            while (rezultatUpita.Read())
            {
                int idTakmicara = rezultatUpita.GetInt32("IdClana");
                treceMjesto.Add(takmicari.Find(e => e.IdOsobe == idTakmicara));
            }
            rezultatUpita.Close();

            rezultat = new Rezultat(takmicenje, kategorija, prvoMjesto, drugoMjesto, treceMjesto.Count > 0 ? treceMjesto[0] : null, treceMjesto.Count == 2 ? treceMjesto[1] : null);

            return rezultat;
        }
        
        public void ObrisiRezultateZaTakmicenje(Takmicenje t)
        {
            MySqlCommand iskazZaBrisanje = DatabaseConnection.GetConnection().CreateCommand();
            iskazZaBrisanje.CommandText = "delete from rezultat where idTakmicenja = @Id";
            MySqlParameter idParametar = new MySqlParameter("@Id", MySqlDbType.Int32) { Value = t.IdTakmicenja };
            iskazZaBrisanje.Parameters.Add(idParametar);
            iskazZaBrisanje.Prepare();
            iskazZaBrisanje.ExecuteNonQuery();
        }

        public LinkedList<Klub> RezultatTakmicenjaZaKlubove(Takmicenje t)
        {
            LinkedList<Klub> rezultati = new LinkedList<Klub>();

            MySqlCommand upitZaRezultate = DatabaseConnection.GetConnection().CreateCommand();
            upitZaRezultate.CommandText = "select * from rezultatkluba where idTakmicenja = @Id and osvojenomjesto = @Mjesto";
            MySqlParameter idParameter = new MySqlParameter("@Id", MySqlDbType.Int32) { Value = t.IdTakmicenja };
            MySqlParameter mjestoParametar = new MySqlParameter("@Mjesto", MySqlDbType.Int16) { Value = 1 };
            upitZaRezultate.Parameters.AddRange(new object[] { idParameter, mjestoParametar });
            upitZaRezultate.Prepare();
            KlubDAO klubDAO = new KlubDAO();
            List<Klub> sviKlubovi = klubDAO.DohvatiKlubove();

            MySqlDataReader rezultatUpita = upitZaRezultate.ExecuteReader();
            if (rezultatUpita.Read())
            {
                int idKluba = rezultatUpita.GetInt32("IdKluba");
                Klub k = sviKlubovi.Find(e => e.IdKluba == idKluba);
                rezultati.AddLast(k);
            }
            rezultatUpita.Close();

            int index = upitZaRezultate.Parameters.IndexOf("@Mjesto");

            upitZaRezultate.Parameters[index].Value = 2;
            upitZaRezultate.Prepare();
            rezultatUpita = upitZaRezultate.ExecuteReader();
            if (rezultatUpita.Read())
            {
                int idKluba = rezultatUpita.GetInt32("IdKluba");
                Klub k = sviKlubovi.Find(e => e.IdKluba == idKluba);
                rezultati.AddLast(k);
            }
            rezultatUpita.Close();

            upitZaRezultate.Parameters[index].Value = 3;
            upitZaRezultate.Prepare();
            rezultatUpita = upitZaRezultate.ExecuteReader();
            if (rezultatUpita.Read())
            {
                int idKluba = rezultatUpita.GetInt32("IdKluba");
                Klub k = sviKlubovi.Find(e => e.IdKluba == idKluba);
                rezultati.AddLast(k);
            }
            rezultatUpita.Close();

            upitZaRezultate.Parameters[index].Value = 4;
            upitZaRezultate.Prepare();
            rezultatUpita = upitZaRezultate.ExecuteReader();
            if (rezultatUpita.Read())
            {
                int idKluba = rezultatUpita.GetInt32("IdKluba");
                Klub k = sviKlubovi.Find(e => e.IdKluba == idKluba);
                rezultati.AddLast(k);
            }
            rezultatUpita.Close();


            return rezultati;
        }

        public void ObrisiRezultatKlubovaZaTakmicenje(Takmicenje t)
        {
            MySqlCommand iskazZaBrisanje = DatabaseConnection.GetConnection().CreateCommand();
            iskazZaBrisanje.CommandText = "delete from rezultatkluba where idtakmicenja = @Id";
            MySqlParameter idParametar = new MySqlParameter("@Id", MySqlDbType.Int32) { Value = t.IdTakmicenja };
            iskazZaBrisanje.Parameters.Add(idParametar);
            iskazZaBrisanje.Prepare();
            iskazZaBrisanje.ExecuteNonQuery();
        }

        public List<RezultatTakmicara> DohvatiRezultateTakmicara(Takmicar t)
        {
            List<RezultatTakmicara> rezultati = new List<RezultatTakmicara>();
            MySqlCommand pozivProcedure = DatabaseConnection.GetConnection().CreateCommand();
            pozivProcedure.CommandText = "call rezultati_takmicara(@Id)";
            MySqlParameter idParametar = new MySqlParameter("@Id", MySqlDbType.Int32) { Value = t.IdOsobe };
            pozivProcedure.Parameters.Add(idParametar);
            pozivProcedure.Prepare();
            List<Kategorija> kategorije = new KategorijaDAO().DohvatiSveKategorije();
            List<Takmicenje> takmicenja = new TakmicenjeDAO().DohvatiSvaTakmicenja();

            MySqlDataReader rezultatProcedure = pozivProcedure.ExecuteReader();
            while (rezultatProcedure.Read())
            {
                Takmicenje takmicenje = takmicenja.Find(tkm => tkm.IdTakmicenja == rezultatProcedure.GetInt32("IdTakmicenja"));
                Kategorija k = kategorije.Find(kt => kt.IdKategorije == rezultatProcedure.GetInt32("IdKategorije"));
                short mjesto = rezultatProcedure.GetInt16("OsvojenoMjesto");
                rezultati.Add(new RezultatTakmicara(t, takmicenje, k, mjesto));
            }
            rezultatProcedure.Close();

            return rezultati;
        }

        public List<RezultatKluba> DohvatiRezultateKluba(Klub k)
        {
            List<RezultatKluba> rezultati = new List<RezultatKluba>();

            MySqlCommand pozivProcedure = DatabaseConnection.GetConnection().CreateCommand();
            pozivProcedure.CommandText = "call rezultat_kluba(@Id)";
            MySqlParameter idParametar = new MySqlParameter("@Id", MySqlDbType.Int32) { Value = k.IdKluba };
            pozivProcedure.Parameters.Add(idParametar);
            pozivProcedure.Prepare();
            List<Takmicenje> takmicenja = new TakmicenjeDAO().DohvatiSvaTakmicenja();
            MySqlDataReader rezultatUpita = pozivProcedure.ExecuteReader();
           
            while (rezultatUpita.Read())
            {
                Takmicenje t = takmicenja.Find(tkm => tkm.IdTakmicenja == rezultatUpita.GetInt32("idtakmicenja"));
                short mjesto = rezultatUpita.GetInt16("osvojenomjesto");
                rezultati.Add(new RezultatKluba(k, t, mjesto));
            }
            rezultatUpita.Close();

            return rezultati;
        }
    }
}
