﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Karate_savez.Database
{
    class DatabaseConnection
    {
        private static MySqlConnection MySqlConnection;
        private static DatabaseConnection Connection;
        private static string ConnectionString;

        static DatabaseConnection()
        {
            ConnectionString = File.ReadAllText("database.config");
        }

        private DatabaseConnection()
        {
            MySqlConnection = new MySqlConnection(ConnectionString);
        }

        public static DatabaseConnection GetConnection()
        {
            if (Connection == null)
            {
                Connection = new DatabaseConnection();
                MySqlConnection.Open();
            }
            return Connection;
        }

        public MySqlCommand CreateCommand()
        {
            return MySqlConnection.CreateCommand();
        }

        public void CloseConnection()
        {
            MySqlConnection.Close();
            MySqlConnection = null;
        }

    }
}
